#	🧑‍🏫	Project Introduction
<hr>
WEB_ORDER라는 매장 내 태블릿으로 주문하는 서비스입니다.
<br><br>
요즘들어 인건비 절감을 위해 어딜가나 구축되어 있는 솔루션이며,
<br>
어떤 방식으로 구현했는지 너무 궁금했습니다.
<br><br>
군생활하며 개발 공부를 독학으로 시작했었고 오랜 기간이 걸렸지만,
<br>
그 동안의 노력으로 쌓아왔던 스스로의 기술만을 활용하여 제작해보고 싶었습니다.


<br><br>


#	🙋‍♂️	Self Introduction
<hr>
<img src="src/main/resources/static/img/me.png">


<br><br>


#	🧑‍💻	Skill's Stack
<hr>

##  Front-End
<img src="https://img.shields.io/badge/HTML-orange">
<img src="https://img.shields.io/badge/CSS-blue">
<img src="https://img.shields.io/badge/Java Script-yellow">
<img src="https://img.shields.io/badge/JQuery-lightblue">
<img src="https://img.shields.io/badge/Thymeleaf-green">
<img src="https://img.shields.io/badge/JSP-red">


<hr>

##  Back-End
<img src="https://img.shields.io/badge/JAVA-darkred">
<img src="https://img.shields.io/badge/Spring Boot-darkgreen">

<hr>

##  Database
<img src="https://img.shields.io/badge/Oracle 21c-red">
<img src="https://img.shields.io/badge/MyBatis-grey">


<br><br>


#  Oracle Database ERD 구조
<hr>
<img src="src/main/resources/static/img/ERD.png">


#   주요 기능
<hr>

##  [사용자]
- 메뉴 탐색, 장바구니 담기, 주문 진행, 주문 내역, 모바일 영수증, 직원 호출
    - 고객은 태블릿(디바이스)을 통해 해당 매장의 메뉴를 탐색할 수 있습니다.
    - 원하는 메뉴를 선택하여 수량을 지정 후 장바구니에 담을 수 있습니다.
    - 장바구니에 담긴 메뉴를 확인하여 수량을 변경, 삭제, 주문을 진행할 수 있습니다.
    - 주문이 완료되면 주문 내역을 확인할 수 있으며, 지류 영수증을 대신해 본인의 연락처로 모바일 영수증을 받아볼 수 있습니다.
    - 필요에 따라 직원을 호출할 수 있으며, 호출 전 매장에서 사전에 설정한 호출 메뉴를 통해 요구사항을 요청할 수 있습니다.

##  [관리자]
- 태블릿 등록, 관리자 로그인, 카테고리 설정, 메뉴 설정, 대기화면 설정, 테이블 설정
    - 최초 태블릿으로 앱 실행 시 매장에 매칭하여 등록할 수 있습니다.
    - 매장 관리자는 솔루션 공급업체로부터 제공받은 계정을 통해 관리자 웹에 로그인 가능합니다.
    - 관리자 웹에서 카테고리 설정이 가능하며, 노출할 수 있는 시간대를 지정할 수 있습니다.
    - 각 메뉴가 설정(추가, 삭제, 변경)이 가능하며, Best 추천 메뉴 여부와 메뉴의 노출 여부를 지정할 수 있습니다.
    - 태블릿 동작이 없을 시 광고와 같은 배너를 노출할 수 있으며, 미동작 감지 시간을 함께 지정할 수 있습니다.
    - 각 테이블 정보를 설정할 수 있으며, 테이블 번호, 사용 여부를 지정할 수 있습니다.
    - 쿠폰을 발행하고 관리할 수 있습니다.


<br><br>


#   API 명세서
<hr>
<table>
    <thead>
        <tr>
            <th>URL</th>
            <th>Method</th>
            <th>Comment</th>
        <tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan="4">/order/tablet</td>
            <td>GET</td>
            <td>태블릿 주문 메인화면으로 접근합니다. 해당 태블릿이 등록된 기기가 아니거나 초기화된 경우 태블릿 업장 등록화면으로 리디렉션되며, 해당 태블릿 고유 UUID 정보를 DB(DEVICE_TABLE_MAPPING)에 저장합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/tablet/category</td>
            <td>GET</td>
            <td>태블릿 메인 주문화면상의 좌측 카테고리 리스트를 반환합니다. 리턴 조건은 카테고리 사용 여부 Y인 경우, 현재 시간이 노출 시간에 포함되어 있는 경우에만 리턴합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/tablet/menu</td>
            <td>GET</td>
            <td>선택한 카테고리에 대한 메뉴 리스트를 리턴합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/tablet/menu/detail</td>
            <td>GET</td>
            <td>선택한 메뉴에 대해 상세 정보(메뉴 설명, 메뉴 금액, 메뉴 사진 등)를 리턴합니다. 메뉴가 이미 카트에 담겨있는 메뉴라면 펑션이 변경됩니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/tablet/menu/history</td>
            <td>GET</td>
            <td>주문 내역을 리턴합니다. 주문 내역을 초기화하지 않은 경우에만 작동합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/tablet/call/staff</td>
            <td>GET</td>
            <td>직원 호출 기능 오픈 시 해당 업장에 필요한 직원호출 편의 기능(물, 티슈, 수저 등)을 리턴합니다. (NEEDS_ITEM 테이블에서 select)</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td>직원을 호출합니다. ORDERING 테이블에 NEEDS_ITEM 칼럼에 저장할지 고민중</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/tablet/cart</td>
            <td>GET</td>
            <td>카트에 담긴 메뉴 정보(메뉴 리스트, 총 금액)를 리턴합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td>카트에 담긴 메뉴 정보(수량)을 업데이트합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td>선택한 메뉴를 카트에 저장합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td>카트에 담긴 메뉴중 선택한 메뉴를 삭제합니다.</td>
        </tr>
        <tr>
            <td rowspan="4">/order/tablet/ordering</td>
            <td>GET</td>
            <td>주문을 진행합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/tablet/register</td>
            <td>GET</td>
            <td>태블릿 등록화면으로 접근합니다. 해당 주소 접근 시 자동으로 태블릿 고유 UUID 값을 리턴하여 보여줍니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td>사용할 테이블명을 DB(DEVICE_TABLE_MAPPING)에 업데이트 처리합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/management/login.do</td>
            <td>GET</td>
            <td>관리자 웹 로그인 페이지로 접근합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td>로그인 처리 로직을 실행합니다. 성공 시 /order/management/index로 리디렉션되며, 실패 시 로그인 페이지로 재접근합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/management/index</td>
            <td>GET</td>
            <td>로그인 성공 시 보여지는 페이지입니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/management/counter</td>
            <td>GET</td>
            <td>주문 현황 관리자 페이지를 리턴합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/management/counter/list</td>
            <td>GET</td>
            <td>주문 현황에 대한 리스트를 실시간으로 리턴합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td>주문 리스트 확인 시 주문상태를 진행으로 업데이트 후 리스트를 재조회합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/management/counter/list/detail</td>
            <td>GET</td>
            <td>주문 현황에서 리스트 선택 시 해당 선택한 주문 건에 대한 상세 정보를 리턴합니다. (주문한 테이블 번호, 메뉴별 진행상태 등)</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/management/menu</td>
            <td>GET</td>
            <td>관리자 웹의 메뉴 설정 화면에서 DB에 저장된 메뉴 리스트를 리턴합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr><tr>
            <td rowspan="4">/order/management/menu/list/detail</td>
            <td>GET</td>
            <td>관리자 웹의 메뉴 설정 화면에서 선택한 메뉴에 대한 상세 정보를 리턴합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td>메뉴 정보를 수정 및 추가합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td>메뉴 정보를 삭제합니다.</td>
        </tr>
        <tr>
            <td rowspan="4">/order/management/table</td>
            <td>GET</td>
            <td>관리자 웹의 테이블 설정 화면에서 설정되어 있는 테이블 정보를 리턴합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td>테이블 정보에 대한 설정을 변경합니다. (테이블 번호, 사용 여부 등)</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td>현재 오더코드세션을 초기화합니다. (다음 식사팀을 위해 초기화 필요 기능)</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/management/table/info/detail</td>
            <td>GET</td>
            <td>관리자 웹의 테이블 설정 화면에서 테이블 선택 시 해당 테이블의 상세 정보를 리턴합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/management/call/staff</td>
            <td>GET</td>
            <td>직원 호출 리스트를 리턴합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/management/category</td>
            <td>GET</td>
            <td>관리자 웹의 카테고리 설정 화면을 보여줍니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/management/category/list</td>
            <td>GET</td>
            <td>관리자 웹의 카테고리 설정 화면에서 카테고리 리스트를 리턴합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/management/category/list/detail</td>
            <td>GET</td>
            <td>관리자 웹의 카테고리 리스트를 선택 시 해당 카테고리의 상세 정보를 리턴합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td>카테고리의 상세 정보를 변경합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td>카테고리를 신규 등록합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td>카테고리 정보를 삭제합니다.</td>
        </tr>
        <tr>
            <td rowspan="4">/order/management/order/offer/done</td>
            <td>GET</td>
            <td>주문 제공 완료로 처리합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/management/sale</td>
            <td>GET</td>
            <td>매출 발생일에 대한 영업현황을 조회합니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="4">/order/management/close-data</td>
            <td>GET</td>
            <td>매출 발생일의 영업마감을 위한 영업 자료를 조회하며, 이상 없을 시 마감을 진행합니다. 미제공 메뉴 및 초기화 되지않은 테이블이 존재하는 경우 마감이 진행되지 않습니다.</td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>PUT</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>POST</td>
            <td></td>
        </tr>
        <tr>
            <!-- <td></td> -->
            <td>DELETE</td>
            <td></td>
        </tr>
    </tbody>
</table>


<br><br>


#   태블릿 화면
<hr>

##  주문 메인화면
<img src="src/main/resources/static/img/tablet_main.png">
<br>

##  장바구니 팝업
<img src="src/main/resources/static/img/cart_main.png">
<br>

##  주문내역 팝업
<img src="src/main/resources/static/img/history_main.png">
<br>

##  직원호출 팝업
<img src="src/main/resources/static/img/call_staff_main.png">
<br>

##  메뉴 상세 팝업
<img src="src/main/resources/static/img/menu_detail.png">
<br>

##  관리자 로그인
<img src="src/main/resources/static/img/login_main.png">
<br>

##  관리자 주문 현황
<img src="src/main/resources/static/img/ordering_list.png">

####    주문 리스트 선택 시 상세 화면이 오픈되며, 자동으로 주문상태가 진행으로 변경됩니다. 
<br>

##  관리자 카테고리 설정
<img src="src/main/resources/static/img/category_list.png">
<br>

##  관리자 메뉴 설정
<img src="src/main/resources/static/img/menu_list.png">
<br>

##  관리자 테이블 설정
<img src="src/main/resources/static/img/table_list.png">
<br>

##  관리자 영업 설정
<img src="src/main/resources/static/img/sale.png">
<br>
