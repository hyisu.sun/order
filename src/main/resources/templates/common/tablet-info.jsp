<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="ko">

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>단말기 등록</title>

		<link rel="stylesheet" href="/css/common/tablet-info.css">
	</head>

	<body>
		<div class="wrap-01">
			<div class="ti">
				<div class="ti-notice">
					<p>해당 단말기는
						<mark>미등록 단말기</mark>
						</em>입니다.
					</p>
					<p>정상적인 서비스 이용을 위해
						<mark>사업자등록번호</mark>
					   를 등록바랍니다.
					</p>
					<p>
					<div>단말기가 초기화되는 사유는 아래와 같습니다.</div>
					<ol>
						<li>단말기 초기화</li>
						<li>앱 재설치(캐시 및 데이터 삭제)</li>
					</ol>
					</p>
				</div>
				<div class="ti-input-area">
					<form action="/device/register" method="post">
						<input type="hidden" name="">
						<input class="company-code" type="text" name="companyCode" placeholder="사업자등록번호(-제외)">
						<br>
						<input class="submit-btn" type="submit" value="등록">
					</form>
				</div>
			</div>
		</div>
	</body>

</html>
