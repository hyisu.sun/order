var Pi = Object.create;
var Ke = Object.defineProperty;
var gi = Object.getOwnPropertyDescriptor;
var Oi = Object.getOwnPropertyNames;
var Si = Object.getPrototypeOf, ui = Object.prototype.hasOwnProperty;
var fi = (t, e) => () => (e || t((e = {exports: {}}).exports, e), e.exports), We = (t, e) => {
	for(var r in e) Ke(t, r, {get: e[r], enumerable: !0})
}, Yi = (t, e, r, o) => {
	if(e && typeof e == "object" || typeof e == "function") for(let p of Oi(e)) !ui.call(t, p) && p !== r && Ke(t, p, {get: () => e[p], enumerable: !(o = gi(e, p)) || o.enumerable});
	return t
};
var wi = (t, e, r) => (r = t != null ? Pi(Si(t)) : {}, Yi(e || !t || !t.__esModule ? Ke(r, "default", {value: t, enumerable: !0}) : r, t));
var Ze = fi((ee, Ce) => {
	(function(t, e) {
		"use strict";
		var r = "1.0.35", o = "", p = "?", C = "function", c = "undefined", N = "object", E = "string", _ = "major", n = "model", a = "name", i = "type", s = "vendor", R = "version", g = "architecture", W = "console", m = "mobile", d = "tablet", b = "smarttv", q = "wearable", Ae = "embedded", Ue = 350, ne = "Amazon", Z = "Apple", Me = "ASUS", ke = "BlackBerry", z = "Browser", oe = "Chrome", mi = "Edge", se = "Firefox", ae = "Google", Ve = "Huawei", Pe = "LG", ge = "Microsoft", Ge = "Motorola", pe = "Opera", Oe = "Samsung", Fe = "Sharp", _e = "Sony", pr = "Viera", Se = "Xiaomi", ue = "Zebra", xe = "Facebook", He = "Chromium OS", je = "Mac OS", yi = function(y, U) {
			var T = {};
			for(var u in y) U[u] && U[u].length % 2 === 0 ? T[u] = U[u].concat(y[u]) : T[u] = y[u];
			return T
		}, Re = function(y) {
			for(var U = {}, T = 0; T < y.length; T++) U[y[T].toUpperCase()] = y[T];
			return U
		}, Je = function(y, U) {
			return typeof y === E ? Q(U).indexOf(Q(y)) !== -1 : !1
		}, Q = function(y) {
			return y.toLowerCase()
		}, Ai = function(y) {
			return typeof y === E ? y.replace(/[^\d\.]/g, o).split(".")[0] : e
		}, fe = function(y, U) {
			if(typeof y === E) return y = y.replace(/^\s\s*/, o), typeof U === c ? y : y.substring(0, Ue)
		}, $ = function(y, U) {
			for(var T = 0, u, H, k, A, I, V; T < U.length && !I;) {
				var we = U[T], Xe = U[T + 1];
				for(u = H = 0; u < we.length && !I && we[u];) if(I = we[u++].exec(y), I) for(k = 0; k < Xe.length; k++) V = I[++H], A = Xe[k], typeof A === N && A.length > 0 ? A.length === 2 ? typeof A[1] == C ? this[A[0]] = A[1].call(this, V) : this[A[0]] = A[1] : A.length === 3 ? typeof A[1] === C && !(A[1].exec && A[1].test) ? this[A[0]] = V ? A[1].call(this, V, A[2]) : e : this[A[0]] = V ? V.replace(A[1], A[2]) : e : A.length === 4 && (this[A[0]] = V ? A[3].call(this, V.replace(A[1], A[2])) : e) : this[A] = V || e;
				T += 2
			}
		}, Ye = function(y, U) {
			for(var T in U) if(typeof U[T] === N && U[T].length > 0) {
				for(var u = 0; u < U[T].length; u++) if(Je(U[T][u], y)) return T === p ? e : T
			} else if(Je(U[T], y)) return T === p ? e : T;
			return y
		}, Ui = {"1.0": "/8", "1.2": "/1", "1.3": "/3", "2.0": "/412", "2.0.2": "/416", "2.0.3": "/417", "2.0.4": "/419", "?": "/"}, qe = {ME: "4.90", "NT 3.11": "NT3.51", "NT 4.0": "NT4.0", 2e3: "NT 5.0", XP: ["NT 5.1", "NT 5.2"], Vista: "NT 6.0", 7: "NT 6.1", 8: "NT 6.2", "8.1": "NT 6.3", 10: ["NT 6.4", "NT 10.0"], RT: "ARM"}, ze = {
			browser: [[/\b(?:crmo|crios)\/([\w\.]+)/i], [R, [a, "Chrome"]], [/edg(?:e|ios|a)?\/([\w\.]+)/i], [R, [a, "Edge"]], [/(opera mini)\/([-\w\.]+)/i, /(opera [mobiletab]{3,6})\b.+version\/([-\w\.]+)/i, /(opera)(?:.+version\/|[\/ ]+)([\w\.]+)/i], [a, R], [/opios[\/ ]+([\w\.]+)/i], [R, [a, pe + " Mini"]], [/\bopr\/([\w\.]+)/i], [R, [a, pe]], [/(kindle)\/([\w\.]+)/i, /(lunascape|maxthon|netfront|jasmine|blazer)[\/ ]?([\w\.]*)/i, /(avant |iemobile|slim)(?:browser)?[\/ ]?([\w\.]*)/i, /(ba?idubrowser)[\/ ]?([\w\.]+)/i, /(?:ms|\()(ie) ([\w\.]+)/i, /(flock|rockmelt|midori|epiphany|silk|skyfire|bolt|iron|vivaldi|iridium|phantomjs|bowser|quark|qupzilla|falkon|rekonq|puffin|brave|whale(?!.+naver)|qqbrowserlite|qq|duckduckgo)\/([-\w\.]+)/i, /(heytap|ovi)browser\/([\d\.]+)/i, /(weibo)__([\d\.]+)/i], [a, R], [/(?:\buc? ?browser|(?:juc.+)ucweb)[\/ ]?([\w\.]+)/i], [R, [a, "UC" + z]], [/microm.+\bqbcore\/([\w\.]+)/i, /\bqbcore\/([\w\.]+).+microm/i], [R, [a, "WeChat(Win) Desktop"]], [/micromessenger\/([\w\.]+)/i], [R, [a, "WeChat"]], [/konqueror\/([\w\.]+)/i], [R, [a, "Konqueror"]], [/trident.+rv[: ]([\w\.]{1,9})\b.+like gecko/i], [R, [a, "IE"]], [/ya(?:search)?browser\/([\w\.]+)/i], [R, [a, "Yandex"]], [/(avast|avg)\/([\w\.]+)/i], [[a, /(.+)/, "$1 Secure " + z], R], [/\bfocus\/([\w\.]+)/i], [R, [a, se + " Focus"]], [/\bopt\/([\w\.]+)/i], [R, [a, pe + " Touch"]], [/coc_coc\w+\/([\w\.]+)/i], [R, [a, "Coc Coc"]], [/dolfin\/([\w\.]+)/i], [R, [a, "Dolphin"]], [/coast\/([\w\.]+)/i], [R, [a, pe + " Coast"]], [/miuibrowser\/([\w\.]+)/i], [R, [a, "MIUI " + z]], [/fxios\/([-\w\.]+)/i], [R, [a, se]], [/\bqihu|(qi?ho?o?|360)browser/i], [[a, "360 " + z]], [/(oculus|samsung|sailfish|huawei)browser\/([\w\.]+)/i], [[a, /(.+)/, "$1 " + z], R], [/(comodo_dragon)\/([\w\.]+)/i], [[a, /_/g, " "], R], [/(electron)\/([\w\.]+) safari/i, /(tesla)(?: qtcarbrowser|\/(20\d\d\.[-\w\.]+))/i, /m?(qqbrowser|baiduboxapp|2345Explorer)[\/ ]?([\w\.]+)/i], [a, R], [/(metasr)[\/ ]?([\w\.]+)/i, /(lbbrowser)/i, /\[(linkedin)app\]/i], [a], [/((?:fban\/fbios|fb_iab\/fb4a)(?!.+fbav)|;fbav\/([\w\.]+);)/i], [[a, xe], R], [/(kakao(?:talk|story))[\/ ]([\w\.]+)/i, /(naver)\(.*?(\d+\.[\w\.]+).*\)/i, /safari (line)\/([\w\.]+)/i, /\b(line)\/([\w\.]+)\/iab/i, /(chromium|instagram)[\/ ]([-\w\.]+)/i], [a, R], [/\bgsa\/([\w\.]+) .*safari\//i], [R, [a, "GSA"]], [/musical_ly(?:.+app_?version\/|_)([\w\.]+)/i], [R, [a, "TikTok"]], [/headlesschrome(?:\/([\w\.]+)| )/i], [R, [a, oe + " Headless"]], [/ wv\).+(chrome)\/([\w\.]+)/i], [[a, oe + " WebView"], R], [/droid.+ version\/([\w\.]+)\b.+(?:mobile safari|safari)/i], [R, [a, "Android " + z]], [/(chrome|omniweb|arora|[tizenoka]{5} ?browser)\/v?([\w\.]+)/i], [a, R], [/version\/([\w\.\,]+) .*mobile\/\w+ (safari)/i], [R, [a, "Mobile Safari"]], [/version\/([\w(\.|\,)]+) .*(mobile ?safari|safari)/i], [R, a], [/webkit.+?(mobile ?safari|safari)(\/[\w\.]+)/i], [a, [R, Ye, Ui]], [/(webkit|khtml)\/([\w\.]+)/i], [a, R], [/(navigator|netscape\d?)\/([-\w\.]+)/i], [[a, "Netscape"], R], [/mobile vr; rv:([\w\.]+)\).+firefox/i], [R, [a, se + " Reality"]], [/ekiohf.+(flow)\/([\w\.]+)/i, /(swiftfox)/i, /(icedragon|iceweasel|camino|chimera|fennec|maemo browser|minimo|conkeror|klar)[\/ ]?([\w\.\+]+)/i, /(seamonkey|k-meleon|icecat|iceape|firebird|phoenix|palemoon|basilisk|waterfox)\/([-\w\.]+)$/i, /(firefox)\/([\w\.]+)/i, /(mozilla)\/([\w\.]+) .+rv\:.+gecko\/\d+/i, /(polaris|lynx|dillo|icab|doris|amaya|w3m|netsurf|sleipnir|obigo|mosaic|(?:go|ice|up)[\. ]?browser)[-\/ ]?v?([\w\.]+)/i, /(links) \(([\w\.]+)/i, /panasonic;(viera)/i], [a, R], [/(cobalt)\/([\w\.]+)/i], [a, [R, /master.|lts./, ""]]],
			cpu: [[/(?:(amd|x(?:(?:86|64)[-_])?|wow|win)64)[;\)]/i], [[g, "amd64"]], [/(ia32(?=;))/i], [[g, Q]], [/((?:i[346]|x)86)[;\)]/i], [[g, "ia32"]], [/\b(aarch64|arm(v?8e?l?|_?64))\b/i], [[g, "arm64"]], [/\b(arm(?:v[67])?ht?n?[fl]p?)\b/i], [[g, "armhf"]], [/windows (ce|mobile); ppc;/i], [[g, "arm"]], [/((?:ppc|powerpc)(?:64)?)(?: mac|;|\))/i], [[g, /ower/, o, Q]], [/(sun4\w)[;\)]/i], [[g, "sparc"]], [/((?:avr32|ia64(?=;))|68k(?=\))|\barm(?=v(?:[1-7]|[5-7]1)l?|;|eabi)|(?=atmel )avr|(?:irix|mips|sparc)(?:64)?\b|pa-risc)/i], [[g, Q]]],
			device: [[/\b(sch-i[89]0\d|shw-m380s|sm-[ptx]\w{2,4}|gt-[pn]\d{2,4}|sgh-t8[56]9|nexus 10)/i], [n, [s, Oe], [i, d]], [/\b((?:s[cgp]h|gt|sm)-\w+|sc[g-]?[\d]+a?|galaxy nexus)/i, /samsung[- ]([-\w]+)/i, /sec-(sgh\w+)/i], [n, [s, Oe], [i, m]], [/(?:\/|\()(ip(?:hone|od)[\w, ]*)(?:\/|;)/i], [n, [s, Z], [i, m]], [/\((ipad);[-\w\),; ]+apple/i, /applecoremedia\/[\w\.]+ \((ipad)/i, /\b(ipad)\d\d?,\d\d?[;\]].+ios/i], [n, [s, Z], [i, d]], [/(macintosh);/i], [n, [s, Z]], [/\b(sh-?[altvz]?\d\d[a-ekm]?)/i], [n, [s, Fe], [i, m]], [/\b((?:ag[rs][23]?|bah2?|sht?|btv)-a?[lw]\d{2})\b(?!.+d\/s)/i], [n, [s, Ve], [i, d]], [/(?:huawei|honor)([-\w ]+)[;\)]/i, /\b(nexus 6p|\w{2,4}e?-[atu]?[ln][\dx][012359c][adn]?)\b(?!.+d\/s)/i], [n, [s, Ve], [i, m]], [/\b(poco[\w ]+)(?: bui|\))/i, /\b; (\w+) build\/hm\1/i, /\b(hm[-_ ]?note?[_ ]?(?:\d\w)?) bui/i, /\b(redmi[\-_ ]?(?:note|k)?[\w_ ]+)(?: bui|\))/i, /\b(mi[-_ ]?(?:a\d|one|one[_ ]plus|note lte|max|cc)?[_ ]?(?:\d?\w?)[_ ]?(?:plus|se|lite)?)(?: bui|\))/i], [[n, /_/g, " "], [s, Se], [i, m]], [/\b(mi[-_ ]?(?:pad)(?:[\w_ ]+))(?: bui|\))/i], [[n, /_/g, " "], [s, Se], [i, d]], [/; (\w+) bui.+ oppo/i, /\b(cph[12]\d{3}|p(?:af|c[al]|d\w|e[ar])[mt]\d0|x9007|a101op)\b/i], [n, [s, "OPPO"], [i, m]], [/vivo (\w+)(?: bui|\))/i, /\b(v[12]\d{3}\w?[at])(?: bui|;)/i], [n, [s, "Vivo"], [i, m]], [/\b(rmx[12]\d{3})(?: bui|;|\))/i], [n, [s, "Realme"], [i, m]], [/\b(milestone|droid(?:[2-4x]| (?:bionic|x2|pro|razr))?:?( 4g)?)\b[\w ]+build\//i, /\bmot(?:orola)?[- ](\w*)/i, /((?:moto[\w\(\) ]+|xt\d{3,4}|nexus 6)(?= bui|\)))/i], [n, [s, Ge], [i, m]], [/\b(mz60\d|xoom[2 ]{0,2}) build\//i], [n, [s, Ge], [i, d]], [/((?=lg)?[vl]k\-?\d{3}) bui| 3\.[-\w; ]{10}lg?-([06cv9]{3,4})/i], [n, [s, Pe], [i, d]], [/(lm(?:-?f100[nv]?|-[\w\.]+)(?= bui|\))|nexus [45])/i, /\blg[-e;\/ ]+((?!browser|netcast|android tv)\w+)/i, /\blg-?([\d\w]+) bui/i], [n, [s, Pe], [i, m]], [/(ideatab[-\w ]+)/i, /lenovo ?(s[56]000[-\w]+|tab(?:[\w ]+)|yt[-\d\w]{6}|tb[-\d\w]{6})/i], [n, [s, "Lenovo"], [i, d]], [/(?:maemo|nokia).*(n900|lumia \d+)/i, /nokia[-_ ]?([-\w\.]*)/i], [[n, /_/g, " "], [s, "Nokia"], [i, m]], [/(pixel c)\b/i], [n, [s, ae], [i, d]], [/droid.+; (pixel[\daxl ]{0,6})(?: bui|\))/i], [n, [s, ae], [i, m]], [/droid.+ (a?\d[0-2]{2}so|[c-g]\d{4}|so[-gl]\w+|xq-a\w[4-7][12])(?= bui|\).+chrome\/(?![1-6]{0,1}\d\.))/i], [n, [s, _e], [i, m]], [/sony tablet [ps]/i, /\b(?:sony)?sgp\w+(?: bui|\))/i], [[n, "Xperia Tablet"], [s, _e], [i, d]], [/ (kb2005|in20[12]5|be20[12][59])\b/i, /(?:one)?(?:plus)? (a\d0\d\d)(?: b|\))/i], [n, [s, "OnePlus"], [i, m]], [/(alexa)webm/i, /(kf[a-z]{2}wi|aeo[c-r]{2})( bui|\))/i, /(kf[a-z]+)( bui|\)).+silk\//i], [n, [s, ne], [i, d]], [/((?:sd|kf)[0349hijorstuw]+)( bui|\)).+silk\//i], [[n, /(.+)/g, "Fire Phone $1"], [s, ne], [i, m]], [/(playbook);[-\w\),; ]+(rim)/i], [n, s, [i, d]], [/\b((?:bb[a-f]|st[hv])100-\d)/i, /\(bb10; (\w+)/i], [n, [s, ke], [i, m]], [/(?:\b|asus_)(transfo[prime ]{4,10} \w+|eeepc|slider \w+|nexus 7|padfone|p00[cj])/i], [n, [s, Me], [i, d]], [/ (z[bes]6[027][012][km][ls]|zenfone \d\w?)\b/i], [n, [s, Me], [i, m]], [/(nexus 9)/i], [n, [s, "HTC"], [i, d]], [/(htc)[-;_ ]{1,2}([\w ]+(?=\)| bui)|\w+)/i, /(zte)[- ]([\w ]+?)(?: bui|\/|\))/i, /(alcatel|geeksphone|nexian|panasonic(?!(?:;|\.))|sony(?!-bra))[-_ ]?([-\w]*)/i], [s, [n, /_/g, " "], [i, m]], [/droid.+; ([ab][1-7]-?[0178a]\d\d?)/i], [n, [s, "Acer"], [i, d]], [/droid.+; (m[1-5] note) bui/i, /\bmz-([-\w]{2,})/i], [n, [s, "Meizu"], [i, m]], [/(blackberry|benq|palm(?=\-)|sonyericsson|acer|asus|dell|meizu|motorola|polytron)[-_ ]?([-\w]*)/i, /(hp) ([\w ]+\w)/i, /(asus)-?(\w+)/i, /(microsoft); (lumia[\w ]+)/i, /(lenovo)[-_ ]?([-\w]+)/i, /(jolla)/i, /(oppo) ?([\w ]+) bui/i], [s, n, [i, m]], [/(kobo)\s(ereader|touch)/i, /(archos) (gamepad2?)/i, /(hp).+(touchpad(?!.+tablet)|tablet)/i, /(kindle)\/([\w\.]+)/i, /(nook)[\w ]+build\/(\w+)/i, /(dell) (strea[kpr\d ]*[\dko])/i, /(le[- ]+pan)[- ]+(\w{1,9}) bui/i, /(trinity)[- ]*(t\d{3}) bui/i, /(gigaset)[- ]+(q\w{1,9}) bui/i, /(vodafone) ([\w ]+)(?:\)| bui)/i], [s, n, [i, d]], [/(surface duo)/i], [n, [s, ge], [i, d]], [/droid [\d\.]+; (fp\du?)(?: b|\))/i], [n, [s, "Fairphone"], [i, m]], [/(u304aa)/i], [n, [s, "AT&T"], [i, m]], [/\bsie-(\w*)/i], [n, [s, "Siemens"], [i, m]], [/\b(rct\w+) b/i], [n, [s, "RCA"], [i, d]], [/\b(venue[\d ]{2,7}) b/i], [n, [s, "Dell"], [i, d]], [/\b(q(?:mv|ta)\w+) b/i], [n, [s, "Verizon"], [i, d]], [/\b(?:barnes[& ]+noble |bn[rt])([\w\+ ]*) b/i], [n, [s, "Barnes & Noble"], [i, d]], [/\b(tm\d{3}\w+) b/i], [n, [s, "NuVision"], [i, d]], [/\b(k88) b/i], [n, [s, "ZTE"], [i, d]], [/\b(nx\d{3}j) b/i], [n, [s, "ZTE"], [i, m]], [/\b(gen\d{3}) b.+49h/i], [n, [s, "Swiss"], [i, m]], [/\b(zur\d{3}) b/i], [n, [s, "Swiss"], [i, d]], [/\b((zeki)?tb.*\b) b/i], [n, [s, "Zeki"], [i, d]], [/\b([yr]\d{2}) b/i, /\b(dragon[- ]+touch |dt)(\w{5}) b/i], [[s, "Dragon Touch"], n, [i, d]], [/\b(ns-?\w{0,9}) b/i], [n, [s, "Insignia"], [i, d]], [/\b((nxa|next)-?\w{0,9}) b/i], [n, [s, "NextBook"], [i, d]], [/\b(xtreme\_)?(v(1[045]|2[015]|[3469]0|7[05])) b/i], [[s, "Voice"], n, [i, m]], [/\b(lvtel\-)?(v1[12]) b/i], [[s, "LvTel"], n, [i, m]], [/\b(ph-1) /i], [n, [s, "Essential"], [i, m]], [/\b(v(100md|700na|7011|917g).*\b) b/i], [n, [s, "Envizen"], [i, d]], [/\b(trio[-\w\. ]+) b/i], [n, [s, "MachSpeed"], [i, d]], [/\btu_(1491) b/i], [n, [s, "Rotor"], [i, d]], [/(shield[\w ]+) b/i], [n, [s, "Nvidia"], [i, d]], [/(sprint) (\w+)/i], [s, n, [i, m]], [/(kin\.[onetw]{3})/i], [[n, /\./g, " "], [s, ge], [i, m]], [/droid.+; (cc6666?|et5[16]|mc[239][23]x?|vc8[03]x?)\)/i], [n, [s, ue], [i, d]], [/droid.+; (ec30|ps20|tc[2-8]\d[kx])\)/i], [n, [s, ue], [i, m]], [/smart-tv.+(samsung)/i], [s, [i, b]], [/hbbtv.+maple;(\d+)/i], [[n, /^/, "SmartTV"], [s, Oe], [i, b]], [/(nux; netcast.+smarttv|lg (netcast\.tv-201\d|android tv))/i], [[s, Pe], [i, b]], [/(apple) ?tv/i], [s, [n, Z + " TV"], [i, b]], [/crkey/i], [[n, oe + "cast"], [s, ae], [i, b]], [/droid.+aft(\w)( bui|\))/i], [n, [s, ne], [i, b]], [/\(dtv[\);].+(aquos)/i, /(aquos-tv[\w ]+)\)/i], [n, [s, Fe], [i, b]], [/(bravia[\w ]+)( bui|\))/i], [n, [s, _e], [i, b]], [/(mitv-\w{5}) bui/i], [n, [s, Se], [i, b]], [/Hbbtv.*(technisat) (.*);/i], [s, n, [i, b]], [/\b(roku)[\dx]*[\)\/]((?:dvp-)?[\d\.]*)/i, /hbbtv\/\d+\.\d+\.\d+ +\([\w\+ ]*; *([\w\d][^;]*);([^;]*)/i], [[s, fe], [n, fe], [i, b]], [/\b(android tv|smart[- ]?tv|opera tv|tv; rv:)\b/i], [[i, b]], [/(ouya)/i, /(nintendo) ([wids3utch]+)/i], [s, n, [i, W]], [/droid.+; (shield) bui/i], [n, [s, "Nvidia"], [i, W]], [/(playstation [345portablevi]+)/i], [n, [s, _e], [i, W]], [/\b(xbox(?: one)?(?!; xbox))[\); ]/i], [n, [s, ge], [i, W]], [/((pebble))app/i], [s, n, [i, q]], [/(watch)(?: ?os[,\/]|\d,\d\/)[\d\.]+/i], [n, [s, Z], [i, q]], [/droid.+; (glass) \d/i], [n, [s, ae], [i, q]], [/droid.+; (wt63?0{2,3})\)/i], [n, [s, ue], [i, q]], [/(quest( 2| pro)?)/i], [n, [s, xe], [i, q]], [/(tesla)(?: qtcarbrowser|\/[-\w\.]+)/i], [s, [i, Ae]], [/(aeobc)\b/i], [n, [s, ne], [i, Ae]], [/droid .+?; ([^;]+?)(?: bui|\) applew).+? mobile safari/i], [n, [i, m]], [/droid .+?; ([^;]+?)(?: bui|\) applew).+?(?! mobile) safari/i], [n, [i, d]], [/\b((tablet|tab)[;\/]|focus\/\d(?!.+mobile))/i], [[i, d]], [/(phone|mobile(?:[;\/]| [ \w\/\.]*safari)|pda(?=.+windows ce))/i], [[i, m]], [/(android[-\w\. ]{0,9});.+buil/i], [n, [s, "Generic"]]],
			engine: [[/windows.+ edge\/([\w\.]+)/i], [R, [a, mi + "HTML"]], [/webkit\/537\.36.+chrome\/(?!27)([\w\.]+)/i], [R, [a, "Blink"]], [/(presto)\/([\w\.]+)/i, /(webkit|trident|netfront|netsurf|amaya|lynx|w3m|goanna)\/([\w\.]+)/i, /ekioh(flow)\/([\w\.]+)/i, /(khtml|tasman|links)[\/ ]\(?([\w\.]+)/i, /(icab)[\/ ]([23]\.[\d\.]+)/i, /\b(libweb)/i], [a, R], [/rv\:([\w\.]{1,9})\b.+(gecko)/i], [R, a]],
			os: [[/microsoft (windows) (vista|xp)/i], [a, R], [/(windows) nt 6\.2; (arm)/i, /(windows (?:phone(?: os)?|mobile))[\/ ]?([\d\.\w ]*)/i, /(windows)[\/ ]?([ntce\d\. ]+\w)(?!.+xbox)/i], [a, [R, Ye, qe]], [/(win(?=3|9|n)|win 9x )([nt\d\.]+)/i], [[a, "Windows"], [R, Ye, qe]], [/ip[honead]{2,4}\b(?:.*os ([\w]+) like mac|; opera)/i, /ios;fbsv\/([\d\.]+)/i, /cfnetwork\/.+darwin/i], [[R, /_/g, "."], [a, "iOS"]], [/(mac os x) ?([\w\. ]*)/i, /(macintosh|mac_powerpc\b)(?!.+haiku)/i], [[a, je], [R, /_/g, "."]], [/droid ([\w\.]+)\b.+(android[- ]x86|harmonyos)/i], [R, a], [/(android|webos|qnx|bada|rim tablet os|maemo|meego|sailfish)[-\/ ]?([\w\.]*)/i, /(blackberry)\w*\/([\w\.]*)/i, /(tizen|kaios)[\/ ]([\w\.]+)/i, /\((series40);/i], [a, R], [/\(bb(10);/i], [R, [a, ke]], [/(?:symbian ?os|symbos|s60(?=;)|series60)[-\/ ]?([\w\.]*)/i], [R, [a, "Symbian"]], [/mozilla\/[\d\.]+ \((?:mobile|tablet|tv|mobile; [\w ]+); rv:.+ gecko\/([\w\.]+)/i], [R, [a, se + " OS"]], [/web0s;.+rt(tv)/i, /\b(?:hp)?wos(?:browser)?\/([\w\.]+)/i], [R, [a, "webOS"]], [/watch(?: ?os[,\/]|\d,\d\/)([\d\.]+)/i], [R, [a, "watchOS"]], [/crkey\/([\d\.]+)/i], [R, [a, oe + "cast"]], [/(cros) [\w]+(?:\)| ([\w\.]+)\b)/i], [[a, He], R], [/panasonic;(viera)/i, /(netrange)mmh/i, /(nettv)\/(\d+\.[\w\.]+)/i, /(nintendo|playstation) ([wids345portablevuch]+)/i, /(xbox); +xbox ([^\);]+)/i, /\b(joli|palm)\b ?(?:os)?\/?([\w\.]*)/i, /(mint)[\/\(\) ]?(\w*)/i, /(mageia|vectorlinux)[; ]/i, /([kxln]?ubuntu|debian|suse|opensuse|gentoo|arch(?= linux)|slackware|fedora|mandriva|centos|pclinuxos|red ?hat|zenwalk|linpus|raspbian|plan 9|minix|risc os|contiki|deepin|manjaro|elementary os|sabayon|linspire)(?: gnu\/linux)?(?: enterprise)?(?:[- ]linux)?(?:-gnu)?[-\/ ]?(?!chrom|package)([-\w\.]*)/i, /(hurd|linux) ?([\w\.]*)/i, /(gnu) ?([\w\.]*)/i, /\b([-frentopcghs]{0,5}bsd|dragonfly)[\/ ]?(?!amd|[ix346]{1,2}86)([\w\.]*)/i, /(haiku) (\w+)/i], [a, R], [/(sunos) ?([\w\.\d]*)/i], [[a, "Solaris"], R], [/((?:open)?solaris)[-\/ ]?([\w\.]*)/i, /(aix) ((\d)(?=\.|\)| )[\w\.])*/i, /\b(beos|os\/2|amigaos|morphos|openvms|fuchsia|hp-ux|serenityos)/i, /(unix) ?([\w\.]*)/i], [a, R]]
		}, v = function(y, U) {
			if(typeof y === N && (U = y, y = e), !(this instanceof v)) return new v(y, U).getResult();
			var T = typeof t !== c && t.navigator ? t.navigator : e, u = y || (T && T.userAgent ? T.userAgent : o), H = T && T.userAgentData ? T.userAgentData : e, k = U ? yi(ze, U) : ze, A = T && T.userAgent == u;
			return this.getBrowser = function() {
				var I = {};
				return I[a] = e, I[R] = e, $.call(I, u, k.browser), I[_] = Ai(I[R]), A && T && T.brave && typeof T.brave.isBrave == C && (I[a] = "Brave"), I
			}, this.getCPU = function() {
				var I = {};
				return I[g] = e, $.call(I, u, k.cpu), I
			}, this.getDevice = function() {
				var I = {};
				return I[s] = e, I[n] = e, I[i] = e, $.call(I, u, k.device), A && !I[i] && H && H.mobile && (I[i] = m), A && I[n] == "Macintosh" && T && typeof T.standalone !== c && T.maxTouchPoints && T.maxTouchPoints > 2 && (I[n] = "iPad", I[i] = d), I
			}, this.getEngine = function() {
				var I = {};
				return I[a] = e, I[R] = e, $.call(I, u, k.engine), I
			}, this.getOS = function() {
				var I = {};
				return I[a] = e, I[R] = e, $.call(I, u, k.os), A && !I[a] && H && H.platform != "Unknown" && (I[a] = H.platform.replace(/chrome os/i, He).replace(/macos/i, je)), I
			}, this.getResult = function() {
				return {ua: this.getUA(), browser: this.getBrowser(), engine: this.getEngine(), os: this.getOS(), device: this.getDevice(), cpu: this.getCPU()}
			}, this.getUA = function() {
				return u
			}, this.setUA = function(I) {
				return u = typeof I === E && I.length > Ue ? fe(I, Ue) : I, this
			}, this.setUA(u), this
		};
		v.VERSION = r, v.BROWSER = Re([a, R, _]), v.CPU = Re([g]), v.DEVICE = Re([n, s, i, W, m, b, d, q, Ae]), v.ENGINE = v.OS = Re([a, R]), typeof ee !== c ? (typeof Ce !== c && Ce.exports && (ee = Ce.exports = v), ee.UAParser = v) : typeof define === C && define.amd ? define(function() {
			return v
		}) : typeof t !== c && (t.UAParser = v);
		var X = typeof t !== c && (t.jQuery || t.Zepto);
		if(X && !X.ua) {
			var Ee = new v;
			X.ua = Ee.getResult(), X.ua.get = function() {
				return Ee.getUA()
			}, X.ua.set = function(y) {
				Ee.setUA(y);
				var U = Ee.getResult();
				for(var T in U) X.ua[T] = U[T]
			}
		}
	})(typeof window == "object" ? window : ee)
});
var he = {};
We(he, {loadIssueBillingKeyUI: () => ar, loadIssueBillingKeyUIRequest: () => te, loadPaymentUI: () => or, requestIdentityVerification: () => er, requestIssueBillingKey: () => rr, requestIssueBillingKeyAndPay: () => tr, requestPayment: () => ci, updateLoadIssueBillingKeyUIRequest: () => di, updateLoadPaymentUIRequest: () => Ci});
var Be;
var Rr = {jsSdkUrl: (Be = void 0) !== null && Be !== void 0 ? Be : "https://cdn.portone.io/v2/browser-sdk.js"};
var Ki = {
		BANK_OF_KOREA: "BANK_BANK_OF_KOREA",
		KOREA_DEVELOPMENT_BANK: "BANK_KOREA_DEVELOPMENT_BANK",
		INDUSTRIAL_BANK_OF_KOREA: "BANK_INDUSTRIAL_BANK_OF_KOREA",
		KOOKMIN_BANK: "BANK_KOOKMIN_BANK",
		SUHYUP_BANK: "BANK_SUHYUP_BANK",
		EXPORT_IMPORT_BANK_OF_KOREA: "BANK_EXPORT_IMPORT_BANK_OF_KOREA",
		NH_NONGHYUP_BANK: "BANK_NH_NONGHYUP_BANK",
		LOCAL_NONGHYUP: "BANK_LOCAL_NONGHYUP",
		WOORI_BANK: "BANK_WOORI_BANK",
		SC_BANK_KOREA: "BANK_SC_BANK_KOREA",
		CITI_BANK_KOREA: "BANK_CITI_BANK_KOREA",
		DAEGU_BANK: "BANK_DAEGU_BANK",
		BUSAN_BANK: "BANK_BUSAN_BANK",
		GWANGJU_BANK: "BANK_GWANGJU_BANK",
		JEJU_BANK: "BANK_JEJU_BANK",
		JEONBUK_BANK: "BANK_JEONBUK_BANK",
		KYONGNAM_BANK: "BANK_KYONGNAM_BANK",
		KFCC: "BANK_KFCC",
		SHINHYUP: "BANK_SHINHYUP",
		SAVINGS_BANK_KOREA: "BANK_SAVINGS_BANK_KOREA",
		MORGAN_STANLEY_BANK: "BANK_MORGAN_STANLEY_BANK",
		HSBC_BANK: "BANK_HSBC_BANK",
		DEUTSCHE_BANK: "BANK_DEUTSCHE_BANK",
		JP_MORGAN_CHASE_BANK: "BANK_JP_MORGAN_CHASE_BANK",
		MIZUHO_BANK: "BANK_MIZUHO_BANK",
		MUFG_BANK: "BANK_MUFG_BANK",
		BANK_OF_AMERICA_BANK: "BANK_BANK_OF_AMERICA_BANK",
		BNP_PARIBAS_BANK: "BANK_BNP_PARIBAS_BANK",
		ICBC: "BANK_ICBC",
		BANK_OF_CHINA: "BANK_BANK_OF_CHINA",
		NATIONAL_FORESTRY_COOPERATIVE_FEDERATION: "BANK_NATIONAL_FORESTRY_COOPERATIVE_FEDERATION",
		UNITED_OVERSEAS_BANK: "BANK_UNITED_OVERSEAS_BANK",
		BANK_OF_COMMUNICATIONS: "BANK_BANK_OF_COMMUNICATIONS",
		CHINA_CONSTRUCTION_BANK: "BANK_CHINA_CONSTRUCTION_BANK",
		EPOST: "BANK_EPOST",
		KODIT: "BANK_KODIT",
		KIBO: "BANK_KIBO",
		HANA_BANK: "BANK_HANA_BANK",
		SHINHAN_BANK: "BANK_SHINHAN_BANK",
		K_BANK: "BANK_K_BANK",
		KAKAO_BANK: "BANK_KAKAO_BANK",
		TOSS_BANK: "BANK_TOSS_BANK",
		KCIS: "BANK_KCIS",
		DAISHIN_SAVINGS_BANK: "BANK_DAISHIN_SAVINGS_BANK",
		SBI_SAVINGS_BANK: "BANK_SBI_SAVINGS_BANK",
		HK_SAVINGS_BANK: "BANK_HK_SAVINGS_BANK",
		WELCOME_SAVINGS_BANK: "BANK_WELCOME_SAVINGS_BANK",
		SHINHAN_SAVINGS_BANK: "BANK_SHINHAN_SAVINGS_BANK",
		KYOBO_SECURITIES: "BANK_KYOBO_SECURITIES",
		DAISHIN_SECURITIES: "BANK_DAISHIN_SECURITIES",
		MERITZ_SECURITIES: "BANK_MERITZ_SECURITIES",
		MIRAE_ASSET_SECURITIES: "BANK_MIRAE_ASSET_SECURITIES",
		BOOKOOK_SECURITIES: "BANK_BOOKOOK_SECURITIES",
		SAMSUNG_SECURITIES: "BANK_SAMSUNG_SECURITIES",
		SHINYOUNG_SECURITIES: "BANK_SHINYOUNG_SECURITIES",
		SHINHAN_FINANCIAL_INVESTMENT: "BANK_SHINHAN_FINANCIAL_INVESTMENT",
		YUANTA_SECURITIES: "BANK_YUANTA_SECURITIES",
		EUGENE_INVESTMENT_SECURITIES: "BANK_EUGENE_INVESTMENT_SECURITIES",
		KAKAO_PAY_SECURITIES: "BANK_KAKAO_PAY_SECURITIES",
		TOSS_SECURITIES: "BANK_TOSS_SECURITIES",
		KOREA_FOSS_SECURITIES: "BANK_KOREA_FOSS_SECURITIES",
		HANA_FINANCIAL_INVESTMENT: "BANK_HANA_FINANCIAL_INVESTMENT",
		HI_INVESTMENT_SECURITIES: "BANK_HI_INVESTMENT_SECURITIES",
		KOREA_INVESTMENT_SECURITIES: "BANK_KOREA_INVESTMENT_SECURITIES",
		HANWHA_INVESTMENT_SECURITIES: "BANK_HANWHA_INVESTMENT_SECURITIES",
		HYUNDAI_MOTOR_SECURITIES: "BANK_HYUNDAI_MOTOR_SECURITIES",
		DB_FINANCIAL_INVESTMENT: "BANK_DB_FINANCIAL_INVESTMENT",
		KB_SECURITIES: "BANK_KB_SECURITIES",
		KTB_INVESTMENT_SECURITIES: "BANK_KTB_INVESTMENT_SECURITIES",
		NH_INVESTMENT_SECURITIES: "BANK_NH_INVESTMENT_SECURITIES",
		SK_SECURITIES: "BANK_SK_SECURITIES",
		SCI: "BANK_SGI",
		KIWOOM_SECURITIES: "BANK_KIWOOM_SECURITIES",
		EBEST_INVESTMENT_SECURITIES: "BANK_EBEST_INVESTMENT_SECURITIES",
		CAPE_INVESTMENT_CERTIFICATE: "BANK_CAPE_INVESTMENT_CERTIFICATE"
	}, Bi = {MOBILE: "MOBILE"}, Di = {CARD: "CARD", MOBILE: "MOBILE", EASY_PAY: "EASY_PAY", PAYPAL: "PAYPAL"},
	bi = {KOREA_DEVELOPMENT_BANK: "CARD_COMPANY_KOREA_DEVELOPMENT_BANK", KFCC: "CARD_COMPANY_KFCC", SHINHYUP: "CARD_COMPANY_SHINHYUP", EPOST: "CARD_COMPANY_EPOST", SAVINGS_BANK_KOREA: "CARD_COMPANY_SAVINGS_BANK_KOREA", KAKAO_BANK: "CARD_COMPANY_KAKAO_BANK", WOORI_CARD: "CARD_COMPANY_WOORI_CARD", BC_CARD: "CARD_COMPANY_BC_CARD", GWANGJU_CARD: "CARD_COMPANY_GWANGJU_CARD", SAMSUNG_CARD: "CARD_COMPANY_SAMSUNG_CARD", SHINHAN_CARD: "CARD_COMPANY_SHINHAN_CARD", HYUNDAI_CARD: "CARD_COMPANY_HYUNDAI_CARD", LOTTE_CARD: "CARD_COMPANY_LOTTE_CARD", SUHYUP_CARD: "CARD_COMPANY_SUHYUP_CARD", CITI_CARD: "CARD_COMPANY_CITI_CARD", NH_CARD: "CARD_COMPANY_NH_CARD", JEONBUK_CARD: "CARD_COMPANY_JEONBUK_CARD", JEJU_CARD: "CARD_COMPANY_JEJU_CARD", HANA_CARD: "CARD_COMPANY_HANA_CARD", KOOKMIN_CARD: "CARD_COMPANY_KOOKMIN_CARD", K_BANK: "CARD_COMPANY_K_BANK", TOSS_BANK: "CARD_COMPANY_TOSS_BANK", MIRAE_ASSET_SECURITIES: "CARD_COMPANY_MIRAE_ASSET_SECURITIES"},
	vi = {SKT: "CARRIER_SKT", KT: "CARRIER_KT", LGU: "CARRIER_LGU", HELLO: "CARRIER_HELLO", KCT: "CARRIER_KCT", SK7: "CARRIER_SK7"}, Li = {
		AF: "COUNTRY_AF",
		AX: "COUNTRY_AX",
		AL: "COUNTRY_AL",
		DZ: "COUNTRY_DZ",
		AS: "COUNTRY_AS",
		AD: "COUNTRY_AD",
		AO: "COUNTRY_AO",
		AI: "COUNTRY_AI",
		AQ: "COUNTRY_AQ",
		AG: "COUNTRY_AG",
		AR: "COUNTRY_AR",
		AM: "COUNTRY_AM",
		AW: "COUNTRY_AW",
		AU: "COUNTRY_AU",
		AT: "COUNTRY_AT",
		AZ: "COUNTRY_AZ",
		BH: "COUNTRY_BH",
		BS: "COUNTRY_BS",
		BD: "COUNTRY_BD",
		BB: "COUNTRY_BB",
		BY: "COUNTRY_BY",
		BE: "COUNTRY_BE",
		BZ: "COUNTRY_BZ",
		BJ: "COUNTRY_BJ",
		BM: "COUNTRY_BM",
		BT: "COUNTRY_BT",
		BO: "COUNTRY_BO",
		BQ: "COUNTRY_BQ",
		BA: "COUNTRY_BA",
		BW: "COUNTRY_BW",
		BV: "COUNTRY_BV",
		BR: "COUNTRY_BR",
		IO: "COUNTRY_IO",
		BN: "COUNTRY_BN",
		BG: "COUNTRY_BG",
		BF: "COUNTRY_BF",
		BI: "COUNTRY_BI",
		KH: "COUNTRY_KH",
		CM: "COUNTRY_CM",
		CA: "COUNTRY_CA",
		CV: "COUNTRY_CV",
		KY: "COUNTRY_KY",
		CF: "COUNTRY_CF",
		TD: "COUNTRY_TD",
		CL: "COUNTRY_CL",
		CN: "COUNTRY_CN",
		CX: "COUNTRY_CX",
		CC: "COUNTRY_CC",
		CO: "COUNTRY_CO",
		KM: "COUNTRY_KM",
		CG: "COUNTRY_CG",
		CD: "COUNTRY_CD",
		CK: "COUNTRY_CK",
		CR: "COUNTRY_CR",
		CI: "COUNTRY_CI",
		HR: "COUNTRY_HR",
		CU: "COUNTRY_CU",
		CW: "COUNTRY_CW",
		CY: "COUNTRY_CY",
		CZ: "COUNTRY_CZ",
		DK: "COUNTRY_DK",
		DJ: "COUNTRY_DJ",
		DM: "COUNTRY_DM",
		DO: "COUNTRY_DO",
		EC: "COUNTRY_EC",
		EG: "COUNTRY_EG",
		SV: "COUNTRY_SV",
		GQ: "COUNTRY_GQ",
		ER: "COUNTRY_ER",
		EE: "COUNTRY_EE",
		ET: "COUNTRY_ET",
		FK: "COUNTRY_FK",
		FO: "COUNTRY_FO",
		FJ: "COUNTRY_FJ",
		FI: "COUNTRY_FI",
		FR: "COUNTRY_FR",
		GF: "COUNTRY_GF",
		PF: "COUNTRY_PF",
		TF: "COUNTRY_TF",
		GA: "COUNTRY_GA",
		GM: "COUNTRY_GM",
		GE: "COUNTRY_GE",
		DE: "COUNTRY_DE",
		GH: "COUNTRY_GH",
		GI: "COUNTRY_GI",
		GR: "COUNTRY_GR",
		GL: "COUNTRY_GL",
		GD: "COUNTRY_GD",
		GP: "COUNTRY_GP",
		GU: "COUNTRY_GU",
		GT: "COUNTRY_GT",
		GG: "COUNTRY_GG",
		GN: "COUNTRY_GN",
		GW: "COUNTRY_GW",
		GY: "COUNTRY_GY",
		HT: "COUNTRY_HT",
		HM: "COUNTRY_HM",
		VA: "COUNTRY_VA",
		HN: "COUNTRY_HN",
		HK: "COUNTRY_HK",
		HU: "COUNTRY_HU",
		IS: "COUNTRY_IS",
		IN: "COUNTRY_IN",
		ID: "COUNTRY_ID",
		IR: "COUNTRY_IR",
		IQ: "COUNTRY_IQ",
		IE: "COUNTRY_IE",
		IM: "COUNTRY_IM",
		IL: "COUNTRY_IL",
		IT: "COUNTRY_IT",
		JM: "COUNTRY_JM",
		JP: "COUNTRY_JP",
		JE: "COUNTRY_JE",
		JO: "COUNTRY_JO",
		KZ: "COUNTRY_KZ",
		KE: "COUNTRY_KE",
		KI: "COUNTRY_KI",
		KP: "COUNTRY_KP",
		KR: "COUNTRY_KR",
		KW: "COUNTRY_KW",
		KG: "COUNTRY_KG",
		LA: "COUNTRY_LA",
		LV: "COUNTRY_LV",
		LB: "COUNTRY_LB",
		LS: "COUNTRY_LS",
		LR: "COUNTRY_LR",
		LY: "COUNTRY_LY",
		LI: "COUNTRY_LI",
		LT: "COUNTRY_LT",
		LU: "COUNTRY_LU",
		MO: "COUNTRY_MO",
		MK: "COUNTRY_MK",
		MG: "COUNTRY_MG",
		MW: "COUNTRY_MW",
		MY: "COUNTRY_MY",
		MV: "COUNTRY_MV",
		ML: "COUNTRY_ML",
		MT: "COUNTRY_MT",
		MH: "COUNTRY_MH",
		MQ: "COUNTRY_MQ",
		MR: "COUNTRY_MR",
		MU: "COUNTRY_MU",
		YT: "COUNTRY_YT",
		MX: "COUNTRY_MX",
		FM: "COUNTRY_FM",
		MD: "COUNTRY_MD",
		MC: "COUNTRY_MC",
		MN: "COUNTRY_MN",
		ME: "COUNTRY_ME",
		MS: "COUNTRY_MS",
		MA: "COUNTRY_MA",
		MZ: "COUNTRY_MZ",
		MM: "COUNTRY_MM",
		NA: "COUNTRY_NA",
		NR: "COUNTRY_NR",
		NP: "COUNTRY_NP",
		NL: "COUNTRY_NL",
		NC: "COUNTRY_NC",
		NZ: "COUNTRY_NZ",
		NI: "COUNTRY_NI",
		NE: "COUNTRY_NE",
		NG: "COUNTRY_NG",
		NU: "COUNTRY_NU",
		NF: "COUNTRY_NF",
		MP: "COUNTRY_MP",
		NO: "COUNTRY_NO",
		OM: "COUNTRY_OM",
		PK: "COUNTRY_PK",
		PW: "COUNTRY_PW",
		PS: "COUNTRY_PS",
		PA: "COUNTRY_PA",
		PG: "COUNTRY_PG",
		PY: "COUNTRY_PY",
		PE: "COUNTRY_PE",
		PH: "COUNTRY_PH",
		PN: "COUNTRY_PN",
		PL: "COUNTRY_PL",
		PT: "COUNTRY_PT",
		PR: "COUNTRY_PR",
		QA: "COUNTRY_QA",
		RE: "COUNTRY_RE",
		RO: "COUNTRY_RO",
		RU: "COUNTRY_RU",
		RW: "COUNTRY_RW",
		BL: "COUNTRY_BL",
		SH: "COUNTRY_SH",
		KN: "COUNTRY_KN",
		LC: "COUNTRY_LC",
		MF: "COUNTRY_MF",
		PM: "COUNTRY_PM",
		VC: "COUNTRY_VC",
		WS: "COUNTRY_WS",
		SM: "COUNTRY_SM",
		ST: "COUNTRY_ST",
		SA: "COUNTRY_SA",
		SN: "COUNTRY_SN",
		RS: "COUNTRY_RS",
		SC: "COUNTRY_SC",
		SL: "COUNTRY_SL",
		SG: "COUNTRY_SG",
		SX: "COUNTRY_SX",
		SK: "COUNTRY_SK",
		SI: "COUNTRY_SI",
		SB: "COUNTRY_SB",
		SO: "COUNTRY_SO",
		ZA: "COUNTRY_ZA",
		GS: "COUNTRY_GS",
		SS: "COUNTRY_SS",
		ES: "COUNTRY_ES",
		LK: "COUNTRY_LK",
		SD: "COUNTRY_SD",
		SR: "COUNTRY_SR",
		SJ: "COUNTRY_SJ",
		SZ: "COUNTRY_SZ",
		SE: "COUNTRY_SE",
		CH: "COUNTRY_CH",
		SY: "COUNTRY_SY",
		TW: "COUNTRY_TW",
		TJ: "COUNTRY_TJ",
		TZ: "COUNTRY_TZ",
		TH: "COUNTRY_TH",
		TL: "COUNTRY_TL",
		TG: "COUNTRY_TG",
		TK: "COUNTRY_TK",
		TO: "COUNTRY_TO",
		TT: "COUNTRY_TT",
		TN: "COUNTRY_TN",
		TR: "COUNTRY_TR",
		TM: "COUNTRY_TM",
		TC: "COUNTRY_TC",
		TV: "COUNTRY_TV",
		UG: "COUNTRY_UG",
		UA: "COUNTRY_UA",
		AE: "COUNTRY_AE",
		GB: "COUNTRY_GB",
		US: "COUNTRY_US",
		UM: "COUNTRY_UM",
		UY: "COUNTRY_UY",
		UZ: "COUNTRY_UZ",
		VU: "COUNTRY_VU",
		VE: "COUNTRY_VE",
		VN: "COUNTRY_VN",
		VG: "COUNTRY_VG",
		VI: "COUNTRY_VI",
		WF: "COUNTRY_WF",
		EH: "COUNTRY_EH",
		YE: "COUNTRY_YE",
		ZM: "COUNTRY_ZM",
		ZW: "COUNTRY_ZW"
	}, hi = {
		KRW: "CURRENCY_KRW",
		USD: "CURRENCY_USD",
		EUR: "CURRENCY_EUR",
		JPY: "CURRENCY_JPY",
		CNY: "CURRENCY_CNY",
		VND: "CURRENCY_VND",
		THB: "CURRENCY_THB",
		SGD: "CURRENCY_SGD",
		AUD: "CURRENCY_AUD",
		HKD: "CURRENCY_HKD",
		AED: "CURRENCY_AED",
		AFN: "CURRENCY_AFN",
		ALL: "CURRENCY_ALL",
		AMD: "CURRENCY_AMD",
		ANG: "CURRENCY_ANG",
		AOA: "CURRENCY_AOA",
		ARS: "CURRENCY_ARS",
		AWG: "CURRENCY_AWG",
		AZN: "CURRENCY_AZN",
		BAM: "CURRENCY_BAM",
		BBD: "CURRENCY_BBD",
		BDT: "CURRENCY_BDT",
		BGN: "CURRENCY_BGN",
		BHD: "CURRENCY_BHD",
		BIF: "CURRENCY_BIF",
		BMD: "CURRENCY_BMD",
		BND: "CURRENCY_BND",
		BOB: "CURRENCY_BOB",
		BOV: "CURRENCY_BOV",
		BRL: "CURRENCY_BRL",
		BSD: "CURRENCY_BSD",
		BTN: "CURRENCY_BTN",
		BWP: "CURRENCY_BWP",
		BYN: "CURRENCY_BYN",
		BZD: "CURRENCY_BZD",
		CAD: "CURRENCY_CAD",
		CDF: "CURRENCY_CDF",
		CHE: "CURRENCY_CHE",
		CHF: "CURRENCY_CHF",
		CHW: "CURRENCY_CHW",
		CLF: "CURRENCY_CLF",
		CLP: "CURRENCY_CLP",
		COP: "CURRENCY_COP",
		COU: "CURRENCY_COU",
		CRC: "CURRENCY_CRC",
		CUC: "CURRENCY_CUC",
		CUP: "CURRENCY_CUP",
		CVE: "CURRENCY_CVE",
		CZK: "CURRENCY_CZK",
		DJF: "CURRENCY_DJF",
		DKK: "CURRENCY_DKK",
		DOP: "CURRENCY_DOP",
		DZD: "CURRENCY_DZD",
		EGP: "CURRENCY_EGP",
		ERN: "CURRENCY_ERN",
		ETB: "CURRENCY_ETB",
		FJD: "CURRENCY_FJD",
		FKP: "CURRENCY_FKP",
		GBP: "CURRENCY_GBP",
		GEL: "CURRENCY_GEL",
		GHS: "CURRENCY_GHS",
		GIP: "CURRENCY_GIP",
		GMD: "CURRENCY_GMD",
		GNF: "CURRENCY_GNF",
		GTQ: "CURRENCY_GTQ",
		GYD: "CURRENCY_GYD",
		HNL: "CURRENCY_HNL",
		HRK: "CURRENCY_HRK",
		HTG: "CURRENCY_HTG",
		HUF: "CURRENCY_HUF",
		IDR: "CURRENCY_IDR",
		ILS: "CURRENCY_ILS",
		INR: "CURRENCY_INR",
		IQD: "CURRENCY_IQD",
		IRR: "CURRENCY_IRR",
		ISK: "CURRENCY_ISK",
		JMD: "CURRENCY_JMD",
		JOD: "CURRENCY_JOD",
		KES: "CURRENCY_KES",
		KGS: "CURRENCY_KGS",
		KHR: "CURRENCY_KHR",
		KMF: "CURRENCY_KMF",
		KPW: "CURRENCY_KPW",
		KWD: "CURRENCY_KWD",
		KYD: "CURRENCY_KYD",
		KZT: "CURRENCY_KZT",
		LAK: "CURRENCY_LAK",
		LBP: "CURRENCY_LBP",
		LKR: "CURRENCY_LKR",
		LRD: "CURRENCY_LRD",
		LSL: "CURRENCY_LSL",
		LYD: "CURRENCY_LYD",
		MAD: "CURRENCY_MAD",
		MDL: "CURRENCY_MDL",
		MGA: "CURRENCY_MGA",
		MKD: "CURRENCY_MKD",
		MMK: "CURRENCY_MMK",
		MNT: "CURRENCY_MNT",
		MOP: "CURRENCY_MOP",
		MRU: "CURRENCY_MRU",
		MUR: "CURRENCY_MUR",
		MVR: "CURRENCY_MVR",
		MWK: "CURRENCY_MWK",
		MXN: "CURRENCY_MXN",
		MXV: "CURRENCY_MXV",
		MZN: "CURRENCY_MZN",
		NAD: "CURRENCY_NAD",
		NGN: "CURRENCY_NGN",
		NIO: "CURRENCY_NIO",
		NOK: "CURRENCY_NOK",
		NPR: "CURRENCY_NPR",
		NZD: "CURRENCY_NZD",
		OMR: "CURRENCY_OMR",
		PAB: "CURRENCY_PAB",
		PEN: "CURRENCY_PEN",
		PGK: "CURRENCY_PGK",
		PHP: "CURRENCY_PHP",
		PKR: "CURRENCY_PKR",
		PLN: "CURRENCY_PLN",
		PYG: "CURRENCY_PYG",
		QAR: "CURRENCY_QAR",
		RON: "CURRENCY_RON",
		RSD: "CURRENCY_RSD",
		RUB: "CURRENCY_RUB",
		RWF: "CURRENCY_RWF",
		SAR: "CURRENCY_SAR",
		SBD: "CURRENCY_SBD",
		SCR: "CURRENCY_SCR",
		SDG: "CURRENCY_SDG",
		SEK: "CURRENCY_SEK",
		SHP: "CURRENCY_SHP",
		SLE: "CURRENCY_SLE",
		SLL: "CURRENCY_SLL",
		SOS: "CURRENCY_SOS",
		SRD: "CURRENCY_SRD",
		SSP: "CURRENCY_SSP",
		STN: "CURRENCY_STN",
		SVC: "CURRENCY_SVC",
		SYP: "CURRENCY_SYP",
		SZL: "CURRENCY_SZL",
		TJS: "CURRENCY_TJS",
		TMT: "CURRENCY_TMT",
		TND: "CURRENCY_TND",
		TOP: "CURRENCY_TOP",
		TRY: "CURRENCY_TRY",
		TTD: "CURRENCY_TTD",
		TWD: "CURRENCY_TWD",
		TZS: "CURRENCY_TZS",
		UAH: "CURRENCY_UAH",
		UGX: "CURRENCY_UGX",
		USN: "CURRENCY_USN",
		UYI: "CURRENCY_UYI",
		UYU: "CURRENCY_UYU",
		UYW: "CURRENCY_UYW",
		UZS: "CURRENCY_UZS",
		VED: "CURRENCY_VED",
		VES: "CURRENCY_VES",
		VUV: "CURRENCY_VUV",
		WST: "CURRENCY_WST",
		XAF: "CURRENCY_XAF",
		XAG: "CURRENCY_XAG",
		XAU: "CURRENCY_XAU",
		XBA: "CURRENCY_XBA",
		XBB: "CURRENCY_XBB",
		XBC: "CURRENCY_XBC",
		XBD: "CURRENCY_XBD",
		XCD: "CURRENCY_XCD",
		XDR: "CURRENCY_XDR",
		XOF: "CURRENCY_XOF",
		XPD: "CURRENCY_XPD",
		XPF: "CURRENCY_XPF",
		XPT: "CURRENCY_XPT",
		XSU: "CURRENCY_XSU",
		XTS: "CURRENCY_XTS",
		XUA: "CURRENCY_XUA",
		XXX: "CURRENCY_XXX",
		YER: "CURRENCY_YER",
		ZAR: "CURRENCY_ZAR",
		ZMW: "CURRENCY_ZMW",
		ZWL: "CURRENCY_ZWL"
	}, Mi = {PAYCO: "EASY_PAY_PROVIDER_PAYCO", SAMSUNGPAY: "EASY_PAY_PROVIDER_SAMSUNGPAY", SSGPAY: "EASY_PAY_PROVIDER_SSGPAY", KAKAOPAY: "EASY_PAY_PROVIDER_KAKAOPAY", NAVERPAY: "EASY_PAY_PROVIDER_NAVERPAY", CHAI: "EASY_PAY_PROVIDER_CHAI", LPAY: "EASY_PAY_PROVIDER_LPAY", KPAY: "EASY_PAY_PROVIDER_KPAY", TOSSPAY: "EASY_PAY_PROVIDER_TOSSPAY", LGPAY: "EASY_PAY_PROVIDER_LGPAY", APPLEPAY: "EASY_PAY_PROVIDER_APPLEPAY", PINPAY: "EASY_PAY_PROVIDER_PINPAY", SKPAY: "EASY_PAY_PROVIDER_SKPAY", TOSS_BRANDPAY: "EASY_PAY_PROVIDER_TOSS_BRANDPAY"}, ki = {MALE: "GENDER_MALE", FEMALE: "GENDER_FEMALE", OTHER: "GENDER_OTHER"}, Vi = {BOOKNLIFE: "GIFT_CERTIFICATE_TYPE_BOOKNLIFE", SMART_MUNSANG: "GIFT_CERTIFICATE_TYPE_SMART_MUNSANG", CULTURELAND: "GIFT_CERTIFICATE_TYPE_CULTURELAND", HAPPYMONEY: "GIFT_CERTIFICATE_TYPE_HAPPYMONEY", CULTURE_GIFT: "GIFT_CERTIFICATE_TYPE_CULTURE_GIFT"}, Gi = {KO_KR: "KO_KR", EN_US: "EN_US", ZH_CN: "ZH_CN"}, Fi = {
		HTML5_INICIS: "PG_PROVIDER_HTML5_INICIS",
		PAYPAL: "PG_PROVIDER_PAYPAL",
		INICIS: "PG_PROVIDER_INICIS",
		DANAL: "PG_PROVIDER_DANAL",
		NICE: "PG_PROVIDER_NICE",
		DANAL_TPAY: "PG_PROVIDER_DANAL_TPAY",
		JTNET: "PG_PROVIDER_JTNET",
		UPLUS: "PG_PROVIDER_UPLUS",
		NAVERPAY: "PG_PROVIDER_NAVERPAY",
		KAKAO: "PG_PROVIDER_KAKAO",
		SETTLE: "PG_PROVIDER_SETTLE",
		KCP: "PG_PROVIDER_KCP",
		MOBILIANS: "PG_PROVIDER_MOBILIANS",
		KAKAOPAY: "PG_PROVIDER_KAKAOPAY",
		NAVERCO: "PG_PROVIDER_NAVERCO",
		SYRUP: "PG_PROVIDER_SYRUP",
		KICC: "PG_PROVIDER_KICC",
		EXIMBAY: "PG_PROVIDER_EXIMBAY",
		SMILEPAY: "PG_PROVIDER_SMILEPAY",
		PAYCO: "PG_PROVIDER_PAYCO",
		KCP_BILLING: "PG_PROVIDER_KCP_BILLING",
		ALIPAY: "PG_PROVIDER_ALIPAY",
		PAYPLE: "PG_PROVIDER_PAYPLE",
		CHAI: "PG_PROVIDER_CHAI",
		BLUEWALNUT: "PG_PROVIDER_BLUEWALNUT",
		SMARTRO: "PG_PROVIDER_SMARTRO",
		PAYMENTWALL: "PG_PROVIDER_PAYMENTWALL",
		TOSSPAYMENTS: "PG_PROVIDER_TOSSPAYMENTS",
		KCP_QUICK: "PG_PROVIDER_KCP_QUICK",
		DAOU: "PG_PROVIDER_DAOU",
		GALAXIA: "PG_PROVIDER_GALAXIA",
		TOSSPAY: "PG_PROVIDER_TOSSPAY",
		KCP_DIRECT: "PG_PROVIDER_KCP_DIRECT",
		SETTLE_ACC: "PG_PROVIDER_SETTLE_ACC",
		SETTLE_FIRM: "PG_PROVIDER_SETTLE_FIRM",
		INICIS_UNIFIED: "PG_PROVIDER_INICIS_UNIFIED",
		KSNET: "PG_PROVIDER_KSNET",
		PAYPAL_V2: "PG_PROVIDER_PAYPAL_V2",
		SMARTRO_V2: "PG_PROVIDER_SMARTRO_V2",
		NICE_V2: "PG_PROVIDER_NICE_V2",
		TOSS_BRANDPAY: "PG_PROVIDER_TOSS_BRANDPAY",
		WELCOME: "PG_PROVIDER_WELCOME",
		TOSSPAY_V2: "PG_PROVIDER_TOSSPAY_V2",
		INICIS_V2: "PG_PROVIDER_INICIS_V2",
		KPN: "PG_PROVIDER_KPN"
	}, j = {PAYMENT: "PAYMENT", ISSUE_BILLING_KEY: "ISSUE_BILLING_KEY", IDENTITY_VERIFICATION: "IDENTITY_VERIFICATION", ISSUE_BILLING_KEY_AND_PAY: "ISSUE_BILLING_KEY_AND_PAY"}, xi = {IFRAME: "IFRAME", POPUP: "POPUP", REDIRECTION: "REDIRECTION", UI: "UI"}, Hi = {PAYPAL_SPB: "PAYPAL_SPB"}, ji = {PAYPAL_RT: "PAYPAL_RT"}, w = Object.freeze({__proto__: null, Bank: Ki, BillingKeyAndPayMethod: Bi, BillingKeyMethod: Di, CardCompany: bi, Carrier: vi, Country: Li, Currency: hi, EasyPayProvider: Mi, Gender: ki, GiftCertificateType: Vi, IssueBillingKeyUIType: ji, Locale: Gi, PaymentUIType: Hi, PgProvider: Fi, TransactionType: j, WindowType: xi});

function ce(t) {
	return le(t) && t.__portOneErrorType === "IdentityVerificationError"
}

var B = class extends Error {
	constructor({code: e, message: r, identityVerificationId: o, identityVerificationTxId: p}) {
		super(r), this.__portOneErrorType = "IdentityVerificationError", this.transactionType = j.IDENTITY_VERIFICATION, this.code = e, this.message = r, this.identityVerificationId = o, this.identityVerificationTxId = p
	}
};

function Ne(t) {
	return le(t) && t.__portOneErrorType === "IssueBillingKeyAndPayError"
}

var G = class extends Error {
	constructor({txId: e, paymentId: r, billingKey: o, code: p, message: C}) {
		super(C), this.__portOneErrorType = "IssueBillingKeyAndPayError", this.transactionType = j.ISSUE_BILLING_KEY_AND_PAY, this.txId = e, this.paymentId = r, this.billingKey = o, this.code = p, this.message = C
	}
};

function L(t) {
	return le(t) && t.__portOneErrorType === "IssueBillingKeyError"
}

var O = class extends Error {
	constructor({code: e, message: r, billingKey: o}) {
		super(r), this.__portOneErrorType = "IssueBillingKeyError", this.transactionType = j.ISSUE_BILLING_KEY, this.code = e, this.message = r, this.billingKey = o
	}
};

function h(t) {
	return le(t) && t.__portOneErrorType === "PaymentError"
}

var S = class extends Error {
	constructor({code: e, message: r, txId: o, paymentId: p}) {
		super(r), this.__portOneErrorType = "PaymentError", this.transactionType = j.PAYMENT, this.code = e, this.message = r, this.txId = o, this.paymentId = p
	}
};
var K = class extends Error {
	constructor({code: e, message: r}) {
		super(r), this.__portOneErrorType = "LoadIssueBillingKeyUIError", this.transactionType = j.ISSUE_BILLING_KEY, this.code = e, this.message = r
	}
};
var M = class extends Error {
	constructor({code: e, message: r}) {
		super(r), this.__portOneErrorType = "LoadPaymentUIError", this.transactionType = j.PAYMENT, this.code = e, this.message = r
	}
};

function le(t) {
	return t != null && typeof t == "object" && "__portOneErrorType" in t && typeof t.__portOneErrorType == "string"
}

var Ji = {CORE_SERVER: "https://service.iamport.kr", CORETELEMETRY_SERVER: "https://coretelemetry.prod.iamport.co", CHECKOUT_SERVER: "https://checkout-service.prod.iamport.co", DRIVER_SERVE_URL: "https://cdn.portone.io/drivers", PAYMENT_BRIDGE_URL: "https://payment-bridge-prod.vercel.app", SDK_VERSION: "1.19.0", TGS_PUBLIC_URI: "https://tx-gateway-service.prod.iamport.co"}, l = Ji;
var P = {
	"loadable-ui/payment/payment-bridge": "loadable-ui/payment/payment-bridge-BDPZGIbxqVSq8L3QknMlq5YGq7hjBEzgNSBLYYMq1Gw=.js",
	"pg/danal/identity-verification/popup-impl": "pg/danal/identity-verification/popup-impl-wT9pfe-65kpHxoxNvNz7kXg_FCdCUCPvzV5F9DT2DBk=.js",
	"pg/danal/identity-verification/popup": "pg/danal/identity-verification/popup-KToLQzxRo6v5M9ZRolwKEyS7dPaaqz7fOslJP8W3A38=.js",
	"pg/danal/identity-verification/redirection": "pg/danal/identity-verification/redirection-ul_5Llh0jNUGyRwGRN-biqYjSQBVqchKdGGnyF6Aa4I=.js",
	"pg/inicis-v2/issue-billing-key/iframe-impl": "pg/inicis-v2/issue-billing-key/iframe-impl-ZKlYsxRwRkgNDXSmmpR1Ekd1Zdj3byeOsnPyQnEvh7U=.js",
	"pg/inicis-v2/issue-billing-key/iframe": "pg/inicis-v2/issue-billing-key/iframe-Cu71m1rY9lz1HOOdHn7oYkeqXlAktjfjpLd5BGKvdnc=.js",
	"pg/inicis-v2/issue-billing-key/redirection": "pg/inicis-v2/issue-billing-key/redirection-nlZugfYC_vP4UeQfwHsqQdv4ukLbKNnh30gsKHfNTzI=.js",
	"pg/inicis-v2/issue-billing-key-and-pay/iframe-impl": "pg/inicis-v2/issue-billing-key-and-pay/iframe-impl-yyPp3BQBItKv4iFxIfnfb1f8IAHnFAXOKdUY3-PVFgM=.js",
	"pg/inicis-v2/issue-billing-key-and-pay/iframe": "pg/inicis-v2/issue-billing-key-and-pay/iframe-vX6oKsAY_ZPpLlMmX-TubYC_XwxRLEMKk_OtuDd9bRk=.js",
	"pg/inicis-v2/issue-billing-key-and-pay/redirection": "pg/inicis-v2/issue-billing-key-and-pay/redirection-oErrYDQzKNXvUoiAK9F6tngzjiphj5_sGcOH7SfSadc=.js",
	"pg/inicis-v2/payment/iframe-impl": "pg/inicis-v2/payment/iframe-impl-quhnOKhzLHbQpGuxcx_Ny_J_SvyC_l3PZSoLGFo9CrY=.js",
	"pg/inicis-v2/payment/iframe": "pg/inicis-v2/payment/iframe-0lVH62yqef7YFiB3AR9managementcJEZQAf6y9R-Rtjq2_xP8=.js",
	"pg/inicis-v2/payment/redirection": "pg/inicis-v2/payment/redirection-p86TlGRrxfnut6IOWSyQ4oXcBVWQL3rhOoVhr2FDh8U=.js",
	"pg/kakaopay/issue-billing-key/iframe-impl": "pg/kakaopay/issue-billing-key/iframe-impl-d2v13U4upFfGA85vB_F4xISztJFlG1nb62FGALsOIEY=.js",
	"pg/kakaopay/issue-billing-key/iframe": "pg/kakaopay/issue-billing-key/iframe-_xf8iBkQkBn35s67uKwCuT6G3szEnDBaRjSIvKoXxDM=.js",
	"pg/kakaopay/issue-billing-key/redirection": "pg/kakaopay/issue-billing-key/redirection-a97lBRhi37KEEIRMMDggDF_j1PkvUSip01T1Y20uTXk=.js",
	"pg/kakaopay/payment/iframe-impl": "pg/kakaopay/payment/iframe-impl-rHD1n_Gv2aNfbBJldfhfoz9Wx4CmMPEbFrU1lB6CcXA=.js",
	"pg/kakaopay/payment/iframe": "pg/kakaopay/payment/iframe-XF-uOpP9NFFHT8bgdNVaKwOeiPiVTNTQ9lcrJYX0KEM=.js",
	"pg/kakaopay/payment/redirection": "pg/kakaopay/payment/redirection--TzcxYxXGBfK77R6iEizyMJ7xfS0jHE5WzZEHhwp-SU=.js",
	"pg/kpn/issue-billing-key/iframe-impl": "pg/kpn/issue-billing-key/iframe-impl-7mHFNd1SF0NQMxdD9A2f1a3LDtt781Q0vMMOya6bV7Q=.js",
	"pg/kpn/issue-billing-key/iframe": "pg/kpn/issue-billing-key/iframe-V67HM5z0aDl1L0jxf9IP-W-7p-H_l7SjUpSndHEK4AI=.js",
	"pg/kpn/issue-billing-key/redirection": "pg/kpn/issue-billing-key/redirection-_U5N57Qcf6FrY0yt90naQ3vGB-GqGePx_WG2CCCBe70=.js",
	"pg/kpn/payment/iframe-impl": "pg/kpn/payment/iframe-impl-l2iMRhuWPJPH50kcjo_P-1uOCr91nVn_IZoYA1RQPGY=.js",
	"pg/kpn/payment/iframe": "pg/kpn/payment/iframe-REQ0IrjfvYltm9uE_yCLGxN6oHS6d6Oz8-gzNzJQA4A=.js",
	"pg/kpn/payment/redirection": "pg/kpn/payment/redirection-6h5z2KULnrgCLkGZsxFRItm2J3wHB2-1mYgCwP_JdPc=.js",
	"pg/ksnet/payment/iframe-impl": "pg/ksnet/payment/iframe-impl-pORZk_rdjvoXtAUFs3ozF8-FiW40aEMS2vYfEk4fzH0=.js",
	"pg/ksnet/payment/iframe": "pg/ksnet/payment/iframe-u_bsm0AOwSEBOtS9oRp3vl5tf69LLwuBViR21QZz6Ek=.js",
	"pg/ksnet/payment/redirection": "pg/ksnet/payment/redirection-tZarULlso-qW5677MOoUJxJu0zWbApoLjksKCUFfJzY=.js",
	"pg/naverpay/issue-billing-key/popup-impl": "pg/naverpay/issue-billing-key/popup-impl-1pyC_FOASXH2KWETslFMdhFwGBjrZ9hslu4-bMLMboc=.js",
	"pg/naverpay/issue-billing-key/popup": "pg/naverpay/issue-billing-key/popup-d1xxirzmYShn2-crPjS_-qOcNXJ48zZ1grfo_btbSuo=.js",
	"pg/naverpay/issue-billing-key/redirection": "pg/naverpay/issue-billing-key/redirection-yMN2ZlAU3NqXkcxcHI_oJg4Kk_H5QadiacN6J6tPMBo=.js",
	"pg/naverpay/payment/popup-impl": "pg/naverpay/payment/popup-impl-WNgQ2fB7SfvPvM3Z9zddbNsQ74kxLB7Jh2NazDm0QQk=.js",
	"pg/naverpay/payment/popup": "pg/naverpay/payment/popup--JDWyvp9ux8K_TGgHNGRsA1XEDiFAmENBmQIQ5t55gY=.js",
	"pg/naverpay/payment/redirection": "pg/naverpay/payment/redirection-K1b-7NYpN0ENwWdd9JjYLJAsR2BOtQXWIvzUL_oGbE4=.js",
	"pg/nice-v2/issue-billing-key/iframe-impl": "pg/nice-v2/issue-billing-key/iframe-impl-2gR_rOIbGoDw_FHY09uV6OGGjHdI6PrARBG3G646ZVc=.js",
	"pg/nice-v2/issue-billing-key/iframe": "pg/nice-v2/issue-billing-key/iframe-fj2Gw-iK2DtNgJ3Ya3ENb_Au5PHier0iQyTZRuIgEMc=.js",
	"pg/nice-v2/issue-billing-key/redirection": "pg/nice-v2/issue-billing-key/redirection-wIMBrNhCo4CM2AMs2Wdn65aGrj7Wrb97FIJRo9h05C8=.js",
	"pg/nice-v2/payment/iframe-impl": "pg/nice-v2/payment/iframe-impl-Ykoi_Jq9IEEJE33GmnmA97379qZlZMQu9GYoodPhqZ0=.js",
	"pg/nice-v2/payment/iframe": "pg/nice-v2/payment/iframe-XbEzhoayC1A6kMLnOUD6avoNH3Cjm1ncncKFtBq2TLg=.js",
	"pg/nice-v2/payment/redirection": "pg/nice-v2/payment/redirection-lwe2DstU50_oOIMLzUAUNwVtsSWTbi3hHK5oyRVSfpY=.js",
	"pg/paypal-v2/issue-billing-key/ui": "pg/paypal-v2/issue-billing-key/ui-NCh3PFDEIY2XsRFeuYysXg6v2b2eSA1IcEK4GZ-JDaY=.js",
	"pg/paypal-v2/payment/ui": "pg/paypal-v2/payment/ui-BKWOPBxSPJMde3iISTSUngPbJgL6BJsobGzyLQg-8Wg=.js",
	"pg/smartro-v2/issue-billing-key/iframe-impl": "pg/smartro-v2/issue-billing-key/iframe-impl-MXu3sZXyTpTfG8aXc-0H_D0YmqXTz6Afy7GRJ7Wfn74=.js",
	"pg/smartro-v2/issue-billing-key/iframe": "pg/smartro-v2/issue-billing-key/iframe-AS6aBP0u9Q7kuM8UtdHLt57s2kh0Lo3ith1u-PE4iwM=.js",
	"pg/smartro-v2/issue-billing-key/redirection": "pg/smartro-v2/issue-billing-key/redirection-hPDhuNRwCy_jsqh-ia2bf78iZ9zjOwF3LS9PPIA1stk=.js",
	"pg/smartro-v2/payment/iframe-impl": "pg/smartro-v2/payment/iframe-impl-T-JI6YaOdeiz3um6gSM5lcejWFbzgpWeqhdVpO845mU=.js",
	"pg/smartro-v2/payment/iframe": "pg/smartro-v2/payment/iframe-3evFvZ3ZSvnkKdqocCzp4hhlVNwI2RjK-wrHIJ7EIvM=.js",
	"pg/smartro-v2/payment/redirection": "pg/smartro-v2/payment/redirection-wbmq9OFGeWZ6gP9Omw1m4CSEd9HTW77g8VXgyBlNZe8=.js",
	"pg/toss-brandpay/module/toss-brandpay": "pg/toss-brandpay/module/toss-brandpay-HPFIpBR0zc-rLdItDaue-vmv2IciFO2tOknMhGlbdww=.js",
	"pg/toss-brandpay/payment/iframe-impl": "pg/toss-brandpay/payment/iframe-impl-JiyEBmzkV_Hjsuxc7GqVvu1bylP2P0sE_QckONMyUaA=.js",
	"pg/toss-brandpay/payment/iframe": "pg/toss-brandpay/payment/iframe-Sun9vhiHdqUf5VqFSsTky3S31B40Gqa10BrB3GxIiZ0=.js",
	"pg/toss-brandpay/payment/ui": "pg/toss-brandpay/payment/ui-_YHoBsIbgBTP7BAGSveRH2vEL7SP84J0a6yhc9mtgLY=.js",
	"pg/tosspay-v2/issue-billing-key/popup-impl": "pg/tosspay-v2/issue-billing-key/popup-impl-IGcWPagTa9i-488iuzuDvJVlAN8yJggLPUXN50UsvBk=.js",
	"pg/tosspay-v2/issue-billing-key/popup": "pg/tosspay-v2/issue-billing-key/popup-CRA7FLg5vvs8_jdxJUn8jbFCOp2hobD3lV5gBEfMZmQ=.js",
	"pg/tosspay-v2/issue-billing-key/redirection": "pg/tosspay-v2/issue-billing-key/redirection-yVs97m9oTetrvyn8SrSeAVpPUANxJp1tYZpwN0b-5LA=.js",
	"pg/tosspay-v2/payment/popup-impl": "pg/tosspay-v2/payment/popup-impl-mjTZvPTJ7lLe95IwitF22GMP_B2E5HXubYy8F8yorzw=.js",
	"pg/tosspay-v2/payment/popup": "pg/tosspay-v2/payment/popup-MUgNrZJFDvfvQDTiShnxJqW6Kdf8n_Fdzr21nTncmnI=.js",
	"pg/tosspay-v2/payment/redirection": "pg/tosspay-v2/payment/redirection-513Jwiv2hq6G2aG1fwbeVaa7gNygSOlmYNOBM3zMDD8=.js",
	"pg/tosspayments/issue-billing-key/iframe-impl": "pg/tosspayments/issue-billing-key/iframe-impl-b68dBHTXLHiz3zml7QRYrYZlsw2cchxltTeWQspCOMc=.js",
	"pg/tosspayments/issue-billing-key/iframe": "pg/tosspayments/issue-billing-key/iframe-0LefUmsAGq7aQUcQ2TAbkpi0Q5vOS2NuzXZDhfyH1LE=.js",
	"pg/tosspayments/issue-billing-key/redirection": "pg/tosspayments/issue-billing-key/redirection-d0ssACCmDNpJnpLl-SgBFjJtW-h_ouIFOOJ5qHqf35E=.js",
	"pg/tosspayments/payment/iframe-impl": "pg/tosspayments/payment/iframe-impl-QO9-MvJVUQnCksrL2IKGHauMh-MjbEE-k7zs3aJjGQQ=.js",
	"pg/tosspayments/payment/iframe": "pg/tosspayments/payment/iframe-iUyq1R_O_diVWxm64cEDbpjJd0SKdO_iGnhLRHXbaPs=.js",
	"pg/tosspayments/payment/redirection": "pg/tosspayments/payment/redirection-xh3ISAQODMpp02UFwH0A3ZAVjX7sLdVh-KH5iqY4voQ=.js",
	"pg/welcome/issue-billing-key/iframe-impl": "pg/welcome/issue-billing-key/iframe-impl-6AHl6jlrOYY5VJdRDv4EklDcAzNpXWR7imMNe5m-Qj8=.js",
	"pg/welcome/issue-billing-key/iframe": "pg/welcome/issue-billing-key/iframe-DGt0RxQUYD8xrGVCHv0LWpGmDduuo3TwHOzW0TsHNf0=.js",
	"pg/welcome/issue-billing-key/redirection": "pg/welcome/issue-billing-key/redirection-Tw4_Y5p_IgmXlZs5onpRvrXnBnPwrCiV0VlJEFtQqa0=.js",
	"pg/welcome/issue-billing-key-and-pay/iframe-impl": "pg/welcome/issue-billing-key-and-pay/iframe-impl-5XVCQVnVMT5rRYbISqQ8CHXwPiKXp5sf_2_gTiBgMs4=.js",
	"pg/welcome/issue-billing-key-and-pay/iframe": "pg/welcome/issue-billing-key-and-pay/iframe-tDuDVqfE-g3tqEp8QxUvf775UzIhT5KBwh3GZGalXLg=.js",
	"pg/welcome/issue-billing-key-and-pay/redirection": "pg/welcome/issue-billing-key-and-pay/redirection-HNgro477iJX7mJ411KhCCQ8D8ka4jUERXRZeNNunX3M=.js",
	"pg/welcome/payment/iframe-impl": "pg/welcome/payment/iframe-impl-8ZNq8hunAKuevpyYpvX-rqe_gro_gyitalVEvzube9g=.js",
	"pg/welcome/payment/iframe": "pg/welcome/payment/iframe--0gxcrXVx_e3UdNkrZMXj-F6M8WdzBDGh6cr8Nfb61E=.js",
	"pg/welcome/payment/redirection": "pg/welcome/payment/redirection-fmo-X2YyXBOQ4aA-s9DDe-Py75QqwUVhjuK07QFd6Ns=.js",
	"pg/danal/identity-verification/popup-def": "pg/danal/identity-verification/popup-def-woF6uPMJ8d4XBZVp_b9RgQaTNEPSZtKviv34eGrwiIo=.json",
	"pg/inicis-v2/issue-billing-key/iframe-def": "pg/inicis-v2/issue-billing-key/iframe-def-rUdy3dfPltRJs5rCs82fThOh9sNcTzab9BUVEIPpIu4=.json",
	"pg/inicis-v2/issue-billing-key-and-pay/iframe-def": "pg/inicis-v2/issue-billing-key-and-pay/iframe-def-GnusyzCNQPdOgZBYUyPRctO_1f_zKbjlczIMm6fSaQk=.json",
	"pg/inicis-v2/payment/iframe-def": "pg/inicis-v2/payment/iframe-def-zNOcKImX7Gxn4U3cydpyVuUcs1eQuSrXSOBXueSMp2o=.json",
	"pg/kakaopay/issue-billing-key/iframe-def": "pg/kakaopay/issue-billing-key/iframe-def-lskmFWDeK9VXxGcfQv15gndAJgEO-Gp-rHGuVMKvqko=.json",
	"pg/kakaopay/payment/iframe-def": "pg/kakaopay/payment/iframe-def-uSCw8dZr5xOVQxFM6QXBWPcnJRqXIBUw3DUUkrNtwmY=.json",
	"pg/kpn/issue-billing-key/iframe-def": "pg/kpn/issue-billing-key/iframe-def-5PrVxqbLDdLepeqmoK8_mh6bN15g3XEYLgLzEtIArdI=.json",
	"pg/kpn/payment/iframe-def": "pg/kpn/payment/iframe-def-AhzIk9VIKr2Xm1jkd56Briy82ILH5ZPmzai7IfPwyr4=.json",
	"pg/ksnet/payment/iframe-def": "pg/ksnet/payment/iframe-def-SVE3pPbBRMdyfMvb2950MTwEPPbx8pz-Ys1AkLaGoyw=.json",
	"pg/naverpay/issue-billing-key/popup-def": "pg/naverpay/issue-billing-key/popup-def-1MmzQxJK_CbJyLaFwTQ8XapbAKPD_xFp4trjAEa-Ofc=.json",
	"pg/naverpay/payment/popup-def": "pg/naverpay/payment/popup-def-0QEQaa10TOD16V7NVdsg7tQmUdOeKpqmp5wf5WsGBcg=.json",
	"pg/nice-v2/issue-billing-key/iframe-def": "pg/nice-v2/issue-billing-key/iframe-def-F9oDQu8zhAICaAF8oqJaERGZbwscH2A_L_Hz5rx5vcQ=.json",
	"pg/nice-v2/payment/iframe-def": "pg/nice-v2/payment/iframe-def-sa3HULFQ11XOhrEuyomUWKU-lpbmW-g-2AVL_yL_9r8=.json",
	"pg/smartro-v2/issue-billing-key/iframe-def": "pg/smartro-v2/issue-billing-key/iframe-def-V80isf_pcwyBCxLgqiRv8uc6HnQ06Q52gHd0LyyDK9U=.json",
	"pg/smartro-v2/payment/iframe-def": "pg/smartro-v2/payment/iframe-def-yECBYeng0BfMxG1O_rdz_XSDlv3Pw-4C-OgRP4I_5yA=.json",
	"pg/toss-brandpay/payment/iframe-def": "pg/toss-brandpay/payment/iframe-def-5488nqVG6LvKJjRtkVvvVyUU-EYgkCpJ8ufvZruE2Ao=.json",
	"pg/tosspay-v2/issue-billing-key/popup-def": "pg/tosspay-v2/issue-billing-key/popup-def-tmtPMR4URJQZwRiPhbedqP34Xx_9bR6CxgQtv3S0jyU=.json",
	"pg/tosspay-v2/payment/popup-def": "pg/tosspay-v2/payment/popup-def-hn_KbhtwUykesOmQsUY5fJP88GrciHZkwU8mWcGCUsg=.json",
	"pg/tosspayments/issue-billing-key/iframe-def": "pg/tosspayments/issue-billing-key/iframe-def-vgi0l06j3TGz_KBc2cq9BbkDEUamg6O_unCEL6jvE9k=.json",
	"pg/tosspayments/payment/iframe-def": "pg/tosspayments/payment/iframe-def-yEDjLexHGyfSr2qDs2VYDoUvSmJJPJty9U4e1ekONO0=.json",
	"pg/welcome/issue-billing-key/iframe-def": "pg/welcome/issue-billing-key/iframe-def-ekIKoyNuYmyJRUQh8WAv8R5aW5TX9MfJEmSY_eEseAY=.json",
	"pg/welcome/issue-billing-key-and-pay/iframe-def": "pg/welcome/issue-billing-key-and-pay/iframe-def-oa7TcIDfyflOXR1QADHhnoHq0tS9PVRb50FzMnJqGic=.json",
	"pg/welcome/payment/iframe-def": "pg/welcome/payment/iframe-def-QqditbC-ROaSR0a2HvsTmoSrhb4m82o3pCuZQ_y0kPE=.json"
};
var f = {}, D = t => {
	if("moduleType" in t) {
		let e = p => p.replace("PG_PROVIDER_", "").replace(/_/g, "-").toLowerCase(), {pgProvider: r, moduleType: o} = t;
		return `pg/${e(r)}/module/${o}`
	}
	if("pgProvider" in t) {
		let e = C => C.replace("PG_PROVIDER_", "").replace(/_/g, "-").toLowerCase(), {pgProvider: r, transactionType: o, windowType: p} = t;
		return `pg/${e(r)}/${e(o)}/${e(p)}`
	} else {
		let e = p => p.replace(/_/g, "-").toLowerCase(), {transactionType: r, uiType: o} = t;
		return `loadable-ui/${e(r)}/${e(o)}`
	}
}, F = t => {
	let e = P[t];
	if(!e) throw new Error(`\uC870\uAC74\uC5D0 \uB9DE\uB294 \uB4DC\uB77C\uC774\uBC84(${t})\uAC00 \uC5C6\uC2B5\uB2C8\uB2E4.`);
	return new Promise(async(r, o) => {
		await Y(), zi();
		let p = document.createElement("script");
		p.async = !0, p.src = `${l.DRIVER_SERVE_URL}/${e}`, p.addEventListener("load", () => r()), p.addEventListener("error", () => o(new Error(`\uAE30\uB2A5 \uC2E4\uD589\uC744 \uC704\uD55C \uC2A4\uD06C\uB9BD\uD2B8 \uB85C\uB529\uC5D0 \uC2E4\uD328\uD558\uC600\uC2B5\uB2C8\uB2E4. (\uB4DC\uB77C\uC774\uBC84 ID: ${t})`))), document.body.appendChild(p)
	})
};

function zi() {
	window.__PORTONE__ = {
		registerDriver(t, e) {
			f[t] = e
		}
	}
}

async function Y() {
	await Promise.all(Object.values(f).filter(Boolean).map(t => t.onBeforeCleanup?.({slots: l, driverManifest: P})));
	for(let t of Object.keys(f)) {
		let e = `${l.DRIVER_SERVE_URL}/${P[t]}`, r = document.querySelector(`script[src="${e}"]`);
		r && r.parentNode?.removeChild(r), delete f[t]
	}
}

var Qe = wi(Ze(), 1);

function x() {
	let {device: t, os: e} = (0, Qe.UAParser)(navigator.userAgent), {type: r} = t, o = r === "mobile" || r === "tablet" ? "PLATFORM_TYPE_MOBILE" : "PLATFORM_TYPE_PC", p = e.name === "Mac OS" && navigator.maxTouchPoints > 1;
	return {platformType: o, isIpad: p}
}

async function De(t) {
	let {pgProvider: e, identityVerificationId: r, storeId: o, identityVerificationTxId: p, windowType: C, message: c} = t;
	try {
		let N = await fetch(`${l.CHECKOUT_SERVER}/api/identity-verification-fail/${e}/v2`, {method: "POST", headers: {"Content-Type": "application/json"}, body: JSON.stringify({identityVerificationId: r, storeId: o, identityVerificationTxId: p, windowType: C, message: c, redirectUrl: t.redirectUrl, isDirectReturn: !0})});
		if(!N.ok) {
			let n = await N.text(), a = "UnknownError", i = `\uBCF8\uC778\uC778\uC99D \uC2E4\uD328 \uCC98\uB9AC \uACFC\uC815\uC5D0\uC11C \uBB38\uC81C\uAC00 \uBC1C\uC0DD\uD558\uC600\uC2B5\uB2C8\uB2E4. ${n}`;
			try {
				let s = JSON.parse(n);
				a = s.code, i = s.message
			} catch {
			}
			throw new B({code: a, message: i, identityVerificationId: r, identityVerificationTxId: p})
		}
		let {redirectUrl: E} = await N.json(), _ = new URL(E, l.CHECKOUT_SERVER);
		return {transactionType: "IDENTITY_VERIFICATION", identityVerificationTxId: _.searchParams.get("identityVerificationTxId") || p, identityVerificationId: _.searchParams.get("identityVerificationId") || r, code: _.searchParams.get("code") || "UnknownError", message: _.searchParams.get("message") || c}
	} catch(N) {
		if(ce(N)) throw N;
		let {code: E, message: _} = N.response?.data || {code: "UnknownError", message: N.message};
		throw new B({code: E, message: _, identityVerificationId: r, identityVerificationTxId: p})
	}
}

var Ie = {};
We(Ie, {ActionType: () => $e, FailureType: () => ei, IdentityVerificationStatus: () => ii, IssueBillingKeyStatus: () => ri, ModuleType: () => ti, PaymentStatus: () => ni, PlatformType: () => oi});
var Xi = {LOAD_UI: "LOAD_UI", REQUEST_IDENTITY_VERIFICATION: "REQUEST_IDENTITY_VERIFICATION", REQUEST_ISSUE_BILLING_KEY: "REQUEST_ISSUE_BILLING_KEY", REQUEST_ISSUE_BILLING_KEY_AND_PAY: "REQUEST_ISSUE_BILLING_KEY_AND_PAY", REQUEST_PAY: "REQUEST_PAY"}, $e = Xi;
var Wi = {
	UNSPECIFIED: "FAILURE_TYPE_UNSPECIFIED",
	INITIALIZE_FAILED_NO_CANDIDATE_CHANNEL: "FAILURE_TYPE_INITIALIZE_FAILED_NO_CANDIDATE_CHANNEL",
	INITIALIZE_FAILED_FETCHING_CHANNELS_FAILED: "FAILURE_TYPE_INITIALIZE_FAILED_FETCHING_CHANNELS_FAILED",
	PREPARE_FAILED_NO_SATISFIED_CHANNEL: "FAILURE_TYPE_PREPARE_FAILED_NO_SATISFIED_CHANNEL",
	PREPARE_FAILED_TGS_PREPARE_FAILED: "FAILURE_TYPE_PREPARE_FAILED_TGS_PREPARE_FAILED",
	STORE_VALIDATION_FAILED: "FAILURE_TYPE_STORE_VALIDATION_FAILED",
	CHANNEL_VALIDATION_FAILED: "FAILURE_TYPE_CHANNEL_VALIDATION_FAILED",
	CONFIRM_PROCESS_FAILED: "FAILURE_TYPE_CONFIRM_PROCESS_FAILED",
	AUTHENTICATION_FAILED: "FAILURE_TYPE_AUTHENTICATION_FAILED",
	FORGERY_CHECK_FAILED: "FAILURE_TYPE_FORGERY_CHECK_FAILED",
	APPROVE_FAILED_TGS_APPROVE_FAILED: "FAILURE_TYPE_APPROVE_FAILED_TGS_APPROVE_FAILED",
	ISSUE_VIRTUAL_ACCOUNT_FAILED: "FAILURE_TYPE_ISSUE_VIRTUAL_ACCOUNT_FAILED",
	STOPPED: "FAILURE_TYPE_STOPPED",
	BILLING_KEY_TGS_PAY_FAILED: "FAILURE_TYPE_BILLING_KEY_TGS_PAY_FAILED",
	INSTANT_TGS_PAY_FAILED: "FAILURE_TYPE_INSTANT_TGS_PAY_FAILED"
}, ei = Wi;
var Zi = {STATUS_UNSPECIFIED: "STATUS_UNSPECIFIED", STATUS_PREPARED: "STATUS_PREPARED", STATUS_FAILED: "STATUS_FAILED", STATUS_VERIFIED: "STATUS_VERIFIED"}, ii = Zi;
var Qi = {STATUS_UNSPECIFIED: "STATUS_UNSPECIFIED", STATUS_PREPARED: "STATUS_PREPARED", STATUS_FAILED: "STATUS_FAILED", STATUS_APPROVED: "STATUS_APPROVED"}, ri = Qi;
var ti = {"toss-brandpay": "Tosspayments Brandpay SDK"};
var $i = {STATUS_UNSPECIFIED: "STATUS_UNSPECIFIED", STATUS_INITIALIZED: "STATUS_INITIALIZED", STATUS_PREPARED: "STATUS_PREPARED", STATUS_FAILED: "STATUS_FAILED", STATUS_STOPPED: "STATUS_STOPPED", STATUS_APPROVED: "STATUS_APPROVED", STATUS_PARTIAL_CANCELLED: "STATUS_PARTIAL_CANCELLED", STATUS_CANCELLED: "STATUS_CANCELLED", STATUS_VIRTUAL_ACCOUNT_ISSUED: "STATUS_VIRTUAL_ACCOUNT_ISSUED"}, ni = $i;
var oi = {PC: "PLATFORM_TYPE_PC", MOBILE: "PLATFORM_TYPE_MOBILE"};

async function si(t, e, r) {
	try {
		let o = new URL(`${l.CHECKOUT_SERVER}/api/identity-verification-status/v2`);
		o.searchParams.append("storeId", t), o.searchParams.append("identityVerificationId", e), o.searchParams.append("identityVerificationTxId", r);
		let p = await fetch(o);
		if(!p.ok) {
			let C = await p.text(), c = "UnknownError", N = `\uBCF8\uC778\uC778\uC99D \uACB0\uACFC \uD655\uC778 \uACFC\uC815\uC5D0\uC11C \uBB38\uC81C\uAC00 \uBC1C\uC0DD\uD558\uC600\uC2B5\uB2C8\uB2E4. ${C}`;
			try {
				let E = JSON.parse(C);
				c = E.code, N = E.message
			} catch {
			}
			throw new B({code: c, message: N, identityVerificationId: e, identityVerificationTxId: r})
		}
		return p.json()
	} catch(o) {
		throw ce(o) ? o : new B({code: "UnknownError", message: o.message || "본인인증 결과 확인 과정에서 문제가 발생하였습니다.", identityVerificationId: e, identityVerificationTxId: r})
	}
}

async function er(t) {
	if(!t) throw new B({code: "BadRequest", message: "본인인증 요청 파라메터가 누락되었습니다."});
	try {
		let e = await fetch(`${l.CHECKOUT_SERVER}/api/identity-verification-prepare/v2`, {method: "POST", headers: {"Content-Type": "application/json"}, body: JSON.stringify({data: t, sdkVersion: l.SDK_VERSION, deviceInfo: x()})});
		if(!e.ok) {
			let p = await e.text(), {code: C, message: c} = (() => {
				try {
					return JSON.parse(p)
				} catch {
					return {code: "UnknownError", message: `\uBCF8\uC778\uC778\uC99D \uCC3D \uD638\uCD9C\uC5D0 \uC2E4\uD328\uD558\uC600\uC2B5\uB2C8\uB2E4. ${p}`}
				}
			})();
			throw new B({code: C, message: c})
		}
		let r = await e.json();
		if(!r) throw new Error("본인인증 창 호출에 필요한 데이터가 전달되지 않았습니다.");
		let o = D({pgProvider: r.pgProvider, transactionType: r.transactionType, windowType: r.windowType});
		return await F(o), await f[o].onAfterInitialize?.({slots: l, driverManifest: P, initializeOrPrepareResponse: r}), await new Promise((p, C) => {
			f[o].onAfterPrepare?.({slots: l, driverManifest: P, prepareResponse: r}, {
				onTransactionFail: async(c, N) => {
					if(c === w.TransactionType.IDENTITY_VERIFICATION && !("txId" in N) && !("billingKey" in N)) {
						let E = await De(N);
						p(E)
					} else p({transactionType: "IDENTITY_VERIFICATION", identityVerificationId: r.identityVerificationId, identityVerificationTxId: r.identityVerificationTxId, code: "UnknownError", message: "올바르지 않은 본인인증 결과가 반환되었습니다."})
				}, onTransactionOver: async(c, N) => {
					await Y(), c === w.TransactionType.IDENTITY_VERIFICATION && "identityVerificationTxId" in N ? p(N) : C(new B({code: "UnknownError", message: "올바르지 않은 본인인증 결과가 반환되었습니다."}))
				}, onForceClose: async() => {
					let c = {transactionType: "IDENTITY_VERIFICATION", identityVerificationId: r.identityVerificationId, identityVerificationTxId: r.identityVerificationTxId};
					try {
						let N = await si(r.storeId, r.identityVerificationId, r.identityVerificationTxId), {status: E} = N;
						switch(E) {
							case "STATUS_VERIFIED": {
								p(c);
								break
							}
							case "STATUS_FAILED": {
								let {failure: _} = N, {code: n, message: a} = (() => {
									if(_) {
										let {pgCode: i, pgMessage: s} = _;
										if(s) return {code: "PG_PROVIDER_ERROR", message: i ? `[${i}] ${s}` : s}
									}
									return {code: "PORTONE_ERROR", message: _?.message || "본인인증에 실패하였습니다."}
								})();
								p({...c, code: n, message: a});
								break
							}
							case "STATUS_PREPARED": {
								let _ = await De({pgProvider: r.pgProvider, storeId: r.storeId, identityVerificationId: r.identityVerificationId, identityVerificationTxId: r.identityVerificationTxId, windowType: "POPUP", redirectUrl: r.redirectUrl, message: "사용자가 본인인증을 취소하였습니다."});
								p(_);
								break
							}
							default: {
								p({...c, code: "UnknownError", message: "해당 본인인증 건의 상태 오류로 인해 요청을 처리할 수 없습니다."});
								break
							}
						}
					} catch(N) {
						p({...c, code: N.code || "UnknownError", message: `\uBCF8\uC778\uC778\uC99D \uD31D\uC5C5 \uC885\uB8CC \uD6C4 \uC11C\uBC84\uC640 \uD1B5\uC2E0\uD558\uB294 \uACFC\uC815\uC5D0\uC11C \uC624\uB958\uAC00 \uBC1C\uC0DD\uD558\uC600\uC2B5\uB2C8\uB2E4. ${N.message}`})
					}
				}
			})
		})
	} catch(e) {
		throw await Y(), e instanceof B ? e : new B({code: "UnknownError", message: `\uBCF8\uC778\uC778\uC99D \uCC3D \uD638\uCD9C\uC5D0 \uC2E4\uD328\uD558\uC600\uC2B5\uB2C8\uB2E4. ${e.message}`})
	}
}

var J = {}, ai = t => {
	if("moduleType" in t) {
		let e = p => p.replace("PG_PROVIDER_", "").replace(/_/g, "-").toLowerCase(), {pgProvider: r, moduleType: o} = t;
		return `pg/${e(r)}/module/${o}`
	}
	if("pgProvider" in t) {
		let e = C => C.replace("PG_PROVIDER_", "").replace(/_/g, "-").toLowerCase(), {pgProvider: r, transactionType: o, windowType: p} = t;
		return `pg/${e(r)}/${e(o)}/${e(p)}`
	} else {
		let e = p => p.replace(/_/g, "-").toLowerCase(), {transactionType: r, uiType: o} = t;
		return `loadable-ui/${e(r)}/${e(o)}`
	}
}, pi = t => {
	let e = P[t];
	if(!e) throw new Error(`\uC870\uAC74\uC5D0 \uB9DE\uB294 \uB4DC\uB77C\uC774\uBC84(${t})\uAC00 \uC5C6\uC2B5\uB2C8\uB2E4.`);
	return new Promise(async(r, o) => {
		await be(), ir();
		let p = document.createElement("script");
		p.async = !0, p.src = `${l.DRIVER_SERVE_URL}/${e}`, p.addEventListener("load", () => r()), p.addEventListener("error", () => o(new Error(`\uAE30\uB2A5 \uC2E4\uD589\uC744 \uC704\uD55C \uC2A4\uD06C\uB9BD\uD2B8 \uB85C\uB529\uC5D0 \uC2E4\uD328\uD558\uC600\uC2B5\uB2C8\uB2E4. (\uB4DC\uB77C\uC774\uBC84 ID: ${t})`))), document.body.appendChild(p)
	})
};

function ir() {
	window.__PORTONE__ = {
		registerDriver(t, e) {
			J[t] = e
		}
	}
}

async function be() {
	await Promise.all(Object.values(J).filter(Boolean).map(t => t.onBeforeCleanup?.({slots: l, driverManifest: P})));
	for(let t of Object.keys(J)) {
		let e = `${l.DRIVER_SERVE_URL}/${P[t]}`, r = document.querySelector(`script[src="${e}"]`);
		r && r.parentNode?.removeChild(r), delete J[t]
	}
}

async function ie(t) {
	let {pgProvider: e, billingKey: r, windowType: o, pgCode: p, pgMessage: C, message: c} = t;
	try {
		let N = await fetch(`${l.CHECKOUT_SERVER}/api/issue-fail/${e}/v2`, {method: "POST", headers: {"Content-Type": "application/json"}, body: JSON.stringify({billingKey: r, portOneBillingKey: r, windowType: o, pgCode: p, pgMessage: C, message: c, responseType: "json"})});
		if(!N.ok) {
			let n = await N.text(), a = "UnknownError", i = `\uBE4C\uB9C1\uD0A4 \uBC1C\uAE09 \uC2E4\uD328 \uCC98\uB9AC \uACFC\uC815\uC5D0\uC11C \uBB38\uC81C\uAC00 \uBC1C\uC0DD\uD558\uC600\uC2B5\uB2C8\uB2E4. ${n}`;
			try {
				let s = JSON.parse(n);
				a = s.code, i = s.message
			} catch {
			}
			throw new O({code: a, message: i, billingKey: r})
		}
		let E = await N.json();
		return {transactionType: "ISSUE_BILLING_KEY", billingKey: E.billingKey, code: E.code, message: E.message}
	} catch(N) {
		if(L(N)) throw N;
		let {code: E, message: _} = N.response?.data || {code: "UnknownError", message: N.message};
		throw new O({code: E, message: _, billingKey: r})
	}
}

async function _i(t) {
	try {
		let e = new URL(`${l.CHECKOUT_SERVER}/api/issue-status/v2`);
		e.searchParams.append("billingKey", t);
		let r = await fetch(e);
		if(!r.ok) {
			let o = await r.text(), p = "UnknownError", C = `\uBE4C\uB9C1\uD0A4 \uBC1C\uAE09 \uACB0\uACFC \uD655\uC778 \uACFC\uC815\uC5D0\uC11C \uBB38\uC81C\uAC00 \uBC1C\uC0DD\uD558\uC600\uC2B5\uB2C8\uB2E4. ${o}`;
			try {
				let c = JSON.parse(o);
				p = c.code, C = c.message
			} catch {
			}
			throw new O({code: p, message: C, billingKey: t})
		}
		return r.json()
	} catch(e) {
		throw L(e) ? e : new O({code: "UnknownError", message: e.message || "빌링키 발급 결과 확인 과정에서 문제가 발생하였습니다.", billingKey: t})
	}
}

async function Te(t, e = !1) {
	let r = await fetch(`${l.CHECKOUT_SERVER}/api/issue-prepare/v2`, {method: "POST", headers: {"Content-Type": "application/json"}, body: JSON.stringify({data: t, sdkVersion: l.SDK_VERSION, deviceInfo: x()})});
	if(!r.ok) {
		let c = await r.text(), {code: N, message: E} = (() => {
			try {
				return JSON.parse(c)
			} catch {
				return {code: "UnknownError", message: `\uBE4C\uB9C1\uD0A4 \uBC1C\uAE09 \uCC3D \uD638\uCD9C\uC5D0 \uC2E4\uD328\uD558\uC600\uC2B5\uB2C8\uB2E4. ${c}`}
			}
		})();
		throw new O({code: N, message: E})
	}
	let o = await r.json();
	if(!o) throw new O({code: "UnknownError", message: "결제 창 호출에 필요한 데이터를 전달하는 과정에서 알 수 없는 문제가 발생하였습니다."});
	let p = ai({pgProvider: o.pgProvider, transactionType: o.transactionType, windowType: o.windowType});
	await pi(p), e || await J[p].onAfterInitialize?.({slots: l, driverManifest: P, initializeOrPrepareResponse: o});
	let C = new Promise((c, N) => {
		J[p].onAfterPrepare?.({slots: l, driverManifest: P, prepareResponse: o}, {
			onTransactionFail: async(E, _) => {
				if(E === w.TransactionType.ISSUE_BILLING_KEY && "billingKey" in _) {
					let n = await ie(_);
					c(n)
				} else c({transactionType: "ISSUE_BILLING_KEY", billingKey: o.billingKey, code: "UnknownError", message: "올바르지 않은 빌링키 발급 결과가 반환되었습니다."})
			}, onTransactionOver: async(E, _) => {
				await be(), E === w.TransactionType.ISSUE_BILLING_KEY && "billingKey" in _ && !("paymentId" in _) ? c(_) : N(new O({code: "UnknownError", message: "올바르지 않은 빌링키 발급 결과가 반환되었습니다."}))
			}, onForceClose: async() => {
				let E = {transactionType: "ISSUE_BILLING_KEY", billingKey: o.billingKey};
				try {
					let _ = await _i(o.billingKey), {status: n} = _;
					switch(n) {
						case "STATUS_APPROVED": {
							c(E);
							break
						}
						case "STATUS_FAILED": {
							let {failure: a} = _, {code: i, message: s} = (() => {
								if(a) {
									let {pgCode: R, pgMessage: g} = a;
									if(g) return {code: "PG_PROVIDER_ERROR", message: R ? `[${R}] ${g}` : g}
								}
								return {code: "PORTONE_ERROR", message: a?.message || "빌링키 발급에 실패하였습니다."}
							})();
							c({...E, code: i, message: s});
							break
						}
						case "STATUS_PREPARED": {
							let a = await ie({pgProvider: o.pgProvider, billingKey: o.billingKey, windowType: "POPUP", message: "사용자가 빌링키 발급을 취소하였습니다."});
							c(a);
							break
						}
						default: {
							c({...E, code: "UnknownError", message: "해당 빌링키 발급 건의 상태 오류로 인해 요청을 처리할 수 없습니다."});
							break
						}
					}
				} catch(_) {
					c({...E, code: _.code || "UnknownError", message: `\uBE4C\uB9C1\uD0A4 \uD31D\uC5C5 \uC885\uB8CC \uD6C4 \uC11C\uBC84\uC640 \uD1B5\uC2E0\uD558\uB294 \uACFC\uC815\uC5D0\uC11C \uC624\uB958\uAC00 \uBC1C\uC0DD\uD558\uC600\uC2B5\uB2C8\uB2E4. ${_.message}`})
				}
			}
		})
	});
	return {prepareResponse: o, issueBillingKeyResult: C}
}

async function rr(t) {
	if(!t) throw new O({code: "BadRequest", message: "빌링키 발급 요청 파라메터가 누락되었습니다."});
	try {
		let {prepareResponse: e, issueBillingKeyResult: r} = await Te(t);
		return await r
	} catch(e) {
		throw await Y(), L(e) ? e : new O({code: "UnknownError", message: `\uBE4C\uB9C1\uD0A4 \uBC1C\uAE09 \uCC3D \uD638\uCD9C\uC5D0 \uC2E4\uD328\uD558\uC600\uC2B5\uB2C8\uB2E4. ${e.message}`})
	}
}

async function Ri(t) {
	let {pgProvider: e, txId: r, paymentId: o, billingKey: p, windowType: C, pgCode: c, pgMessage: N, message: E} = t;
	try {
		let _ = await fetch(`${l.CHECKOUT_SERVER}/api/issue-billing-key-and-pay-fail/${e}/v2`, {method: "POST", headers: {"Content-Type": "application/json"}, body: JSON.stringify({txId: r, paymentId: o, billingKey: p, windowType: C, pgCode: c, pgMessage: N, message: E, isDirectReturn: !0})});
		if(!_.ok) {
			let i = await _.text(), s = "UnknownError", R = `\uBE4C\uB9C1\uD0A4 \uBC1C\uAE09 \uBC0F \uACB0\uC81C \uC2E4\uD328 \uCC98\uB9AC \uACFC\uC815\uC5D0\uC11C \uBB38\uC81C\uAC00 \uBC1C\uC0DD\uD558\uC600\uC2B5\uB2C8\uB2E4. ${i}`;
			try {
				let g = JSON.parse(i);
				s = g.code, R = g.message
			} catch {
			}
			throw new G({txId: r, paymentId: o, billingKey: p, code: s, message: R})
		}
		let {redirectUrl: n} = await _.json(), a = new URL(n, l.CHECKOUT_SERVER);
		return {transactionType: "ISSUE_BILLING_KEY_AND_PAY", txId: a.searchParams.get("txId") || r, paymentId: a.searchParams.get("paymentId") || o, billingKey: a.searchParams.get("billingKey") || p, code: a.searchParams.get("code") || "UnknownError", message: a.searchParams.get("message") || E}
	} catch(_) {
		if(Ne(_)) throw _;
		let {code: n, message: a} = _.response?.data || {code: "UnknownError", message: _.message};
		throw new G({txId: r, paymentId: o, billingKey: p, code: n, message: a})
	}
}

async function tr(t) {
	if(!t) throw new G({code: "BadRequest", message: "빌링키 발급 및 결제 요청 파라메터가 누락되었습니다."});
	try {
		let e = await fetch(`${l.CHECKOUT_SERVER}/api/issue-billing-key-and-pay-prepare/v2`, {method: "POST", headers: {"Content-Type": "application/json"}, body: JSON.stringify({data: {...t, actionType: Ie.ActionType.REQUEST_ISSUE_BILLING_KEY_AND_PAY}, sdkVersion: l.SDK_VERSION})});
		if(!e.ok) {
			let n = await e.text(), {code: a, message: i} = (() => {
				try {
					return JSON.parse(n)
				} catch {
					return {code: "UnknownError", message: `\uBE4C\uB9C1\uD0A4 \uBC1C\uAE09 \uBC0F \uACB0\uC81C \uCC3D \uD638\uCD9C\uC5D0 \uC2E4\uD328\uD558\uC600\uC2B5\uB2C8\uB2E4. ${n}`}
				}
			})();
			throw new G({code: a, message: i})
		}
		let r = await e.json();
		if(!r) throw new Error("빌링키 발급 및 결제 창 호출에 필요한 데이터가 전달되지 않았습니다.");
		let {txId: o, paymentId: p, billingKey: C, pgProvider: c, transactionType: N, windowType: E} = r, _ = D({pgProvider: c, transactionType: N, windowType: E});
		return await F(_), await f[_].onAfterInitialize?.({slots: l, driverManifest: P, initializeOrPrepareResponse: r}), await new Promise((n, a) => {
			f[_].onAfterPrepare?.({slots: l, driverManifest: P, prepareResponse: r}, {
				onTransactionFail: async(i, s) => {
					if(i === w.TransactionType.ISSUE_BILLING_KEY_AND_PAY && "txId" in s && "paymentId" in s && "billingKey" in s) {
						let R = await Ri(s);
						n(R)
					} else n({transactionType: "ISSUE_BILLING_KEY_AND_PAY", txId: o, paymentId: p, billingKey: C, code: "UnknownError", message: "올바르지 않은 빌링키 발급 및 결제 결과가 반환되었습니다."})
				}, onTransactionOver: async(i, s) => {
					await Y(), i === w.TransactionType.ISSUE_BILLING_KEY_AND_PAY && "txId" in s && "paymentId" in s && "billingKey" in s ? n(s) : a(new G({code: "UnknownError", message: "올바르지 않은 빌링키 발급 및 결제 결과가 반환되었습니다."}))
				}, onForceClose: async() => {
					n({...{transactionType: "ISSUE_BILLING_KEY_AND_PAY", txId: o, paymentId: p, billingKey: C}, code: "UnknownError", message: "V2 requestIssueBillingKeyAndPay()에 onForceClose() 구현 필요"})
				}
			})
		})
	} catch(e) {
		throw await Y(), Ne(e) ? e : new G({code: "UnknownError", message: `\uBE4C\uB9C1\uD0A4 \uBC1C\uAE09 \uBC0F \uACB0\uC81C \uCC3D \uD638\uCD9C\uC5D0 \uC2E4\uD328\uD558\uC600\uC2B5\uB2C8\uB2E4. ${e.message}`})
	}
}

async function re(t) {
	let {pgProvider: e, storeId: r, txId: o, paymentId: p, windowType: C, pgCode: c, pgMessage: N, message: E} = t;
	try {
		let _ = await fetch(`${l.CHECKOUT_SERVER}/api/fail/${e}/v2`, {method: "POST", headers: {"Content-Type": "application/json"}, body: JSON.stringify({storeId: r, txId: o, paymentId: p, windowType: C, pgCode: c, pgMessage: N, message: E, responseType: "json"})});
		if(!_.ok) {
			let i = await _.text(), s = "UnknownError", R = `\uACB0\uC81C \uC2E4\uD328 \uCC98\uB9AC \uACFC\uC815\uC5D0\uC11C \uBB38\uC81C\uAC00 \uBC1C\uC0DD\uD558\uC600\uC2B5\uB2C8\uB2E4. ${i}`;
			try {
				let g = JSON.parse(i);
				s = g.code, R = g.message
			} catch {
			}
			throw new S({code: s, message: R, txId: o, paymentId: p})
		}
		let n = await _.json();
		return {transactionType: "PAYMENT", txId: n.txId, paymentId: n.paymentId, code: n.code, message: n.message}
	} catch(_) {
		if(h(_)) throw _;
		let {code: n, message: a} = _.response?.data || {code: "UnknownError", message: _.message};
		throw new S({code: n, message: a, txId: o, paymentId: p})
	}
}

async function Ei(t, e, r) {
	try {
		let o = new URL(`${l.CHECKOUT_SERVER}/api/status/v2`);
		o.searchParams.append("storeId", t), o.searchParams.append("paymentId", e), o.searchParams.append("txId", r);
		let p = await fetch(o);
		if(!p.ok) {
			let C = await p.text(), c = "UnknownError", N = `\uACB0\uC81C \uACB0\uACFC \uD655\uC778 \uACFC\uC815\uC5D0\uC11C \uBB38\uC81C\uAC00 \uBC1C\uC0DD\uD558\uC600\uC2B5\uB2C8\uB2E4. ${C}`;
			try {
				let E = JSON.parse(C);
				c = E.code, N = E.message
			} catch {
			}
			throw new S({code: c, message: N, txId: r, paymentId: e})
		}
		return p.json()
	} catch(o) {
		throw h(o) ? o : new S({code: "UnknownError", message: o.message || "결제 결과 확인 과정에서 문제가 발생하였습니다.", txId: r, paymentId: e})
	}
}

async function de(t, e = !1) {
	let r = await fetch(`${l.CHECKOUT_SERVER}/api/prepare/v2`, {method: "POST", headers: {"Content-Type": "application/json"}, body: JSON.stringify({data: t, sdkVersion: l.SDK_VERSION, deviceInfo: x()})});
	if(!r.ok) {
		let c = await r.text(), {code: N, message: E} = (() => {
			try {
				return JSON.parse(c)
			} catch {
				return {code: "UnknownError", message: `\uACB0\uC81C \uCC3D \uD638\uCD9C\uC5D0 \uC2E4\uD328\uD558\uC600\uC2B5\uB2C8\uB2E4. ${c}`}
			}
		})();
		throw new S({code: N, message: E})
	}
	let o = await r.json();
	if(!o) throw new S({code: "UnknownError", message: "결제 창 호출에 필요한 데이터를 전달하는 과정에서 알 수 없는 문제가 발생하였습니다."});
	let p = D({pgProvider: o.pgProvider, transactionType: o.transactionType, windowType: o.windowType});
	await F(p), e || await f[p].onAfterInitialize?.({slots: l, driverManifest: P, initializeOrPrepareResponse: o});
	let C = new Promise((c, N) => {
		f[p].onAfterPrepare?.({slots: l, driverManifest: P, prepareResponse: o}, {
			onTransactionFail: async(E, _) => {
				if(E === w.TransactionType.PAYMENT && !("billingKey" in _) && !("identityVerificationTxId" in _)) {
					let n = await re(_);
					c(n)
				} else c({transactionType: "PAYMENT", txId: o.txId, paymentId: o.paymentId, code: "UnknownError", message: "올바르지 않은 결제 결과가 반환되었습니다."})
			}, onTransactionOver: async(E, _) => {
				await Y(), E === w.TransactionType.PAYMENT && "txId" in _ && !("billingKey" in _) ? c(_) : N(new S({code: "UnknownError", message: "올바르지 않은 결제 결과가 반환되었습니다."}))
			}, onForceClose: async() => {
				let E = {transactionType: "PAYMENT", txId: o.txId, paymentId: o.paymentId};
				try {
					let _ = await Ei(o.storeId, o.paymentId, o.txId), {status: n} = _;
					switch(n) {
						case "STATUS_APPROVED":
						case "STATUS_VIRTUAL_ACCOUNT_ISSUED": {
							c(E);
							break
						}
						case "STATUS_FAILED": {
							let {failure: a} = _, {code: i, message: s} = (() => {
								if(a) {
									let {pgCode: R, pgMessage: g} = a;
									if(g) return {code: "PG_PROVIDER_ERROR", message: R ? `[${R}] ${g}` : g}
								}
								return {code: "PORTONE_ERROR", message: a?.message || "결제에 실패하였습니다."}
							})();
							c({...E, code: i, message: s});
							break
						}
						case "STATUS_PREPARED": {
							let a = await re({...o, windowType: "POPUP", message: "사용자가 결제를 취소하였습니다."});
							c(a);
							break
						}
						default: {
							c({...E, code: "UnknownError", message: "해당 결제 건의 상태 오류로 인해 요청을 처리할 수 없습니다."});
							break
						}
					}
				} catch(_) {
					c({...E, code: _.code || "UnknownError", message: `\uACB0\uC81C \uD31D\uC5C5 \uC885\uB8CC \uD6C4 \uC11C\uBC84\uC640 \uD1B5\uC2E0\uD558\uB294 \uACFC\uC815\uC5D0\uC11C \uC624\uB958\uAC00 \uBC1C\uC0DD\uD558\uC600\uC2B5\uB2C8\uB2E4. ${_.message}`})
				}
			}
		})
	});
	return {prepareResponse: o, paymentResult: C}
}

async function ci(t) {
	if(!ci) throw new S({code: "BadRequest", message: "결제요청 파라메터가 누락되었습니다."});
	try {
		let {prepareResponse: e, paymentResult: r} = await de(t);
		return await r
	} catch(e) {
		throw await Y(), h(e) ? e : new S({code: "UnknownError", message: `\uACB0\uC81C \uCC3D \uD638\uCD9C\uC5D0 \uC2E4\uD328\uD558\uC600\uC2B5\uB2C8\uB2E4. ${e.message}`})
	}
}

async function Ni(t) {
	if(!t) throw new S({code: "BadRequest", message: "결제요청 파라메터가 누락되었습니다."});
	await Y();
	try {
		let e = await fetch(`${l.CHECKOUT_SERVER}/api/initialize-payment/v2`, {method: "POST", headers: {"Content-Type": "application/json"}, body: JSON.stringify({data: t, sdkVersion: l.SDK_VERSION, deviceInfo: x()})});
		if(!e.ok) {
			let p = await e.text(), {code: C, message: c} = (() => {
				try {
					return JSON.parse(p)
				} catch {
					return {code: "UnknownError", message: `\uACB0\uC81C \uCD08\uAE30\uD654\uC5D0 \uC2E4\uD328\uD558\uC600\uC2B5\uB2C8\uB2E4. ${p}`}
				}
			})();
			throw new S({code: C, message: c})
		}
		let r = await e.json(), o = D({pgProvider: r.pgProvider, transactionType: r.transactionType, windowType: r.windowType});
		return await F(o), await f[o].onAfterInitialize?.({slots: l, driverManifest: P, initializeOrPrepareResponse: r}), r
	} catch(e) {
		throw await Y(), h(e) ? e : new S({code: "UnknownError", message: `\uACB0\uC81C \uCD08\uAE30\uD654\uC5D0 \uC2E4\uD328\uD558\uC600\uC2B5\uB2C8\uB2E4. ${e.message}`})
	}
}

async function li(t) {
	let {pgProvider: e, pgRawRequest: r, storeId: o, txId: p, paymentId: C, windowType: c} = t;
	try {
		let N = await fetch(`${l.CHECKOUT_SERVER}/api/approve/${e}/v2`, {method: "POST", headers: {"Content-Type": "application/json"}, body: JSON.stringify({...r, storeId: o, txId: p, paymentId: C, windowType: c, responseType: "json"})});
		if(!N.ok) {
			let n = await N.text(), a = "UnknownError", i = `\uACB0\uC81C \uC2B9\uC778 \uACFC\uC815\uC5D0\uC11C \uBB38\uC81C\uAC00 \uBC1C\uC0DD\uD558\uC600\uC2B5\uB2C8\uB2E4. ${n}`;
			try {
				let s = JSON.parse(n);
				a = s.code, i = s.message
			} catch {
			}
			throw new S({code: a, message: i})
		}
		let E = await N.json();
		return {transactionType: "PAYMENT", txId: E.txId, paymentId: E.paymentId, code: E.code, message: E.message}
	} catch(N) {
		if(h(N)) throw N;
		let {code: E, message: _} = N || {code: "UnknownError", message: N.message};
		throw new S({code: E, message: _, txId: p, paymentId: C})
	}
}

async function me(t) {
	let e = new URL("/api/client-credentials/v2", l.CHECKOUT_SERVER);
	e.search = new URLSearchParams(t).toString();
	try {
		let r = await fetch(e.toString(), {method: "GET", headers: {"Content-Type": "application/json"}});
		if(!r.ok) {
			let o = await r.text(), {code: p, message: C} = (() => {
				try {
					return JSON.parse(o)
				} catch {
					throw new Error(o)
				}
			})();
			throw new Error(`${p}: ${C}`)
		}
		return await r.json()
	} catch(r) {
		throw new Error(`credential \uC870\uD68C \uACFC\uC815\uC5D0\uC11C \uBB38\uC81C\uAC00 \uBC1C\uC0DD\uD558\uC600\uC2B5\uB2C8\uB2E4. ${r.message}`)
	}
}

var ye = {payment: {}}, nr = {...Object.fromEntries(Object.values(w.PgProvider).map(t => [t.substring(12), []])), PAYPAL_V2: ["PAYPAL_SPB"]};

async function or(t, e) {
	let {uiType: r} = t;
	Ci(t);
	try {
		let {storeId: o, channelKey: p} = t;
		if(!o) throw new K({code: "BadRequest", message: "storeId 파라미터는 필수 입력입니다."});
		if(!p) throw new K({code: "BadRequest", message: "channelKey 파라미터는 필수 입력입니다."});
		let C = {storeId: t.storeId, channelKey: t.channelKey}, N = (await me(C)).pgProvider, E = nr[N.substring(12)] || [];
		if(E.length === 0) throw new M({code: "UnknownError", message: `\uD574\uB2F9 PG\uC0AC(${N})\uB294 UI \uB85C\uB529\uC744 \uC9C0\uC6D0\uD558\uC9C0 \uC54A\uC2B5\uB2C8\uB2E4.`});
		if(!E.includes(r)) throw new M({code: "UnknownError", message: `\uD574\uB2F9 PG\uC0AC(${r})\uC758 UI \uD0C0\uC785\uC740 ${E.join(", ")}\uB9CC \uD5C8\uC6A9\uB429\uB2C8\uB2E4.`});
		let _ = await Ni({...ye.payment[r], payMethod: ve(r), actionType: "LOAD_UI"}) ?? void 0;
		if(!_) throw new M({code: "UnknownError", message: "결제 UI를 로드하기 위한 사전 요청에 실패하였습니다."});
		let n = D({pgProvider: _.pgProvider, transactionType: _.transactionType, windowType: _.windowType});
		if(!f[n]?.onLoadPaymentUI) throw new M({code: "UnknownError", message: "결제 UI 로딩을 지원하지 않는 PG사이거나, 모듈 로딩 과정에서 오류가 발생하였습니다."});
		f[n].onLoadPaymentUI?.({slots: l, driverManifest: P, paymentRequest: {...ye.payment[r], payMethod: ve(r)}, initializeOrPrepareResponse: _}, {
			onPreparePayment: async() => {
				try {
					let {prepareResponse: a} = await de({...ye.payment[r], payMethod: ve(r)}, !0);
					return a
				} catch(a) {
					e.onPaymentFail(a)
				}
			}, onApprovePayment: async a => {
				try {
					let i = await li(a);
					e.onPaymentSuccess(i)
				} catch(i) {
					h(i) ? e.onPaymentFail(i) : new S({code: "UnknownError", txId: i.txId, paymentId: i.paymentId, message: i.message})
				}
			}, onFailPayment: async a => {
				try {
					let i = await re({...a, windowType: "POPUP", message: "사용자가 결제를 취소하였습니다."});
					e.onPaymentFail(new S({code: i.code, txId: i.txId, paymentId: i.paymentId, message: i.message ?? ""}))
				} catch(i) {
					h(i) ? e.onPaymentFail(i) : e.onPaymentFail(new S({code: "UnknownError", txId: i.txId, paymentId: i.paymentId, message: i.message}))
				}
			}, onTransactionOver: () => {
				e.onPaymentFail(new S({code: "UnknownError", txId: "", paymentId: "", message: "NotImplemented"}))
			}
		})
	} catch(o) {
		throw await Y(), o instanceof M ? o : new M({code: "UnknownError", message: `\uACB0\uC81C UI \uB85C\uB529\uC5D0 \uC2E4\uD328\uD558\uC600\uC2B5\uB2C8\uB2E4. ${o.message}`})
	}
}

function Ci(t) {
	let {uiType: e} = t;
	if(!e) throw new M({code: "BadRequest", message: "UI 타입은 필수 입력입니다."});
	if(!Object.keys(w.PaymentUIType).includes(e)) throw new M({code: "BadRequest", message: `\uC694\uCCAD\uD558\uC2E0 UI \uD0C0\uC785(${e})\uC740 \uC77C\uBC18\uACB0\uC81C\uB97C \uC9C0\uC6D0\uD558\uC9C0 \uC54A\uC2B5\uB2C8\uB2E4.`});
	ye.payment[e] = t
}

function ve(t) {
	switch(t) {
		case "PAYPAL_SPB":
			return "PAYPAL"
	}
}

async function Ii(t) {
	if(!t) throw new O({code: "BadRequest", message: "빌링키 발급 요청 파라메터가 누락되었습니다."});
	await Y();
	try {
		let e = await fetch(`${l.CHECKOUT_SERVER}/api/initialize-issue-billing-key/v2`, {method: "POST", headers: {"Content-Type": "application/json"}, body: JSON.stringify({data: t, sdkVersion: l.SDK_VERSION, deviceInfo: x()})});
		if(!e.ok) {
			let p = await e.text(), {code: C, message: c} = (() => {
				try {
					return JSON.parse(p)
				} catch {
					return {code: "UnknownError", message: `\uBE4C\uB9C1\uD0A4 \uBC1C\uAE09 \uCD08\uAE30\uD654\uC5D0 \uC2E4\uD328\uD558\uC600\uC2B5\uB2C8\uB2E4. ${p}`}
				}
			})();
			throw new O({code: C, message: c})
		}
		let r = await e.json(), o = D({pgProvider: r.pgProvider, transactionType: r.transactionType, windowType: r.windowType});
		return await F(o), await f[o].onAfterInitialize?.({slots: l, driverManifest: P, initializeOrPrepareResponse: r}), r
	} catch(e) {
		throw await Y(), L(e) ? e : new O({code: "UnknownError", message: `\uBE4C\uB9C1\uD0A4 \uBC1C\uAE09 \uCD08\uAE30\uD654\uC5D0 \uD638\uCD9C\uC5D0 \uC2E4\uD328\uD558\uC600\uC2B5\uB2C8\uB2E4. ${e.message}`})
	}
}

async function Ti(t) {
	let {pgProvider: e, storeId: r, txId: o, issueId: p, billingKey: C, pgRawRequest: c, windowType: N} = t;
	try {
		let E = await fetch(`${l.CHECKOUT_SERVER}/api/issue-approve/${e}/v2`, {method: "POST", headers: {"Content-Type": "application/json"}, body: JSON.stringify({storeId: r, txId: o, issueId: p, billingKey: C, windowType: N, responseType: "json", ...c})});
		if(!E.ok) {
			let a = await E.text(), i = "UnknownError", s = `\uBE4C\uB9C1\uD0A4 \uBC1C\uAE09 \uC2B9\uC778 \uACFC\uC815\uC5D0\uC11C \uBB38\uC81C\uAC00 \uBC1C\uC0DD\uD558\uC600\uC2B5\uB2C8\uB2E4. ${a}`;
			try {
				let R = JSON.parse(a);
				i = R.code, s = R.message
			} catch {
			}
			throw new O({code: i, message: s, billingKey: C})
		}
		let _ = await E.json();
		return {transactionType: "ISSUE_BILLING_KEY", billingKey: _.billingKey, code: _.code, message: _.message}
	} catch(E) {
		if(L(E)) throw E;
		let {code: _, message: n} = E.response?.data || {code: "UnknownError", message: E.message};
		throw new O({code: _, message: n, billingKey: C})
	}
}

var te = {issueBillingKey: {}}, sr = {...Object.fromEntries(Object.values(w.PgProvider).map(t => [t.substring(12), []])), PAYPAL_V2: ["PAYPAL_RT"]};

async function ar(t, e) {
	let {uiType: r} = t;
	di(t);
	try {
		let {storeId: o, channelKey: p} = t;
		if(!o) throw new K({code: "BadRequest", message: "storeId 파라미터는 필수 입력입니다."});
		if(!p) throw new K({code: "BadRequest", message: "channelKey 파라미터는 필수 입력입니다."});
		let C = {storeId: t.storeId, channelKey: t.channelKey}, N = (await me(C)).pgProvider, E = sr[N.substring(12)] || [];
		if(E.length === 0) throw new K({code: "BadRequest", message: `\uD574\uB2F9 PG\uC0AC(${N})\uB294 UI \uB85C\uB529\uC744 \uC9C0\uC6D0\uD558\uC9C0 \uC54A\uC2B5\uB2C8\uB2E4.`});
		if(!E.includes(r)) throw new K({code: "BadRequest", message: `\uD574\uB2F9 PG\uC0AC(${r})\uC758 UI \uD0C0\uC785\uC740 ${E.join(", ")}\uB9CC \uD5C8\uC6A9\uB429\uB2C8\uB2E4.`});
		let _ = await Ii({...te.issueBillingKey[r], billingKeyMethod: Le(r), actionType: "LOAD_UI"}) ?? void 0;
		if(!_) throw new K({code: "BadRequest", message: "빌링키 발급 UI를 로드하기 위한 사전 요청에 실패하였습니다."});
		let n = D({pgProvider: _.pgProvider, transactionType: _.transactionType, windowType: _.windowType});
		if(!f[n]?.onLoadIssueBillingKeyUI) throw new K({code: "UnknownError", message: "빌링키 발급 UI 로딩을 지원하지 않는 PG사이거나, 모듈 로딩 과정에서 오류가 발생하였습니다."});
		f[n].onLoadIssueBillingKeyUI?.({slots: l, driverManifest: P, issueBillingKeyRequest: {...te.issueBillingKey[r], billingKeyMethod: Le(r)}, initializeOrPrepareResponse: _}, {
			onPrepareIssueBillingKey: async() => {
				try {
					let {prepareResponse: a} = await Te({...te.issueBillingKey[r], billingKeyMethod: Le(r)}, !0);
					return a
				} catch(a) {
					e.onIssueBillingKeyFail(a)
				}
			}, onApproveIssueBillingKey: async a => {
				try {
					let i = await Ti(a);
					e.onIssueBillingKeySuccess(i)
				} catch(i) {
					L(i) ? e.onIssueBillingKeyFail(i) : e.onIssueBillingKeyFail(new O({code: "UnknownError", message: i.message, billingKey: i.billingKey}))
				}
			}, onFailIssueBillingKey: async a => {
				try {
					let i = await ie(a);
					e.onIssueBillingKeyFail(new O({code: i.code, message: i.message ?? "", billingKey: i.billingKey}))
				} catch(i) {
					L(i) ? e.onIssueBillingKeyFail(i) : e.onIssueBillingKeyFail(new O({code: "UnknownError", message: i.message, billingKey: i.billingKey}))
				}
			}, onTransactionOver: () => {
				e.onIssueBillingKeyFail(new O({code: "UnknownError", message: "NotImplemented", billingKey: ""}))
			}
		})
	} catch(o) {
		throw await Y(), o instanceof K ? o : new K({code: "UnknownError", message: `\uACB0\uC81C UI \uB85C\uB529\uC5D0 \uC2E4\uD328\uD558\uC600\uC2B5\uB2C8\uB2E4. ${o.message}`})
	}
}

function di(t) {
	let {uiType: e} = t;
	if(!e) throw new K({code: "BadRequest", message: "UI 타입은 필수 입력입니다."});
	if(!Object.keys(w.IssueBillingKeyUIType).includes(e)) throw new K({code: "BadRequest", message: `\uC694\uCCAD\uD558\uC2E0 UI \uD0C0\uC785(${e})\uC740 \uBE4C\uB9C1\uD0A4 \uBC1C\uAE09\uC744 \uC9C0\uC6D0\uD558\uC9C0 \uC54A\uC2B5\uB2C8\uB2E4.`});
	te.issueBillingKey[e] = t
}

function Le(t) {
	switch(t) {
		case "PAYPAL_RT":
			return "PAYPAL"
	}
}

var xn = he;
export {xn as default, ar as loadIssueBillingKeyUI, te as loadIssueBillingKeyUIRequest, or as loadPaymentUI, er as requestIdentityVerification, rr as requestIssueBillingKey, tr as requestIssueBillingKeyAndPay, ci as requestPayment, l as slots, di as updateLoadIssueBillingKeyUIRequest, Ci as updateLoadPaymentUIRequest};
