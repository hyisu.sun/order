/**
 * 띄워줄 모달에 html을 삽입과 동시에 모달 오픈
 * @param data 서버에서 리턴받은 데이터
 * @param modal 뷰에 띄워줄 모달 요소 .modal
 */
const showModal = (data) => {
	const modal = document.getElementById("modalContainer");


	if(data != null && typeof data == "string") {  // ajax를 통해 받은 데이터의 경우
		modal.innerHTML = data;
	} else {
		modal.innerHTML = `<div class="modal-container">` + data + `</div>`;
	}
	modal.classList.add("modal-toggle");
	document.body.style.overflowY = "hidden";
}


window.addEventListener("mouseup", (eventTarget) => {
	const modal = document.getElementById("modalContainer");

	if(eventTarget.target.classList.contains("modal-toggle-screen")) {
		modal.classList.remove("modal-toggle-screen");
		document.body.style.overflowY = null;
		return false;
	} else if(eventTarget.target.classList.contains("modal-toggle")) {
		modal.classList.remove("modal-toggle");
		modal.innerHTML               = null;
		document.body.style.overflowY = null;
		return false;
	}
});
