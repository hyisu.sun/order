
const updateDeviceInfo = () => {
    const requestData = new URLSearchParams({
        "deviceId": window.document.getElementById("D_I").value,
        "companyCode": window.document.getElementById("companyCode").value,
        "tableNo": window.document.getElementById("tableNo").value
    });

    const xhr = new XMLHttpRequest();
    xhr.open("put", "/order/tablet/register?" + requestData, true);
    xhr.send();
    xhr.onload = () => {
        const responseData = JSON.parse(xhr.response);
        if(xhr.status == 200) {
            if(responseData.resultCode == 1) {
                alert("기기가 정상적으로 등록되었습니다.\r\n이 후 기기 설정은 관리자 웹에서 가능합니다.\r\n메인 화면 접속까지 최대 2초 소요됩니다.");
                window.location.reload();
            }
        }
    }
    xhr.onerror = () => {
        console.log("통신 에러");
    }
}
