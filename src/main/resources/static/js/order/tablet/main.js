wsu.onmessage = (event) => {
	const code = JSON.parse(event.data).code;

	switch(code) {
		case "01":
			findCategory(initData.categoryCode);
			break;

		case "02":
			getCategory();
			break;

		case "05":
			// const companyCode = JSON.parse(event.data).companyCode;
			// const tableNo = JSON.parse(event.data).tableNo;
			const deviceId = JSON.parse(event.data).deviceId;

			const targetDeviceId = window.document.getElementById("D_I").value;

			if(targetDeviceId == deviceId) {     // 태블릿에 등록된 회사 코드와 테이블 번호가 같은 경우에만 초기화를 진행한다.
				window.location.reload();
			}
			break;

		default:
			break;
	}
}


/**
 * @description     2분동안 아무 반응이 없을 경우 대기화면을 적용한다.
 * @since           2024/03/23
 * @author          선희수
 */
const viewScreen = () => {
	const currentTime   = new Date().getTime();
	const lastTouchTime = sessionStorage.getItem("time");

	const actionTime = Math.round(((currentTime - lastTouchTime) / 1000));  // 마지막 터치 후 지난 시간

	if(actionTime >= 300) {  // 초 단위 입력
		const modal = document.getElementById("modalContainer");
		modal.classList.add("modal-toggle-screen");

		return clearInterval(itvViewScreen);
	}
}


/**
 * @description 대기화면 펑션 반복 호출
 * @since       2024/03/23
 * @author      선희수
 */
let itvViewScreen = setInterval(viewScreen, 1000);


/**
 * @since               2024/07/23
 * @author              선희수
 * @description         원하는 쿠키 값 추출
 * @param {*} name      추출 원하는 값
 */
function getCookie(name) {
	let matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
	return matches ? decodeURIComponent(matches[1]) : undefined;
}


/**
 * 1. display 터치, 클릭 이벤트 발생 시 time 세션 값 현재 UTC로 초기화
 * 2. 현재 대기화면이 적용되어 있을 경우에는 기본 화면으로 전환한다.
 *
 * @since   2024/03/24
 * @author  선희수
 */
window.addEventListener("click", () => {
	clearInterval(itvViewScreen);

	sessionStorage.setItem("time", new Date().getTime());
	itvViewScreen = setInterval(viewScreen, 1000);
});


window.addEventListener("load", () => {
	// window.document.getElementById("loadingContainer").classList.add("dp-on");
	// setTimeout(() => {
	getCategory();
	findCategory();

	// window.document.getElementById("loadingContainer").classList.remove("dp-on");
	// }, 2000);

	sessionStorage.setItem("time", new Date().getTime());
});


const getCategory = () => {
	const requestData = new URLSearchParams({});

	const xhr = new XMLHttpRequest();
	xhr.open("get", "/order/tablet/category?" + requestData, true);
	xhr.send();
	xhr.onload = () => {
		if(xhr.status == 200) {
			document.getElementById("categoryArea").innerHTML = xhr.response;
		}
	}
}

const itvGetCategory = setInterval(getCategory, 15000);


/**
 * 카테고리별 메뉴 조회
 * 카테고리 없을 시 전체 메뉴 조회한다.
 */
const findCategory = (categoryCode, categoryName) => {
	const requestData = new URLSearchParams({
		"categoryCode": categoryCode
	});


	const xhr = new XMLHttpRequest();
	xhr.open("get", "/order/tablet/menu?" + requestData, true);
	xhr.send();
	xhr.onload = () => {
		if(xhr.status == 200) {
			document.getElementsByClassName("menus")[0].innerHTML                  = xhr.response;
			document.getElementsByClassName("selected-category-name")[0].innerText = categoryName != undefined ? categoryName : "ALL";
		}
	}
}


const modalMenuOpen = (data) => {
	const requestData = new URLSearchParams({
		// "categoryCode": data.categoryCode !== undefined ? data.categoryCode : "all",  // 카테고리 코드가 지정이 안되어 있을 경우 모든 메뉴를 조회한다.
		"imgCode": data.imgCode,
		"menuExplain": data.menuExplain,
		"menuName": data.menuName,
		"menuPrice": data.menuPrice,
		"menuCode": data.menuCode
	});


	const xhr = new XMLHttpRequest();
	xhr.open("get", "/order/tablet/menu/detail?" + requestData, true);
	xhr.send();
	xhr.onload = () => {
		showModal(xhr.response);
	}
}


/**
 * 메뉴 상세 모달 종료
 */
const modalClose = () => {
	// $(".modal").hide();
	$("body").removeAttr("style");
	$(".wrap-01").removeAttr("style");
}


/**
 * 메뉴 수량/금액 변경 표기
 */

const modalMenuCount = (status) => {
	let selectCount = Number($(".menu-count").val());  // 기존 선택 수량
	let menuPrice   = Number($(".modal-menu-price").data("price"));  // 메뉴 단가

	if(status == "up") {
		document.getElementsByClassName("menu-count")[0].value = (selectCount += 1);
	} else if(status == "down") {
		if(selectCount <= 1) {
			alert("최소 주문 수량은 1개입니다.");
			return;
		} else {
			$(".menu-count").val(selectCount -= 1);
		}
	}

	$(".modal-menu-price").text((selectCount * menuPrice).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));  // 최종 금액 표기
}


/**
 * 장바구니 조회
 */
const findCartMenu = () => {
	const requestData = new URLSearchParams();


	const xhr = new XMLHttpRequest();
	xhr.open("get", "/order/tablet/cart?" + requestData, true);
	xhr.send();
	xhr.onload = () => {
		if(xhr.status == 200) {
			showModal(xhr.response);
		}
	};
}


/**
 * 주문 진행
 */
const doOrder = () => {
	const checkMenuList = document.querySelectorAll(".menu-check:checked");


	const orderData = new FormData();  // 데이터가 배열이기 때문에 FormData 객체 생성
	orderData.append("orderSize", checkMenuList.length);  // 오더 수량
	orderData.append("menuTotalPrice", document.getElementById("menuTotalPrice").dataset.tmp);

	checkMenuList.forEach((data, index) => {  // 메뉴 코드, 메뉴 수량, 메뉴 금액은 선택된 것만 체크하여 formData에 전달한다.
		orderData.append("menuCode", data.dataset.menuCode);
		orderData.append("menuQuantity", data.dataset.menuQuantity);
		orderData.append("menuPrice", data.dataset.menuPrice);
	});


	const xhr = new XMLHttpRequest();
	xhr.open("post", "/order/tablet/ordering", true);
	xhr.send(orderData);
	xhr.onload  = () => {
		if(xhr.status == 200) {
			const responseData = JSON.parse(xhr.response);

			alert(responseData.resultMsg);

			const orderData = {
				"code": responseData.resultCode
			}

			wsSend(orderData);

			document.getElementById("modalContainer").classList.remove("modal-toggle");
		}
	};
	xhr.onerror = (error) => {
		console.log("error code:: " + error);
	};
	modalClose();
};


/**
 * 카트에 담긴 메뉴 전체 선택 시
 */
const allCheckCart = () => {
	const allMenuChecked = document.querySelector(".all-check-cart").checked;
	const menuChecked    = document.querySelectorAll(".menu-check");

	let tmpMenuTotalPrice = 0;

	if(allMenuChecked == false) {
		$(".menu-check").prop("checked", false);

		document.getElementsByClassName("total-menu-price")[0].innerText = 0;

	} else if(allMenuChecked == true) {
		menuChecked.forEach(data => {

			tmpMenuTotalPrice += parseInt(data.dataset.menuPrice);
		});

		$(".menu-check").prop("checked", true);

		document.getElementsByClassName("total-menu-price")[0].innerText = tmpMenuTotalPrice.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
	}
}


const checkCartMenu = (_this) => {
	const checked        = document.querySelectorAll(".menu-check");
	const allCheck       = document.getElementById("allChkCart");
	const menuTotalPrice = document.getElementsByClassName("total-menu-price")[0];

	let tmpMenuTotalPrice = 0;  // 클릭할 때 마다 금액 초기화

	const allChecked = Array.from(checked).every(chk => chk.checked);  // 전부 체크되었는지 확인

	if(allChecked) {
		allCheck.checked = true;
	} else if(!allChecked) {
		allCheck.checked = false;
	}

	checked.forEach(data => {
		if(data.checked == true) {
			tmpMenuTotalPrice += parseInt(data.dataset.menuPrice);
		}
	});
	menuTotalPrice.innerText = tmpMenuTotalPrice.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
}


const addCartMenu = (menuCode) => {
	const menuTotalPrice = Number(document.getElementById("menuTotalPrice").innerText.replaceAll(',', ''));

	const requestData = new FormData();
	requestData.append("menuCode", menuCode);
	requestData.append("menuPrice", menuTotalPrice);
	requestData.append("menuQuantity", window.document.getElementById("menuCnt").value);

	const xhr = new XMLHttpRequest();
	xhr.open("POST", "/order/tablet/cart", true);
	xhr.send(requestData);
	xhr.onload    = () => {
		const responseData = JSON.parse(xhr.response);
		if(xhr.status == 200) {
			alert(responseData.resultMsg);
			document.getElementById("modalContainer").classList.remove("modal-toggle");
		}
	};
	xhr.onerror   = () => {
		alert("장바구니 담는 중 에러가 발생하였습니다.");
	}
	xhr.onloadend = () => {
		document.body.style.overflowY = null;
	}
}


const changeCartMenu = (menuCode) => {
	const menuTotalPrice = Number(document.getElementById("menuTotalPrice").innerText.replaceAll(',', ''));

	const requestData = new FormData();
	requestData.append("menuCode", menuCode);
	requestData.append("menuPrice", menuTotalPrice);
	requestData.append("menuQuantity", document.getElementById("menuCnt").value);


	const xhr = new XMLHttpRequest();
	xhr.open("PUT", "/order/tablet/cart", true);
	xhr.send(requestData);
	xhr.onload    = () => {
		const responseData = JSON.parse(xhr.response);
		if(xhr.status == 200) {
			alert(responseData.resultMsg);
			document.getElementById("modalContainer").classList.remove("modal-toggle");
		}
	};
	xhr.onerror   = () => {
		alert("장바구니 담는 중 에러가 발생하였습니다.");
	}
	xhr.onloadend = () => {
		document.body.style.overflowY = null;
	}
}


const removeCartMenu = () => {
	const chkData = document.querySelectorAll(".menu-check:checked");

	if(chkData.length == 0) {  // 삭제할 메뉴가 없는 경우
		alert("삭제하실 메뉴를 체크하여 주세요.");
		return;
	}

	const needData = {  // 데이터 삭제를 위한 객체
		orderCode: [],
		menuCode: []
	}

	chkData.forEach((item, index) => {
		needData.menuCode[index] = item.dataset.menuCode;
	});


	const requestData = new FormData();
	requestData.append("menuCode", needData.menuCode);


	const xhr = new XMLHttpRequest();
	xhr.open("DELETE", "/order/tablet/cart", true);
	xhr.send(requestData);

	xhr.onprogress = (progress) => {
		// 카트에 담긴 메뉴가 많을 경우 진행 상태가 오래 걸리기 때문에 프로그레스바 구현이 필요함.
	}
	xhr.onload     = () => {
		if(xhr.status == 200) {
			alert("선택하신 메뉴가 카트에서 삭제되었습니다.");
		}
		document.getElementById("modalContainer").classList.remove("modal-toggle");
	}
	xhr.onerror    = () => {
		alert("error");
	}
	xhr.onloadend  = () => {
		document.body.style.overflowY = null;
	}
}


const orderHistory = () => {
	const requestData = new URLSearchParams({});


	const xhr = new XMLHttpRequest();
	xhr.open("get", "/order/tablet/history?" + requestData, true);
	xhr.send();
	xhr.onload  = () => {
		if(xhr.status == 200) {
			showModal(xhr.responseText);
		} else {
			alert("ajax 통신 상태 불량");
		}
	}
	xhr.onerror = () => {
		alert("ajax 통신 실패");
	}
}


/**
 * 스태프 버튼 클릭 시
 * @since  2024/03/10
 * @author  선희수
 */

const callStaff = () => {
	const requestData = new FormData();

	requestData.append("tableNo", document.getElementsByClassName("table-no")[0].dataset.tableNo);

	const xhr = new XMLHttpRequest();
	xhr.open("get", "/order/tablet/call/staff", true);
	xhr.send(requestData);
	xhr.onload  = () => {

		showModal(xhr.responseText);
	}
	xhr.onerror = () => {
		alert("직원 호출 실패");
	}
}


/**
 * 직원 호출 서비스 실제 요청하는 펑션
 *
 * @since  2024/03/24
 * @author  선희수
 */

const callNeedsItemSubmit = () => {
	const itemQty   = document.querySelectorAll(".select-cnt-area");
	let requestData = new FormData();

	itemQty.forEach((item, index) => {

		requestData.append("itemCode", item.dataset.itemCode);

		requestData.append("itemQty", item.value);
	});

	requestData.append("size", itemQty.length);
	requestData.append("tableNo", "22");

	const xhr = new XMLHttpRequest();
	xhr.open("POST", "/order/tablet/call/staff", true);
	xhr.send(requestData);
	xhr.onload  = () => {
		const responseData = JSON.parse(xhr.responseText);
		if(responseData.resultCode == 0) {
			alert("직원 호출에 실패하였습니다.\r\n카운터에 문의하여 주세요.");
		} else if(responseData.resultCode == 1) {
			alert("직원을 호출하였습니다.");

			document.getElementById("modalContainer").classList.remove("modal-toggle");
			wsSend({
				code: "03",
				tableNo: "22"
			});
		}
	}
	xhr.onerror = () => {
	}
}


/**
 * 직원 호출 필요 수량 입력 시 수량 제한 펑션(미사용)
 *
 * @since  2024/03/24
 * @author  선희수
 */
const inputCnt = (_this, cnt) => {
	if(_this.value > cnt) {
		alert("최대 요청가능 수량은" + " " + cnt + "개입니다.");
		_this.value = null;
	}
}


/**
 * @since           2024/04/13(update)
 * @author          선희수
 * @description     결제 진행
 */
const pay = async() => {
	const xhr = new XMLHttpRequest();

	const requestData = new URLSearchParams();
	requestData.append("tableNo", document.getElementsByClassName("table-no")[0].dataset.tableNo);

	// 비동기적으로 결제에 필요한 총 금액 가져오기
	let totalAmount = await new Promise((resolve, reject) => {
		xhr.open("get", "/order/tablet/menu-total-price", true);
		xhr.send();
		xhr.onload  = () => {
			if(xhr.status === 200) {
				const responseData = JSON.parse(xhr.response);

				if(responseData.resultCode == "error") {
					alert(responseData.resultMsg);
					return false;
				} else {
					resolve(Number(responseData.menuTotalPrice));  // Number type으로 형 변환
				}
			} else {
				reject(new Error("Failed to fetch menu total price"));
			}
		};
		xhr.onerror = () => {
			reject(new Error("Failed to fetch menu total price"));
		};
	}).catch(error => {
		console.error(error);
	});

	if(totalAmount <= 0) {  // 결제 금액이 없을경우
		alert("결제하실 금액이 없습니다.\r\n주문 후 결제하여 주시기 바랍니다.");
		return false;
	}

	if(typeof totalAmount != "number") {  // 결제 금액 타입이 number가 아닐경우
		alert("결제 금액 조회하였으나,\r\nnumber type이 아닙니다.");
		alert(totalAmount);
		return false;
	}

	// 결제 검증 단계에서 발생하는 문제의 예외는 여기에서 처리하며, 내부 로직을 바로 return 처리한다.

	const paymentDate  = new Date();
	const paymentYear  = paymentDate.getFullYear().toString();
	const paymentMonth = ('0' + (paymentDate.getMonth() + 1)).slice(-2);
	const paymentDay   = ('0' + (paymentDate.getDate() + 1)).slice(-2);
	const paymentId    = paymentYear + paymentMonth + paymentDay + Math.random().toString().substring(2);

	if(confirm("결제하시겠습니까?")) {  // 결제 예정 금액을 호출이 완료되면 고객에게 컨펌을 받는다.
		const requestPay = await PortOne.requestPayment({  // 모든 로직을 거친 후 이상 없을경우 결제 진행 모듈 호출
			storeId: "store-5a10b66f-2550-4775-9f5b-255f83e9d56e",
			channelKey: "channel-key-7a394663-d5d0-43df-a69b-31e231fa36ff",
			paymentId: paymentId,
			orderName: "큐오더 테스트 결제",
			totalAmount: totalAmount,
			currency: "KRW",
			payMethod: "CARD",
			isTestChannel: true,
			windowType: {
				mobile: "REDIRECTION"
			},
			redirectUrl: "http://shse.iptime.org/order/tablet"
		});
		if(requestPay.code != null) {  // 해당 조건문 내부에는 코드가 존재할 시(에러) 로직을 작성해준다.
			if(requestPay.code == "FAILURE_TYPE_PG") {
				alert("결제를 취소하였습니다.");
				return false;
			} else {
				alert("알 수 없는 에러 발생" + "\r\n" + requestPay.code + "|" + requestPay.message);
				return false;
			}
		} else {  // 에러 코드가 없을 경우 내부 로직 작성
			const requestData = new FormData();
			requestData.append("paymentId", requestPay.paymentId);

			xhr.open("post", "/payment/complete", true);
			xhr.send(requestData);
			xhr.onload  = () => {
				if(xhr.status == 200) {
					const responseData = JSON.parse(xhr.responseText);
					if(responseData.payStatus == "PAID") {  // 결제 완료 시
						alert("주문 및 결제가 완료되었습니다.\r\n금방 조리 후 가져다 드릴게요!");
					} else {
						alert("결제에 실패하였습니다.\r\n직원에게 문의바랍니다.");
						console.log(responseData.payStatus);
					}
				} else {
					alert("error" + "|" + xhr.status);
				}
			}
			xhr.onerror = () => {
				alert("최종 결제 단계에서 실패하였습니다.\r\n::console::");
			}
		}
	} else {
		alert("결제를 취소하였습니다.");
	}
};


const adminLogin = () => {
	alert("구현중인 기능입니다.");
}
