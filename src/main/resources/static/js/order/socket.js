const wsu = new WebSocket("ws://" + location.host + "/api/socket");


const wsCode = {
	connCode: false
}


const wsSend = (data) => {
	try {
		wsu.send(JSON.stringify(data));
	} catch(error) {
		console.log(error);
	}
}


wsu.onopen = () => {
	// alert("연결되었습니다.");
}


wsu.onclose = () => {
	// alert("연결이 끊어졌습니다.\r\n확인 버튼을 클릭하시면 재연결을 시도합니다.");
	window.location.reload();
	// wsu;
}


wsu.onerror = (error) => {
	console.log(error);
}
