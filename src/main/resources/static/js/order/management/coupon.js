function insertCoupon() {
	fetch("/order/management/coupon/info", {
		method: "POST",
		headers: {
			accept: "application/json"
		},
		body: new FormData(document.getElementById("couponForm"))
	}).then((response) => response.json())
	  .then((data) => {
		  if(data.returnMsg != null) {
			  alert(data.returnMsg);
		  } else {
			  alert("알 수 없는 에러가 발생하였습니다.");
		  }

	  });
}


function couponDetail(couponCode) {
	console.log(location.href);

	let url = new URL("./coupon/info", location.href);
	url.searchParams.set("couponCode", couponCode);

	location.href = url;
}
