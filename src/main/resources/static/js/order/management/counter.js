window.addEventListener("load", () => {  // DOM 로드 시
	const ac = new (AudioContext);
	if(ac.state == "running") {  // 상태 값이 동작인 경우
		getOrderList();
	} else {
		alert("실시간 주문 현황이 작동하지 않습니다.\r\n브라우저 설정에서 소리 재생을 허용해주세요.");
	}

	getOrderList();


	const viewMode = new URL(window.location.href).searchParams.get("view-mode");
	const initData = {
		"viewMode": viewMode
	}
});


// 주어진 이름의 쿠키를 반환하는데,
// 조건에 맞는 쿠키가 없다면 undefined를 반환합니다.
const getCookie = (name) => {
	let matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
	return matches ? decodeURIComponent(matches[1]) : undefined;
}


wsu.onmessage = (event) => {
	const code = JSON.parse(event.data).code;

	switch(code) {
		case "03":  // 직원 호출 시
			alert("직원 호출");

		case "04":  // 주문
			getOrderList();
			break;

		case "05":  // 주문
			getOrderList();
			break;

		default:
			break;
	}
}


const bell = new Audio("/sound/ordering3.mp3");
bell.loop = true;
bell.volume = 0.2;

const callStaffBell = new Audio("/sound/call-staff.mp3");
callStaffBell.volume = 0.2;


/**
 * 주문 현황 상태 변경
 */
const changeOrderStatus = () => {
	const orderList = document.querySelectorAll(".order-list");

	let isOrderStatus = false;  // 기본 주문 상태

	orderList.forEach((item, index) => {
		if(item.dataset.orderStatus == "주문") {
			item.style.backgroundColor = "rgba(222, 22, 22, 0.8)";
			item.style.color = "white";
			isOrderStatus = true;

			bell.play();
		} else if(item.dataset.orderStatus == "확인") {
			item.style.backgroundColor = "rgba(222, 222, 22, 0.8)";
			item.style.color = "black";
		} else if(item.dataset.orderStatus == "제공") {
			item.style.backgroundColor = "rgba(22, 222, 22, 0.8)";
			item.style.color = "black";
		}
	});

	if(!isOrderStatus) {  // "주문" 상태가 없을 경우 벨 소리 중단
		bell.pause();


		bell.currentTime = 0;
	}
}


/**
 * @since           2024/06/09
 * @author          선희수
 * @description     제공완료 클릭 시 상태 변경
 */
const changeOrderComplete = (orderCode) => {
	if(document.getElementsByClassName("input-order-status")[0].value == "제공") {
		alert("이미 제공이 완료된 주문 건입니다.");
		document.getElementById("modalContainer").classList.remove("modal-toggle");

		return false;
	}


	if(confirm("정말로 제공 완료 처리를 하시겠습니까?\r\n제공이 완료되면 되돌릴 수 없습니다.")) {
		const requestData = new FormData();
		requestData.append("orderCode", orderCode);
		requestData.append("tableNo", document.getElementsByClassName("input-table-no")[0].value);


		const xhr = new XMLHttpRequest();
		xhr.open("PUT", "/order/management/order/offer/done", true);
		xhr.send(requestData);
		xhr.onload = () => {
			if(xhr.status == 200) {
				const responseData = JSON.parse(xhr.response);
				if(responseData.result == "01") {
					alert("제공 완료 처리하였습니다.");
					getOrderList();
				} else if(responseData.result == "02") {
					alert("제공 완료 처리 실패하였습니다.");
				}
				document.getElementById("modalContainer").classList.remove("modal-toggle");
			}
		}
		xhr.onerror = () => {
			console.log("error");
		}
	}
}


/**
 * 주문 현황 조회
 * @since   2024/03/10
 * @author  선희수
 */
const getOrderList = () => {
	const viewMode = getCookie("viewMode");

	const requestData = new URLSearchParams();
	requestData.append("viewMode", viewMode);
	if(viewMode == "l") {
		requestData.append("orderStatus", document.getElementById("selectSearchOrderStatusCondition").value);
		requestData.append("calculateStatus", document.getElementById("searchCalculateCondition").value);
	}


	const xhr = new XMLHttpRequest();
	xhr.open("get", "/order/management/counter/list?" + requestData, true);
	xhr.send();
	xhr.onload = () => {
		if(xhr.status == 200) {
			if(viewMode == 'l') {
				document.getElementById("listViewModeData").innerHTML = xhr.response;
			} else if(viewMode == 't') {
				document.getElementsByClassName("table-container")[0].innerHTML = xhr.response;
			}


			changeOrderStatus();
		} else {
			alert("요청에 대한 응답 코드에서 에러가 발생하였습니다.");
		}
	}
	xhr.onerror = () => {
		alert("xhr error..!!");
	}
}


/**
 * description      주문 상세현황 확인
 */
const clickOrderList = (_this, requestData, viewMode) => {
	if(requestData.orderCode == null) {
		alert("주문 내역이 존재하지 않습니다.");
		return false;
	}

	if(_this.dataset.orderStatus == "주문") {
		const sendData1 = new URLSearchParams();
		sendData1.append("orderCode", requestData.orderCode);


		const xhr = new XMLHttpRequest();

		xhr.open("put", "/order/management/counter/list?" + sendData1, true);
		xhr.send();
		xhr.onload = (e) => {
			if(xhr.status == 200) {
				getOrderList();
			} else {
				console.log("통신 상태 값 불량" + "\r\n" + xhr.status);
			}
		}
		xhr.onerror = () => {
			alert("xhr error..!!");
		}
	}


	const xhr = new XMLHttpRequest();

	const sendData2 = new URLSearchParams();
	sendData2.append("orderCode", requestData.orderCode);
	sendData2.append("tableNo", requestData.tableNo);
	sendData2.append("orderStatus", requestData.orderStatus);

	xhr.open("get", "/order/management/order/list/detail?" + sendData2, true);
	xhr.send();
	xhr.onload = () => {
		if(xhr.status == 200) {
			showModal(xhr.responseText);
		}
	}
	xhr.onerror = () => {
		alert("xhr error..!!");
	}
}


const transferView = (viewMode) => {
	const requestData = new URLSearchParams();
	requestData.append("viewMode", viewMode);


	const xhr = new XMLHttpRequest();
	xhr.open("GET", "/order/management/counter/view-mode?" + requestData);
	xhr.send();
	xhr.onload = () => {
		console.log("success")
	}
	xhr.onerror = () => {
		console.log("view transfer error")
	}
}


const transView = () => {
	const value = document.getElementById("transView").value;


	document.cookie = "viewMode=" + value;
	window.location.reload();
}
