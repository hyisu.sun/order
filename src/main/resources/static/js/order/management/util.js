/**
 * @since               2024/07/23
 * @author              선희수
 * @description         원하는 쿠키 값 추출
 * @param {*} name      추출 원하는 값
 */
function getCookie (name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}


export {getCookie};
