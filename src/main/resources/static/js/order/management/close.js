// import {getCookie} from "../order/management/util.js";


window.document.getElementById("closeSaleBtn").addEventListener("click", () => {
    const saleDate = window.document.getElementById("saleDate").innerText;
    if(confirm("영업일자 : " + saleDate + "\r\n" + "영업을 마감하시겠습니까?")) {
        const xhr = new XMLHttpRequest();
        xhr.open("get", "/order/management/close-data", true);
        xhr.send();
        xhr.onload = () => {
            const responseData = JSON.parse(xhr.response);

            alert(responseData.resultMsg);

            if(responseData.resultCode == "01") {
                location.reload();
            }
        }
        xhr.onerror = () => {
            alert("에러가 발생하였습니다.");
        }
    } else {
        alert("당일 영업마감 취소");
    }
});
