const OPEN_MENU = {};

window.addEventListener("load", () => {
	const target = document.getElementsByClassName("menu-title");
	let ls       = JSON.parse(sessionStorage.getItem("OPEN_MENU"));


	if(JSON.parse(sessionStorage.getItem("OPEN_MENU")) == null) {
		Array.from(target).forEach((item) => {
			OPEN_MENU[item.id] = false;
		});
		sessionStorage.setItem("OPEN_MENU", JSON.stringify(OPEN_MENU));
	}


	// 로컬 스토리지에 저장된 확장 메뉴 값을 비교, true 시 메뉴 확장
	const expandTarget = document.getElementsByClassName("move-management");
	Array.from(expandTarget).forEach((item) => {
		if(ls[item.dataset.menu] != false) {
			item.classList.remove("dp-none");
		}
	});
});


function sideMenuExpand(type) {
	const targetMenus = document.getElementsByClassName("move-management");


	let openMenu   = JSON.parse(sessionStorage.getItem("OPEN_MENU"));
	openMenu[type] = !openMenu[type];
	sessionStorage.setItem("OPEN_MENU", JSON.stringify(openMenu));


	Array.from(targetMenus).forEach((item) => {
		if(item.dataset.menu == type) {  // 선택한 메뉴 값과 dp-none 값이 같을 경우
			if(item.classList.contains("dp-none")) {
				item.classList.remove("dp-none");
			} else {
				item.classList.add("dp-none");
			}
		}
	});
}
