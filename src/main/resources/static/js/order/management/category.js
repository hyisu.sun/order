window.addEventListener("load", () => {
	getCategoryList();
});


/**
 * @description     관리자 웹 카테고리 리스트 조회
 * @since           2024/03/02
 * @author          선희수
 */
const getCategoryList = () => {
	const xhr = new XMLHttpRequest();
	xhr.open("get", "/order/management/category/list", true);
	xhr.send();
	xhr.onload = () => {
		document.getElementById("list-data").innerHTML = xhr.response;
	}
}


/**
 * @since       2024/03/02
 * @author      선희수
 * @description 카테고리 리스트 클릭 시 상세 모달 오픈
 */
const openCategoryListDtl = (categoryCode) => {
	const requestData = new URLSearchParams();
	requestData.append("categoryCode", categoryCode);

	const xhr = new XMLHttpRequest();
	xhr.open("GET", "/order/management/category/list/detail?" + requestData, true);
	xhr.send();
	xhr.onload = () => {
		showModal(xhr.responseText);
		flatpickr(".hour", {
			enableTime: true,
			noCalendar: true,
			dateFormat: "H:i",
			time_24hr: true,
			minuteIncrement: 1
		});
	}
}


/**
 * @since           2024/03/09
 * @author          선희수
 * @description     카테고리 업데이트 로직
 * @method          PUT
 */
const changeCategory = () => {
	const requestData = new FormData();
	requestData.append("openTime", document.getElementsByName("openTime")[0].value.replace(":", ""));
	requestData.append("closeTime", document.getElementsByName("closeTime")[0].value.replace(":", ""));
	requestData.append("useYn", document.getElementsByName("useYn")[0].value);
	requestData.append("categoryName", document.getElementsByName("categoryName")[0].value);
	requestData.append("categoryCode", document.getElementsByName("categoryCode")[0].value);

	const xhr = new XMLHttpRequest();
	xhr.open("PUT", "/order/management/category/list/detail", true);
	xhr.send(requestData);
	xhr.onload = () => {
		const responseData = JSON.parse(xhr.response);
		if(xhr.status == 200) {
			if(responseData.resultCode == "01") {
				wsSend({code: "02"});
				alert(responseData.resultMsg);
				getCategoryList();
				document.getElementById("modalContainer").classList.remove("modal-toggle");
			} else if(responseData.resultCode == "02") {
				alert(responseData.resultMsg);
				console.log("error" + "|" + xhr.status);
			}
		}
	}
	xhr.onerror = () => {
		alert("에러가 발생하였습니다.");
		window.location.reload();
	}
}


/**
 * @since           2024/03/09
 * @author          선희수
 * @description     카테고리 업데이트 로직
 * @method          POST
 */
const addCategory = () => {
	const requestData = new FormData();
	requestData.append("openTime", document.getElementsByName("openTime")[0].value.replace(":", ""));
	requestData.append("closeTime", document.getElementsByName("closeTime")[0].value.replace(":", ""));
	requestData.append("useYn", document.getElementsByName("useYn")[0].value);
	requestData.append("categoryName", document.getElementsByName("categoryName")[0].value);

	const xhr = new XMLHttpRequest();
	xhr.open("PUT", "/order/management/category/list/detail", true);
	xhr.send(requestData);
	xhr.onload = () => {
		const responseData = JSON.parse(xhr.response);
		if(xhr.status == 200) {
			if(responseData.resultCode == "01") {
				wsSend({code: "02"});
				alert(responseData.resultMsg);
				getCategoryList();
				document.getElementById("modalContainer").classList.remove("modal-toggle");
			} else if(responseData.resultCode == "02") {
				alert(responseData.resultMsg);
				console.log("error" + "|" + xhr.status);
			}
		}
	}
	xhr.onerror = () => {
		alert("에러가 발생하였습니다.");
		window.location.reload();
	}
}


/**
 * @since           2024/06/02
 * @author          선희수
 * @description     카테고리 삭제 펑션
 */
const deleteCategory = (companyCode, categoryCode) => {
	const requestData = new FormData();
	requestData.append("companyCode", companyCode);
	requestData.append("categoryCode", categoryCode);


	const xhr = new XMLHttpRequest();
	xhr.open("DELETE", "/order/management/category/list/detail", true);

	if(confirm("정말 삭제하시겠습니까?")) {
		xhr.send(requestData);
	} else {
		alert("작업을 취소하였습니다.");
		return false;
	}


	xhr.onload = () => {
		if(xhr.status == 200) {
			const responseData = JSON.parse(xhr.response);
			alert(responseData.resultMsg);
			getCategoryList();
			document.getElementById("modalContainer").classList.remove("modal-toggle");
		}
	}
	xhr.onerror = () => {
		alert("api 통신 에러 error");
	}
}
