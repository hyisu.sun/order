window.addEventListener("load", () => {
	// getMenuList();
});


/**
 * @author          선희수
 * @since           2024/03/05
 * @description     조회 내역 항목 클릭 시 수정 가능한 상세 모달 창 오픈
 * @description     파라미터 없을경우 신규 메뉴 등록 모달 오픈
 */
const menuDetail = (menuCode) => {
	let type = 'C';
	if(menuCode == null || menuCode == undefined) {
		type = 'N';
	}

	const requestData = new URLSearchParams();
	requestData.append("menuCode", menuCode);
	requestData.append("type", type);

	const xhr = new XMLHttpRequest();
	xhr.open("get", "/order/management/menu/list/detail?" + requestData, true);
	xhr.send();
	xhr.onload = () => {
		if(xhr.status == 200) {
			showModal(xhr.responseText);
		}
	}
}


/**
 * @author          선희수
 * @description     메뉴 적용버튼 클릭 시 펑션
 * @description     필수 입력 값 체크는 서버에서 처리한다.
 */
const applyContent = (type) => {
	const menuImg = document.getElementById("menuImg").value;


	const requestData = new FormData();
	if(type == "post" || type == "put") {
		requestData.append("menuName", document.getElementById("menuName").value);
		requestData.append("menuPrice", Number(document.getElementById("menuPrice").value.replaceAll(",", "")));
		requestData.append("categoryCode", document.getElementById("categoryCode").value);
		requestData.append("menuExplain", document.getElementById("menuExplain").value);
		requestData.append("showYn", document.getElementById("showYn").value);
		requestData.append("bestYn", document.getElementById("bestYn").value);
		if(type == "put") {
			requestData.append("menuCode", document.getElementsByClassName("menu-code")[0].value);
		}
		requestData.append("type", type);
		if(menuImg.length !== 0) {
			requestData.append("menuImg", document.getElementById("menuImg").files[0]);
		}


		const xhr = new XMLHttpRequest();
		xhr.open("post", "/order/management/menu/list/detail", true);
		xhr.send(requestData);
		xhr.onload = () => {
			const responseData = JSON.parse(xhr.response);

			if(xhr.status == 200) {
				if(responseData.resultCode == "01") {
					alert(responseData.resultMsg);
					location.reload();
					// document.getElementById("modalContainer").classList.remove("modal-toggle");
				} else if(responseData.resultCode == "02") {
					alert(responseData.resultMsg);

					return false;
				}
				wsSend({code: "01"});
			}
		}
		xhr.onerror = () => {
			alert("작업중 에러가 발생하였습니다.");
		}
	}


	if(type == "delete") {
		const requestData = new URLSearchParams();
		requestData.append("menuCode", document.getElementsByClassName("menu-code")[0].value);
		requestData.append("type", type);


		const xhr = new XMLHttpRequest();
		xhr.open("post", "/order/management/menu/list/detail", true);
		xhr.open("delete", "/order/management/menu/list/detail?" + requestData, true);
		xhr.send();
		xhr.onload = () => {
			const responseData = JSON.parse(xhr.response);

			if(xhr.status == 200) {
				if(responseData.resultCode == "01") {
					alert(responseData.resultMsg);
					getMenuList();
					document.getElementById("modalContainer").classList.remove("modal-toggle");
				} else if(responseData.resultCode == "02") {
					alert(responseData.resultMsg);

					return false;
				}
				wsSend({code: "01"});
			}
		}
		xhr.onerror = () => {
			alert("작업중 에러가 발생하였습니다.");
		}
	}
}


/**
 * @author          선희수
 * @description     메뉴금액 입력 시 쉼표 자동 추가 펑션
 * @since           2024/07/20
 */
const priceToAddComma = () => {
	let target = document.getElementById("menuPrice");

	target.value = target.value.replace(/[^0-9.-]/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


const previewImg = (type) => {
	let previewTarget = document.getElementById("imgPreview");
	let imgSrc = URL.createObjectURL(this.event.target.files[0]);
	previewTarget.src = imgSrc;
}


/**
 * 2024/03/05
 * 페이지 접근 시 자동 조회
 * 조회 조건(카테고리) 선택 시 자동 조회
 * 메뉴명 입력 시 자동 조회
 */
const getMenuList = (type) => {
	let categoryCode = document.getElementById("categoryCode").value;
	const menuName = document.getElementsByClassName("search-menu-name")[0].value;


	const requestData = new URLSearchParams();
	requestData.append("categoryCode", categoryCode);
	requestData.append("menuName", menuName);


	const xhr = new XMLHttpRequest();
	xhr.open("get", "/order/management/menu?" + requestData, true);
	xhr.send();
	xhr.onload = () => {
		if(xhr.status == 200) {
			document.getElementById("menuList").innerHTML = xhr.response;
		}
	}
}


function movePage(currentPage) {
	const form = document.getElementById("searchFormData");
	const formData = new FormData(form);
	formData.append("currentPage", currentPage);
	const serializeData = new URLSearchParams(formData).toString();


	location.href = "?" + serializeData;
}
