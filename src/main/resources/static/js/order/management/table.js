window.addEventListener("DOMContentLoaded", () => {  // DOM 요소만 로딩된 이후 바로
	const tableList = document.querySelectorAll(".table-items");

	tableList.forEach((item, index) => {
		if(item.dataset.mappingYn == 'N') {  // 기본 조회한 테이블 리스트에서 매핑 값이 N일 경우 배경 색상 변경 처리
			item.style.backgroundColor = "rgba(222, 222, 222, 0.2)";
		}
	});
});


/**
 * 테이블 상세 정보 조회
 * @since: 2024/03/16
 * @author: 선희수
 */
const getTableInfoDtl = (tableNo, deviceId) => {
	const xhr = new XMLHttpRequest();

	const requestData = new URLSearchParams({
		"tableNo": tableNo,
		"deviceId": deviceId
	});

	xhr.open("get", "/order/management/table/info/detail?" + requestData, true);
	xhr.send();
	xhr.onload = () => {
		if(xhr.status == 200) {
			showModal(xhr.response);
		} else {
			console.log("ajax error");
		}
	}
	xhr.onerror = () => {
		console.log("ajax error");
	}
}


/**
 * 테이블 번호 변경
 */
const changeTable = (companyCode, tableNo, orderCode, deviceId) => {
	const requestData = new URLSearchParams();
	requestData.append("companyCode", companyCode);
	requestData.append("tableNo", document.getElementById("tableNo").value);
	requestData.append("orderCode", orderCode);
	requestData.append("deviceId", deviceId);

	fetch("/order/management/table?" + requestData, {
		method: "put"
	}).then((response) => {
		if(response.status == 200 || response.ok) {
			return response.text();
		}
	}).then((data) => {
		alert(data);
	}).catch((error) => {
		alert("알 수 없는 에러가 발생하였습니다.");
		console.error(error);
	}).finally(() => {
		window.location.reload();
	});
}


/**
 * 테이블 정보 초기화
 * @since: 2024/03/16
 * @author: 선희수
 */
const resetTable = (companyCode, tableNo, orderCode, deviceId) => {
	const requestData = new FormData();
	requestData.append("companyCode", companyCode);
	requestData.append("tableNo", tableNo);
	requestData.append("orderCode", orderCode);
	requestData.append("deviceId", deviceId);


	const xhr = new XMLHttpRequest();
	xhr.open("put", "/order/management/table", true);
	xhr.send(requestData);
	xhr.onload = () => {
		if(xhr.status == 200) {
			const responseData = xhr.response;
			console.log(responseData)

			if(responseData == '1') {
				alert("초기화되었습니다.");
				wsSend(
					{
						"code": "05",
						"companyCode": companyCode,
						"tableNo": tableNo,
						"deviceId": deviceId
					}
				);
			} else if(responseData == '2') {
				alert("해당 테이블의 미제공 메뉴가 존재합니다.\r\n모든 메뉴를 제공완료 처리하세요.");
			} else if(responseData == '3') {
				alert("테이블을 초기화가 불가능한 상태입니다.\r\n해당 단말기 재실행 요망");
			} else if(responseData == '99') {
				alert("알 수 없는 에러가 발생하였습니다.");
			}
		}
	}
	xhr.onerror = () => {
		console.log("reset table ajax error");
	}
	xhr.onloadend = () => {
		window.location.reload();
	}
}
