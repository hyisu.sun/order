package com.web.order.config;


import com.web.order.app.orderManagement.controller.OrderManagementInterceptor;
import com.web.order.app.orderTablet.controller.OrderTabletInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
	@Bean
	public OrderManagementInterceptor ordermanagementInterceptor() {
		return new OrderManagementInterceptor();
	}


	@Bean
	public OrderTabletInterceptor orderTabletInterceptor() {
		return new OrderTabletInterceptor();
	}


	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(ordermanagementInterceptor()).excludePathPatterns("/css/**", "/img/**", "/js/**").excludePathPatterns("/order/management/login.do");
		registry.addInterceptor(orderTabletInterceptor()).excludePathPatterns("/css/**", "/img/**", "/js/**");
	}


	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**").allowedOrigins("http://localhost:5173");
	}

}
