package com.web.order.app.common.model.data;


/**
 * 페이징 처리에 필요한 기본 데이터 객체
 *
 */
public class Pagination {
	private int currentPage;
	private int rowCount;
	private int offset;
	private int limit;


	public int getCurrentPage() {
		return this.currentPage;
	}


	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage - 1;
	}


	public int getRowCount() {
		return this.rowCount;
	}


	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}


	public int getOffset() {
		return this.currentPage * this.limit;
	}


	public void setOffset(int offset) {
		this.offset = offset;
	}


	public int getLimit() {
		return this.limit;
	}


	public void setLimit(int limit) {
		this.limit = limit;
	}
}
