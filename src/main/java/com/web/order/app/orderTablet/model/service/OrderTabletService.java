package com.web.order.app.orderTablet.model.service;


import com.web.order.app.orderTablet.model.data.OrderTablet;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;


@Service
public class OrderTabletService {
	private static final String QUERY_ID_PREFIX = "orderTablet";


	private final Logger logger = Logger.getLogger("OrderTabletService");


	@Autowired
	private SqlSession sqlSession;


	public List<OrderTablet> selectCategory(OrderTablet condition) {
		return sqlSession.selectList(QUERY_ID_PREFIX + ".selectCategory", condition);
	}


	public List<OrderTablet> selectMenu(OrderTablet condition) {
		return sqlSession.selectList(QUERY_ID_PREFIX + ".selectMenu", condition);
	}


	public List<OrderTablet> selectOrderHistory(OrderTablet condition) {
		return sqlSession.selectList(QUERY_ID_PREFIX + ".selectOrderHistory", condition);
	}


	public List<OrderTablet> selectNeedsItem(OrderTablet condition) {
		return sqlSession.selectList(QUERY_ID_PREFIX + ".selectNeedsItem", condition);
	}


	public int insertCallStaff(OrderTablet condition) {
		return sqlSession.insert(QUERY_ID_PREFIX + ".insertCallStaff", condition);
	}


	public OrderTablet selectNotCalMenuTotalPrice(OrderTablet condition) {
		return sqlSession.selectOne(QUERY_ID_PREFIX + ".selectNotCalMenuTotalPrice", condition);
	}


	public OrderTablet selectOrderCodeSession(OrderTablet condition) {
		return sqlSession.selectOne(QUERY_ID_PREFIX + ".selectOrderCodeSession", condition);
	}


	public int insertOrderCodeSession(OrderTablet condition) {
		return sqlSession.insert(QUERY_ID_PREFIX + ".insertOrderCodeSession", condition);
	}


	public int updateDeviceInfo(OrderTablet condition) {
		return sqlSession.update(QUERY_ID_PREFIX + ".updateDeviceInfo", condition);
	}


	public OrderTablet selectDeviceInfo(OrderTablet condition) {
		return sqlSession.selectOne(QUERY_ID_PREFIX + ".selectDeviceInfo", condition);
	}


	public int insertDeviceInfo(OrderTablet condition) {
		return sqlSession.insert(QUERY_ID_PREFIX + ".insertDeviceInfo", condition);
	}


	/**
	 * 카트 메뉴 담기
	 */
	public int insertCartMenu(OrderTablet condition) {
		return sqlSession.insert(QUERY_ID_PREFIX + ".insertCartMenu", condition);
	}


	/**
	 * 카트 메뉴 업데이트
	 */
	public int updateCartMenu(OrderTablet condition) {
		return sqlSession.update(QUERY_ID_PREFIX + ".updateCartMenu", condition);
	}


	/**
	 * 카트에 담긴 메뉴 조회
	 */
	public List<OrderTablet> selectCartMenu(OrderTablet condition) {
		return sqlSession.selectList(QUERY_ID_PREFIX + ".selectCartMenu", condition);
	}


	/**
	 * 카트에 중복 메뉴 조회
	 */
	public OrderTablet selectDuplicateCartMenu(OrderTablet condition) {
		return sqlSession.selectOne(QUERY_ID_PREFIX + ".selectDuplicateCartMenu", condition);
	}


	/**
	 * 카트에 담긴 메뉴 삭제(선택한 항목만)
	 */
	public int deleteCartMenu(OrderTablet condition) {
		return sqlSession.delete(QUERY_ID_PREFIX + ".deleteCartMenu", condition);
	}


	/**
	 * 주문 진행(카트에 담겨있고 선택한 메뉴만 해당)
	 */
	public int insertOrdering(OrderTablet condition) {
		return sqlSession.insert(QUERY_ID_PREFIX + ".insertOrdering", condition);
	}


	public int insertOrderingDtl(OrderTablet condition) {
		return sqlSession.insert(QUERY_ID_PREFIX + ".insertOrderingDtl", condition);
	}


	public int updateCartMenuStatus(OrderTablet condition) {
		return sqlSession.delete(QUERY_ID_PREFIX + ".updateCartMenuStatus", condition);
	}


	public OrderTablet selectMenuConfirm(OrderTablet condition) {
		return sqlSession.selectOne(QUERY_ID_PREFIX + ".selectMenuConfirm", condition);
	}


	public OrderTablet selectBeforeOrderCode(OrderTablet condition) {
		return sqlSession.selectOne(QUERY_ID_PREFIX + ".selectBeforeOrderCode", condition);
	}


	public int updateOrdering(OrderTablet condition) {
		return sqlSession.update(QUERY_ID_PREFIX + ".updateOrdering", condition);
	}


	public OrderTablet selectBeforeOrderDtl(OrderTablet condition) {
		return sqlSession.selectOne(QUERY_ID_PREFIX + ".selectBeforeOrderDtl", condition);
	}


	public int updateOrderingDtl(OrderTablet condition) {
		return sqlSession.update(QUERY_ID_PREFIX + ".updateOrderingDtl", condition);
	}
}
