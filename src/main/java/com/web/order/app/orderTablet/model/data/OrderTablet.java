package com.web.order.app.orderTablet.model.data;


import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class OrderTablet {


	public OrderTablet() {}


	public OrderTablet(String companyCode, String deviceId, String orderCode) {
		this.companyCode = companyCode;
		this.deviceId = deviceId;
		this.orderCode = orderCode;
	}


	private String companyCode;     //  회사코드
	private String companyName;     //  회사이름
	private String categoryName;        //  카테고리 이름
	private String categoryCode;        //  카테고리 코드
	private String menuQuantity;        //  메뉴수량
	private String menuName;        //  메뉴이름
	private String menuExplain;     //  메뉴설명
	private String menuImg;     //  메뉴사진
	private String menuPrice;       //  메뉴금액
	private String menuCode;
	private String imgCode;
	private String imgExtension;
	private String tableNo;
	private String bestYn;
	private String deviceId;
	private String useYn;       //  사용여부
	private String itemCode;
	private String itemName;
	private String itemMinQty;
	private String itemMaxQty;
	private String itemQty; // DBMS: INT
	private String payStatus;
	private String menuTotalPrice; // 메뉴최종금액
	private String orderCode;

	private String status;
	private String orderStatus;
	private String memberType;
	private String isrtDate;
	private String updtDate;
	private String calculateStatus;
	private String updtType; // 업데이트 구분
	private String idKey;
	private String orderSize; // 유효성 검증을 위함(단순 데이터)


	/*  */
	private int size;
	private String[] menuCodes;

}
