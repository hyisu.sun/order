package com.web.order.app.orderTablet.controller;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.web.order.app.orderTablet.model.data.OrderTablet;
import com.web.order.app.orderTablet.model.service.OrderTabletService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Logger;


@Controller
@RequestMapping("/order/tablet")
@SessionAttributes("orderTablet")
public class OrderTabletController {


	Logger orderTabletLogger = Logger.getLogger("OrderTabletController");


	@Autowired
	private OrderTabletService orderTabletService;


	@Value("${menu.img.view.path}")
	private String imgViewPath;


	@GetMapping({"", "/"})
	public String mainTablet(OrderTablet orderTablet, Model model, HttpSession session) {
		orderTablet.setDeviceId(session.getAttribute("deviceId").toString());


		OrderTablet selectDeviceInfo = orderTabletService.selectDeviceInfo(orderTablet);

		if(selectDeviceInfo == null) { // 최초 태블릿으로 앱 접속했을 경우(기기 정보가 없는 경우)
			int insertDeviceInfo = orderTabletService.insertDeviceInfo(orderTablet);
			if(insertDeviceInfo != 0) {
				return "redirect:/register";
			}
		}


		if(selectDeviceInfo.getCompanyCode() == null || selectDeviceInfo.getTableNo() == null) { // 디바이스 UUID 값이 존재할 경우
			model.addAttribute("deviceId", orderTablet.getDeviceId());
			return "redirect:/register";
		}


		orderTablet.setCompanyCode(selectDeviceInfo.getCompanyCode());
		orderTablet.setTableNo(selectDeviceInfo.getTableNo());


		try { // 오더코드 만료되었을 경우 신규 등록
			OrderTablet selectOrderCodeSession = orderTabletService.selectOrderCodeSession(orderTablet);

			if(selectOrderCodeSession == null) {
				try {
					orderTablet.setOrderCode(UUID.randomUUID().toString());

					int insertOrderCodeSession = orderTabletService.insertOrderCodeSession(orderTablet); // 신규 등록

					if(insertOrderCodeSession != 0) {
						orderTabletLogger.info("Order Code 등록");
					}
				} catch(Exception e) {
					orderTabletLogger.severe("Order Code 등록 에러  ==>>  " + e.getMessage());
				}
			}
		} catch(Exception e) {
			orderTabletLogger.severe("Order Code 조회 에러  ==>>  " + e.getMessage());
		} finally {
			orderTablet.setOrderCode(orderTabletService.selectOrderCodeSession(orderTablet).getOrderCode());

			model.addAttribute("tableNo", selectDeviceInfo.getTableNo());
			model.addAttribute("companyName", selectDeviceInfo.getCompanyName());
		}
		return "order/tablet/main";
	}


	@GetMapping("/category")
	public String categoryList(OrderTablet orderTablet, Model model) {
		model.addAttribute("menuCategory", orderTabletService.selectCategory(orderTablet));
		return "order/tablet/ajax/category-list";
	}


	@GetMapping("/menu")
	public String ajaxMenuList(OrderTablet orderTablet, Model model, HttpSession session) { // 메뉴 조회 리스트는 여기서 가공해서 보내준다.
		if("undefined".equals(orderTablet.getCategoryCode())) {
			orderTablet.setCategoryCode("all");
		}

		try {
			model.addAttribute("menu", orderTabletService.selectMenu(orderTablet));
			model.addAttribute("menuImg", imgViewPath + orderTablet.getCompanyCode());
		} catch(Exception e) {
			e.printStackTrace();
		}
		return "order/tablet/ajax/menu-list";
	}


	@GetMapping("/menu/detail")
	public String ajaxOrderMenuDetail(OrderTablet orderTablet, Model model, HttpSession session) {
		model.addAttribute("menu", orderTablet); // 메뉴 정보
		model.addAttribute("menuImg", imgViewPath + orderTablet.getCompanyCode()); // 메뉴 이미지 경로
		model.addAttribute("isCartMenu", orderTabletService.selectDuplicateCartMenu(orderTablet) != null ? true : false);

		return "order/tablet/ajax/modal-menu";
	}


	@GetMapping("/history")
	public String getOrderHistory(HttpServletRequest request, OrderTablet orderTablet, HttpSession session, Model model) {
		List<OrderTablet> selectOrderHistory = orderTabletService.selectOrderHistory(orderTablet);

		model.addAttribute("orderHistory", selectOrderHistory);

		return "order/tablet/ajax/order-history";
	}


	@GetMapping("/call/staff")
	public String callStaff(HttpServletRequest request, OrderTablet orderTablet, HttpSession session, Model model) {
		orderTablet.setDeviceId(session.getAttribute("deviceId").toString());
		orderTablet.setCompanyCode(orderTabletService.selectDeviceInfo(orderTablet).getCompanyCode());

		model.addAttribute("needsItem", orderTabletService.selectNeedsItem(orderTablet));

		return "order/tablet/ajax/call-staff";
	}


	@PostMapping("/call/staff")
	public @ResponseBody HashMap<String, Object> callStaffConfirm(HttpServletRequest request, OrderTablet orderTablet, HttpSession session) {
		HashMap<String, Object> responseData = new HashMap<String, Object>();
		String resultMsg = null; // 결과 값 호출 메세지
		int resultCode = 0; // 호출 서비스 성공 유무

		String[] itemCode = orderTablet.getItemCode().split(","); // 품목 코드
		String[] itemQty = orderTablet.getItemQty().split(",");

		orderTablet.setDeviceId(session.getAttribute("deviceId").toString());
		orderTablet.setCompanyCode(orderTabletService.selectDeviceInfo(orderTablet).getCompanyCode());
		for(int i = 0; i < orderTablet.getSize(); i++) {
			orderTablet.setItemCode(itemCode[i]);
			orderTablet.setItemQty(itemQty[i]);

			try {
				int insertCallStaff = orderTabletService.insertCallStaff(orderTablet);

				if(insertCallStaff != 0) {
					resultCode = 1;
				} else {
					resultCode = 0;
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		responseData.put("resultCode", resultCode);
		return responseData;
	}


	@PostMapping("/payment/complete")
	public @ResponseBody CompletableFuture<HashMap<String, Object>> orderPayment(HttpServletRequest request) {
		HashMap<String, Object> responseData = new HashMap<String, Object>();

		String apiKey = "7LgzqeS3weTNqjEoV4GUm7M6TuQoUJEwCbPnh2LMcwhpifAPW04sxUhVCTHNdHRv98fRyKBJu07XnacC";
		String paymentId = request.getParameter("paymentId"); // 결제 ID
		String tmp = "";

		// WebClient 호출 후 CompletableFuture 반환
		return WebClient.builder().baseUrl("https://api.portone.io").build().get().uri(builder -> builder.path("/payments/" + paymentId).build()).header("Authorization", "PortOne" + " " + apiKey).retrieve().bodyToMono(String.class).map(responseBody -> {
			try {
				ObjectMapper mapper = new ObjectMapper();
				JsonNode payStatus = mapper.readTree(responseBody).get("status");
				if("PAID".equals(payStatus.asText())) { // 결제 완료 시
					orderTabletLogger.info("##  결제 완료  ##");
					responseData.put("payStatus", "PAID");
				} else {
					orderTabletLogger.warning("##  결제 실패  ##");
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
			return responseData;
		}).toFuture();
	}


	/* 미사용 */
	@GetMapping("/duplicate/cart")
	public @ResponseBody int duplicateCart(HttpServletRequest request, OrderTablet orderTablet, Model model) {
		OrderTablet selectDuplicateCartMenu = orderTabletService.selectDuplicateCartMenu(orderTablet);

		int resultCode = 0;
		if(selectDuplicateCartMenu != null) {
			resultCode = 1;
		}
		return resultCode;
	}


	@GetMapping("/menu-total-price")
	public @ResponseBody HashMap<String, Object> getMenuTotalPrice(HttpServletRequest request, OrderTablet orderTablet, HttpSession session) {
		orderTablet.setDeviceId(session.getAttribute("deviceId").toString());
		orderTablet.setOrderCode(orderTabletService.selectDeviceInfo(orderTablet).getOrderCode());

		HashMap<String, Object> responseData = new HashMap<String, Object>();

		OrderTablet selectNotCalMenuTotalPrice = orderTabletService.selectNotCalMenuTotalPrice(orderTablet);

		String orderStatus = selectNotCalMenuTotalPrice.getOrderStatus();

		if(orderStatus != "3") {
			responseData.put("resultCode", "error");
			responseData.put("resultMsg", "아직 제공되지 않은 메뉴가 존재합니다.");
		} else {
			responseData.put("menuTotalPrice", selectNotCalMenuTotalPrice.getMenuTotalPrice());
		}

		return responseData;
	}


	private List<OrderTablet> findCart(OrderTablet orderTablet) {
		List<OrderTablet> data = null;

		try {
			data = orderTabletService.selectCartMenu(orderTablet);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return data;
	}


	@GetMapping("/cart")
	public String orderTabletCart(OrderTablet orderTablet, Model model, HttpSession session) {
		int menuTotalPrice = orderTabletService.selectCartMenu(orderTablet).stream().mapToInt(menuPrice -> Integer.parseInt(menuPrice.getMenuPrice())).sum();

		model.addAttribute("cartMenu", orderTabletService.selectCartMenu(orderTablet));
		model.addAttribute("menuTotalPrice", menuTotalPrice);

		return "order/tablet/ajax/order-cart";
	}


	@PostMapping("/cart")
	public @ResponseBody Map<String, Object> addCartMenu(OrderTablet orderTablet, HttpServletRequest request) {
		HashMap<String, Object> data = new HashMap<String, Object>();

		OrderTablet findDuplicateCartMenu = orderTabletService.selectDuplicateCartMenu(orderTablet);

		if(findDuplicateCartMenu == null) { // 선택한 품목이 카트에 없는 경우
			try { // 카트에 새로운 메뉴 정보 등록
				OrderTablet selectMenuConfirm = orderTabletService.selectMenuConfirm(orderTablet);
				if(selectMenuConfirm != null) { // 데이터 검증
					int insertCartMenu = orderTabletService.insertCartMenu(orderTablet); // 쿼리에서 추가 검증한다.

					if(insertCartMenu != 0) {
						data.put("resultMsg", "장바구니에 메뉴를 담았어요!");
					}
				} else {
					data.put("resultMsg", "메뉴를 담지 못했어요..\r\n직원에게 문의하여 주세요.");
				}
			} catch(Exception e) {
				orderTabletLogger.warning("카트 업데이트 에러 발생  ==>>  " + e.getMessage());
			}
		}
		return data;
	}


	@PutMapping("/cart")
	public @ResponseBody Map<String, Object> updateCartMenu(OrderTablet orderTablet) {
		HashMap<String, Object> data = new HashMap<String, Object>();


		OrderTablet selectDuplicateCartMenu = orderTabletService.selectDuplicateCartMenu(orderTablet);
		if(selectDuplicateCartMenu != null) {
			try { // 수량과 기타 정보를 업데이트
				OrderTablet selectMenuConfirm = orderTabletService.selectMenuConfirm(orderTablet);
				if(selectMenuConfirm != null) { // 데이터 검증
					int updateCartMenu = orderTabletService.updateCartMenu(orderTablet);
					if(updateCartMenu != 0) {
						data.put("resultMsg", "메뉴를 추가로 담았어요!");
					}
				} else {
					data.put("resultMsg", "알 수 없는 에러로 메뉴를 담지 못했어요!\r\n직원에게 문의하여 주세요..");
				}

			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return data;
	}


	@DeleteMapping("/cart")
	public String deleteCartMenu(OrderTablet orderTablet, Model model) {
		String[] tmpMenuCode = orderTablet.getMenuCode().split(",");


		orderTablet.setMenuCodes(Arrays.stream(orderTablet.getMenuCode().split(",")).map(String::trim).toArray(String[]::new));


		for(int i = 0; i < tmpMenuCode.length; i++) {
			orderTablet.setMenuCode(tmpMenuCode[i]);
			try {
				orderTabletService.deleteCartMenu(orderTablet);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return "order/tablet/ajax/order-cart";
	}


	@PostMapping("/ordering")
	public @ResponseBody HashMap<String, Object> doOrderCartInMenu(OrderTablet orderTablet, HttpServletRequest request, HttpSession session) {

		HashMap<String, Object> responseData = new HashMap<String, Object>();

		boolean isResult = false; // 주문 로직 결과

		int orderSize = Integer.parseInt(orderTablet.getOrderSize());
		if(orderSize == 0) {
			responseData.put("resultMsg", "주문은 최소 한 개 이상입니다.\r\n장바구니 메뉴를 확인하여 주세요.");

			return responseData;
		} else {
			isResult = true;
		}

		if(isResult) {
			orderTablet.setCompanyCode(orderTabletService.selectDeviceInfo(orderTablet).getCompanyCode());
			orderTablet.setOrderCode(orderTabletService.selectDeviceInfo(orderTablet).getOrderCode());

			OrderTablet selectBeforeOrderCode = orderTabletService.selectBeforeOrderCode(orderTablet);

			if(selectBeforeOrderCode != null) { // 기존 주문한 오더코드가 있을 경우
				int beforeMenuTotalPrice = Integer.parseInt(selectBeforeOrderCode.getMenuTotalPrice()); // 이미 오더코드의 총 금액
				int afterMenuTotalPrice = beforeMenuTotalPrice + Integer.parseInt(orderTablet.getMenuTotalPrice()); // 이미 주문한 오더코드의 총 금액 + 현재 주문한 오더코드의 총 금액

				orderTablet.setMenuTotalPrice(Integer.toString(afterMenuTotalPrice)); // 최종 금액 DTO 셋팅

				try {
					int updateOrdering = orderTabletService.updateOrdering(orderTablet); // ORDERING 업데이트 처리

					if(updateOrdering != 0) {
						isResult = true;
					} else if(updateOrdering <= 0) {
						orderTabletLogger.warning("기존 주문 건 업데이트 처리 과정중 에러 발생");
					}
				} catch(Exception e) {
					orderTabletLogger.severe("주문 업데이트 처리 에러");
					e.printStackTrace();
					isResult = false;
				}

			} else { // 신규 주문의 경우
				try {
					int insertOrdering = orderTabletService.insertOrdering(orderTablet); // insert 처리한다.

					if(insertOrdering != 0) {
						isResult = true;
					} else if(insertOrdering <= 0) {
						orderTabletLogger.warning("신규 주문 건 업데이트 처리 과정중 에러 발생");
					}
				} catch(Exception e) {
					orderTabletLogger.severe("신규 주문 인서트 에러");
					e.printStackTrace();
					isResult = false;
				}

			}
		}

		if(isResult) {
			String[] menuCode = orderTablet.getMenuCode().split(",");
			String[] menuQuantity = orderTablet.getMenuQuantity().split(",");
			String[] menuPrice = orderTablet.getMenuPrice().split(",");

			for(int i = 0; i < orderSize; i++) {
				orderTablet.setMenuCode(menuCode[i]);
				orderTablet.setMenuQuantity(menuQuantity[i]);
				orderTablet.setMenuPrice(menuPrice[i]);

				OrderTablet selectBeforeOrderDtl = orderTabletService.selectBeforeOrderDtl(orderTablet);
				if(selectBeforeOrderDtl != null) { // 이미 주문한 메뉴코드가 존재하는 경우 업데이트 처리 로직

					int afterMenuQuantity = Integer.parseInt(orderTablet.getMenuQuantity()) + Integer.parseInt(selectBeforeOrderDtl.getMenuQuantity());
					int afterMenuPrice = Integer.parseInt(orderTablet.getMenuPrice()) + Integer.parseInt(selectBeforeOrderDtl.getMenuPrice());

					orderTablet.setMenuQuantity(Integer.toString(afterMenuQuantity));
					orderTablet.setMenuPrice(Integer.toString(afterMenuPrice));

					try { // 주문상세 업데이트
						int updateOrderingDtl = orderTabletService.updateOrderingDtl(orderTablet); // 업데이트 처리

						isResult = updateOrderingDtl != 0;
					} catch(Exception e) {
						orderTabletLogger.severe("주문 상세 업데이트 에러");
						e.printStackTrace();
						isResult = false;
					}

				} else {
					try { // 주문상세 인서트
						int insertOrderingDtl = orderTabletService.insertOrderingDtl(orderTablet);

						isResult = insertOrderingDtl != 0;
					} catch(Exception e) {
						orderTabletLogger.severe("주문 상세 인서트 에러");
						e.printStackTrace();
						isResult = false;
					}
				}

				try { // 카트 주문상태 업데이트
					int updateCartMenuStatus = orderTabletService.updateCartMenuStatus(orderTablet);

					if(updateCartMenuStatus != 0) {
						isResult = true;
					} else if(updateCartMenuStatus <= 0) {
						orderTabletLogger.warning("카트 정보 STATUS(2) 업데이트 처리 과정중 에러 발생");
					}
				} catch(Exception e) {
					orderTabletLogger.severe("카트 주문상태 업데이트 에러");
					isResult = false;
				}
			}
		}

		if(isResult) {
			responseData.put("resultMsg", "주문 완료되었습니다.\r\n맛있게 조리 후 가져다드릴게요!");
			responseData.put("resultCode", "04"); // 주문 성공 코드
		} else {
			responseData.put("resultMsg", "주문에 실패하였습니다.\r\n매장 직원에게 문의하여 주세요.");
		}

		return responseData;
	}


	@GetMapping("/register")
	public String registerDeviceInfo(Model model, HttpSession session, HttpServletRequest request) {
		model.addAttribute("deviceId", (request.getHeader("DEVICE_ID") != null) ? request.getHeader("DEVICE_ID") : "TEST");

		return "order/tablet/device-register";
	}


	@PutMapping("/register")
	public @ResponseBody HashMap<String, Object> updateDeviceInfo(OrderTablet orderTablet, HttpServletResponse response, HttpSession session) {
		HashMap<String, Object> responseData = new HashMap<>();

		int updateDeviceInfo = orderTabletService.updateDeviceInfo(orderTablet);
		if(updateDeviceInfo != 0) {
			responseData.put("resultCode", 1);

			orderTabletLogger.info("신규 태블릿 등록 완료");
		}
		return responseData;
	}


}
