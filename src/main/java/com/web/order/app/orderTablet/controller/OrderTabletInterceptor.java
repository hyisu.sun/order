package com.web.order.app.orderTablet.controller;


import com.web.order.app.orderTablet.model.service.OrderTabletService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.HandlerInterceptor;

import java.util.logging.Logger;


public class OrderTabletInterceptor implements HandlerInterceptor {
	Logger logger = Logger.getLogger("DeviceInterceptor");

	@Autowired
	private OrderTabletService orderTabletService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {


		if (request.getRequestURI().equals("/")) {
			response.sendRedirect("/order/tablet");
		}


		if (request.getRequestURI().startsWith("/order/tablet")) {
			HttpSession session = request.getSession();


			if (session.getAttribute("deviceId") != null) { // 디바이스 첫 등록 후 메인 접근 이슈 방지
				return true;
			}


			if (session.getAttribute("deviceId") == null) {
				session.setAttribute("deviceId", request.getHeader("DEVICE_ID"));
				if (request.getHeader("DEVICE_ID") == null) {
					session.setAttribute("deviceId", "TEST");
				}
				return true;
			}
		}
		return HandlerInterceptor.super.preHandle(request, response, handler);
	}
}
