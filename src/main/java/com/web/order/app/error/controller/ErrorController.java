package com.web.order.app.error.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class ErrorController {


	@GetMapping({"", "/"})
	public String errorPage() {
		return "error/main";
	}
}
