package com.web.order.app.orderManagement.controller;


import com.web.order.app.orderManagement.model.data.OrderManagement;
import com.web.order.app.orderManagement.model.service.OrderManagementService;
import com.web.order.app.orderTablet.model.data.OrderTablet;
import com.web.order.app.orderTablet.model.service.OrderTabletService;
import com.web.order.util.FtpUtil;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;


/**
 * @author 선희수
 * @apiNote 주문 관리 컨트롤러
 */
@Controller
@RequestMapping("/order/management")
public class OrderManagementController {


	Logger logger = Logger.getLogger("OrdermanagementController");

	@Autowired
	private OrderManagementService orderManagementService;

	@Autowired
	private FtpUtil ftpUtil;

	@Autowired
	private OrderTabletService orderTabletService;

	@Value("${menu.img.view.path}")
	private String imgViewPath;

	@Value("${ftp.host}")
	private String ftpHost;

	@Value("${ftp.port}")
	private int ftpPort;

	@Value("${ftp.username}")
	private String ftpUsername;

	@Value("${ftp.password}")
	private String ftpPassword;


	/**
	 * @author 선희수
	 * @apiNote 관리자 페이지 쿠키 값 셋팅
	 */
	private HashMap<String, Object> initData(HttpServletRequest request) {
		HashMap<String, Object> initData = new HashMap<String, Object>();

		for(Cookie data : request.getCookies()) {
			if("isLogin".equals(data.getName())) {
				initData.put("isLogin", data.getValue());
			}

			if("companyCode".equals(data.getName())) {
				initData.put("companyCode", data.getValue());
			}

			if("companyName".equals(data.getName())) {
				initData.put("companyName", data.getValue());
			}

			if("viewMode".equals(data.getName())) {
				initData.put("viewMode", data.getValue());
			}
		}
		return initData;
	}


	@ModelAttribute
	public void saleDate(Model model, OrderManagement orderManagement, HttpSession session, HttpServletRequest request) {
		if(!"/order/management/login.do".equals(request.getRequestURI())) {
			orderManagement.setCompanyCode(session.getAttribute("companyCode").toString());

			String beforeSaleDate = orderManagementService.selectSaleDate(orderManagement).getSaleDate();
			String saleDate = beforeSaleDate.substring(0, 4) + "/" + beforeSaleDate.substring(4, 6) + "/" + beforeSaleDate.substring(6, 8);
			model.addAttribute("saleDate", saleDate); // 영업 일자

			LocalDate currentDate = LocalDate.now();
			model.addAttribute("currentDate", currentDate.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"))); // 오늘 날짜

			model.addAttribute("companyCode", session.getAttribute("companyCode"));
			model.addAttribute("companyName", session.getAttribute("companyName"));
		}
	}


	@GetMapping({"index"})
	public String index() {
		return "order/management/index";
	}


	@GetMapping("/counter")
	public String ordermanagementCounter(Model model, HttpServletResponse response, @CookieValue(required = false, defaultValue = "l") String viewMode) {

		model.addAttribute("viewMode", viewMode);
		Cookie viewModeCookie = new Cookie("viewMode", viewMode);
		response.addCookie(viewModeCookie);

		return "order/management/counter";
	}


	@GetMapping("/counter/list")
	public String orderList(OrderManagement orderManagement, Model model, HttpServletResponse response, HttpSession session) {

		orderManagement.setCompanyCode(session.getAttribute("companyCode").toString());

		List<OrderManagement> orderList = orderManagementService.selectOrderList(orderManagement);

		model.addAttribute("viewMode", orderManagement.getViewMode());
		model.addAttribute("orderList", orderList);

		return "order/management/ajax/order-list";
	}


	/**
	 * @apiNote 주문 진행상태로 변경
	 */
	@PutMapping("/counter/list")
	public String orderStatusChg(HttpServletRequest request, OrderManagement orderManagement, OrderTablet orderTablet, Model model, HttpSession session) {
		orderManagement.setOrderCode(request.getParameter("orderCode"));
		orderManagement.setCompanyCode(session.getAttribute("companyCode").toString());

		try {
			int updateOrderStatus1 = orderManagementService.updateOrderStatus(orderManagement);

			if(updateOrderStatus1 != 0) {
				//				orderManagement.setCompanyCode(initData(request).get("companyCode").toString());
				orderManagement.setViewMode(initData(request).get("viewMode").toString());
				model.addAttribute("orderList", orderManagementService.selectOrderList(orderManagement));
			}
		} catch(Exception e) {
			e.printStackTrace();
		}

		return "order/management/ajax/order-list";
	}


	/**
	 * 주문 상세현황 조회
	 */
	@GetMapping("/order/list/detail")
	public String getOrderListDetail(OrderManagement condition, Model model) {
		List<OrderManagement> orderListDtl = orderManagementService.selectOrderListDtl(condition);

		model.addAttribute("tableNo", condition.getTableNo());
		model.addAttribute("orderCode", condition.getOrderCode());
		model.addAttribute("orderStatus", condition.getOrderStatus());
		model.addAttribute("orderListDtl", orderListDtl);

		return "order/management/ajax/order-list-dtl";
	}


	@GetMapping("/login.do")
	public String ordermanagementLogin() {
		return "order/management/login";
	}


	@PostMapping("/login.do")
	public String doLogin(OrderManagement condition, HttpSession session, HttpServletResponse response, Model model, RedirectAttributes redirectAttributes) {
		OrderManagement loginAccount = orderManagementService.selectAccount(condition);

		if(loginAccount != null) { // 로그인 정보가 존재할 경우
			session.setAttribute("isLogin", loginAccount);
			session.setAttribute("companyCode", loginAccount.getCompanyCode());

			return "redirect:/order/management/index";
		} else { // 로그인 정보가 존재하지 않을 경우 로그인 페이지로 이동
			redirectAttributes.addFlashAttribute("loginMessage", "로그인 정보가 부적절합니다.");
			return "redirect:/order/management/login.do";
		}
	}


	@GetMapping("/menu")
	public String menuManagement(OrderManagement condition, HttpServletRequest request, Model model, HttpSession session) {
		condition.setCompanyCode(session.getAttribute("companyCode").toString());

		if(condition.getCategoryCode() == null) {  // 카테고리 코드가 기본 값의 경우
			condition.setCategoryCode("ALL");
		}

		if(request.getParameter("limit") == null) {
			condition.setLimit(20);
		}


		try {
			RowBounds rowBounds = new RowBounds(condition.getOffset(), condition.getLimit());
			List<OrderManagement> selectMenuList = orderManagementService.selectMenuList(condition, rowBounds);
			if(selectMenuList != null) {
				model.addAttribute("selectShowYn", condition.getShowYn());
				model.addAttribute("selectCategoryCode", condition.getCategoryCode());
				model.addAttribute("menuName", condition.getMenuName());
				model.addAttribute("selectMenuList", selectMenuList);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}


		try {
			int pageSize = 0;

			OrderManagement selectMenuListCount = orderManagementService.selectMenuListCount(condition);
			if(selectMenuListCount != null) {
				int tempPageSize = selectMenuListCount.getRowCount() % condition.getLimit();

				if(tempPageSize != 0) {
					pageSize = selectMenuListCount.getRowCount() / condition.getLimit() + 1;
				} else {
					pageSize = selectMenuListCount.getRowCount() / condition.getLimit();
				}
				model.addAttribute("pageSize", pageSize);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}


		try {
			// 조회 조건 값 선택을 위한 카테고리 종류 리스트 반환
			List<OrderManagement> categoryList = orderManagementService.selectCategory(condition);
			if(categoryList != null) {
				model.addAttribute("categoryList", categoryList);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return "order/management/menu";
	}


	@GetMapping("/menu/list/detail")
	public String menuDetail(OrderManagement orderManagement, HttpSession session, Model model, HttpServletRequest request) {
		orderManagement.setCompanyCode(session.getAttribute("companyCode").toString());

		OrderManagement menuDetail = orderManagementService.selectMenuDetail(orderManagement);
		List<OrderManagement> categoryList = orderManagementService.selectCategory(orderManagement);

		String type = request.getParameter("type");

		// 정보 수정은 'C', 신규 등록은 'N'
		if("C".equals(type)) {
			model.addAttribute("menuDetail", menuDetail);
			model.addAttribute("menuImg", imgViewPath + "00" + "/" + menuDetail.getImgCode());

			// try {
			// 	FTPClient ftpClient = fileUtil.ftpConnect();

			// 	if (ftpClient.isConnected()) {
			// 		FTPFile[] ftpFiles = ftpClient.listFiles(companyCode);
			// 		for (FTPFile ftpFile : ftpFiles) {
			// 			if (menuDetail.getImgCode().equals(ftpFile.getName())) {

			// 				InputStream inputStream = ftpClient.retrieveFileStream(companyCode + "/" + ftpFile.getName());

			// 				model.addAttribute("menuImg", "data:image/" + menuDetail.getImgExtension() + ";base64," + fileUtil.convertBase64(inputStream));
			// 			}
			// 		}
			// 	}
			// } catch (Exception e) {
			// 	e.printStackTrace();
			// } finally {
			// 	fileUtil.ftpDisconnect();
			// }
		} else if("N".equals(type)) {
			// 메뉴 신규 생성의 경우 추가적으로 전송하고 싶은 데이터를 넣어준다.
		}
		model.addAttribute("categoryList", categoryList);

		return "order/management/ajax/menu-list-dtl";
	}


	/**
	 * @author 선희수
	 * @apiNote 관리자 웹 메뉴 상세정보 추가(put method 방식으로 이미지 저장이 불가능하기에 해당 http method에서 분기 처리한다.)
	 */
	@PostMapping("/menu/list/detail")
	public @ResponseBody HashMap<String, Object> createMenuDetail(OrderManagement orderManagement, HttpServletRequest request) {
		HashMap<String, Object> responseData = new HashMap<String, Object>();

		//		String companyCode = initData(request).get("companyCode").toString();
		//		condition.setCompanyCode(session.getAttribute("companyCode").toString());

		String nullChk = isInputChk("changeMenuDetail", orderManagement);
		if(nullChk != null) { // 입력 값 유효성 검증결과 null 값이 있을경우
			responseData.put("resultMsg", nullChk);
			responseData.put("resultCode", "02");

			return responseData;
		}


		boolean result = false; // 결과 값

		if(orderManagement.getMenuImg() != null) { // 이미지 파일을 첨부한 경우
			String fileExtension = FilenameUtils.getExtension(orderManagement.getMenuImg().getOriginalFilename());

			orderManagement.setImgExtension(fileExtension);
			orderManagement.setImgCode(UUID.randomUUID().toString() + "." + fileExtension);
			orderManagement.setImgName(orderManagement.getMenuImg().getOriginalFilename());

			try {
				FTPClient ftpClient = ftpUtil.ftpConnect();

				ftpClient.connect(ftpHost, ftpPort);
				ftpClient.login(ftpUsername, ftpPassword);
				ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

				if(ftpClient.isConnected()) {
					try(InputStream inputStream = orderManagement.getMenuImg().getInputStream()) {
						//						boolean isUpload = ftpClient.storeFile("/data/media/img/" + "00" + "/" + orderManagement.getImgCode(), inputStream);
						boolean isUpload = ftpClient.storeFile("/mnt/data/order/img/" + "00" + "/" + orderManagement.getImgCode(), inputStream);

						if(isUpload) {
							result = true;
						} else {
							String replyMessage = ftpClient.getReplyString();
							logger.warning("이미지 업로드 실패\r\n" + replyMessage);
						}
					} catch(Exception e) {
						e.printStackTrace();
					} finally {
						ftpUtil.ftpDisconnect();
					}
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}

		try {
			if(orderManagement.getMenuExplain().length() == 2000) { // 이미지 설명 2,000자 제한
				return responseData;
			}

			if("post".equals(orderManagement.getType())) {

				orderManagement.setMenuCode(UUID.randomUUID().toString());
			}

			int resultMenuInfo = 0;

			if("post".equals(orderManagement.getType())) {
				resultMenuInfo = orderManagementService.insertMenuInfo(orderManagement); // 신규 메뉴 생성 서비스 로직 실행
			} else if("put".equals(orderManagement.getType())) {
				resultMenuInfo = orderManagementService.updateMenuInfo(orderManagement); // 기존 메뉴 업데이트 서비스 로직 실행
			}
			if(resultMenuInfo != 0) {
				result = true;
			} else {
				if("post".equals(orderManagement.getType())) {
					responseData.put("resultMsg", "메뉴 상세 정보 신규 등록에 실패하였습니다.\r\n고객센터로 문의하여 주세요.");
				} else if("put".equals(orderManagement.getType())) {
					responseData.put("resultMsg", "메뉴 상세 정보 업데이트에 실패하였습니다.\r\n고객센터로 문의하여 주세요.");
				}

				return responseData;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}

		if(result) {
			if("post".equals(orderManagement.getType())) {
				responseData.put("resultMsg", "메뉴 상세 정보가 신규 등록되었습니다.");
			} else if("put".equals(orderManagement.getType())) {
				responseData.put("resultMsg", "메뉴 상세 정보가 업데이트 되었습니다.");
			}
			responseData.put("resultCode", "01");
		} else if(!result) {
			responseData.put("resultMsg", "알 수 없는 에러가 발생하였습니다.\r\n고객센터로 문의하여 주세요.");
		}
		return responseData;
	}


	/**
	 * @author 선희수
	 * @apiNote 관리자 웹 메뉴 상세정보 삭제
	 */
	@DeleteMapping("/menu/list/detail")
	public @ResponseBody HashMap<String, Object> deleteMenuDetail(OrderManagement orderManagement, HttpServletRequest request) {
		HashMap<String, Object> responseData = new HashMap<String, Object>();

		//		String companyCode = initData(request).get("companyCode").toString();
		//		condition.setCompanyCode(session.getAttribute("companyCode").toString());

		boolean result = false;

		try {
			int resultMenuInfo = 0;

			resultMenuInfo = orderManagementService.deleteMenuInfo(orderManagement); // 기존 메뉴 업데이트 서비스 로직 실행
			if(resultMenuInfo != 0) {
				result = true;
			} else {
				responseData.put("resultMsg", "메뉴 삭제처리 과정에서 에러가 발생하였습니다.\r\n고객센터로 문의하여 주세요.");
				result = false;
				return responseData;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}

		if(result) {
			responseData.put("resultMsg", "메뉴 정보가 삭제되었습니다.");
			responseData.put("resultCode", "01");
		} else if(!result) {
			responseData.put("resultMsg", "알 수 없는 에러가 발생하였습니다.\r\n고객센터로 문의하여 주세요.");
		}
		return responseData;
	}


	/**
	 * @since: 2024/03/16
	 * @return: 테이블 리스트 데이터와 페이지 동시 반환
	 */
	@GetMapping("/table")
	public String ordermanagementTable(HttpServletRequest request, OrderManagement condition, Model model, HttpSession session) {
		//		condition.setCompanyCode(initData(request).get("companyCode").toString());

		List<OrderManagement> selectmanagementTableList = orderManagementService.selectmanagementTableList(condition);

		model.addAttribute("tableList", selectmanagementTableList);

		return "order/management/table";
	}


	/**
	 * @since 테이블 변경 로직
	 */
	@PutMapping("/table")
	@ResponseBody
	public String changeTable(OrderManagement orderManagement) {

		int updateTableNo = orderManagementService.updateTableNo(orderManagement);
		String result = null; // 결과 구분

		try {
			if(updateTableNo != 0) {
				result = "테이블 정보가 변경되었습니다.";
			} else {
				result = "업데이트 이력이 없습니다.";
			}
		} catch(Exception e) {
			result = "알 수 없는 에러가 발생하였습니다.";
		}

		return result;
	}


	/**
	 * @return 결과 값 반환
	 *
	 * @author 선희수
	 * @apiNote 관리자 웹 테이블 초기화
	 * @since 2024/03/16
	 */
	@PostMapping("/table")
	public @ResponseBody int resetTableInfo(HttpServletRequest request, OrderManagement orderManagement, OrderTablet orderTablet, HttpSession session) {

		/**
		 * 1: 성공
		 * 2: 미제공 주문
		 * 3: 테이블 초기화 실패
		 * 99: 실패
		 */

		int resultCode;

		try {
			List<OrderManagement> selectOrderList = orderManagementService.selectOrderList(orderManagement);

			if(selectOrderList.size() == 0) {
				int updateResetTable = orderManagementService.updateResetTable(orderManagement);

				if(updateResetTable != 0) {
					resultCode = 1;
				} else { // 오더세션코드에 조회된 자료가 없을 시
					resultCode = 3;
				}
			} else {
				resultCode = 2;
			}
		} catch(Exception e) {
			e.printStackTrace();
			return 99;
		}

		return resultCode;
	}


	@GetMapping("/table/info/detail")
	public String getTableListDtl(HttpServletRequest request, OrderManagement orderManagement, OrderTablet orderTablet, HttpSession session, Model model) {
		OrderManagement selectTableInfoDtl = orderManagementService.selectTableInfoDtl(orderManagement);

		if(selectTableInfoDtl == null) {
			model.addAttribute("deviceInfo", orderManagementService.selectDeviceInfo(orderManagement));
		}

		model.addAttribute("tableInfoDtl", selectTableInfoDtl);

		// orderTablet.setDeviceId(session.getAttribute("deviceId").toString());
		System.out.println(orderTablet.getDeviceId());
		String orderCode = orderTabletService.selectDeviceInfo(orderTablet).getOrderCode();
		model.addAttribute("orderCode", orderCode);

		return "order/management/ajax/table-dtl";

	}


	/**
	 * @since 2024/03/10 직원 호출 팝업 오픈
	 */
	@GetMapping("/call/staff")
	public String callStaff(HttpSession session, Model model, OrderManagement condition) {
		condition.setCompanyCode(session.getAttribute("companyCode").toString());
		model.addAttribute("tableNo", condition.getTableNo());
		model.addAttribute("callStaffInfo", orderManagementService.selectCallStaffInfo(condition));
		return "order/management/ajax/call-staff";
	}


	@GetMapping("/category")
	public String ordermanagementCategory(HttpSession session, Model model, OrderManagement condition) {
		return "order/management/category";
	}


	@GetMapping("/category/list")
	public String getCategoryList(HttpServletRequest request, OrderManagement condition, Model model, HttpSession session) {
		//		condition.setCompanyCode(initData(request).get("companyCode").toString());
		model.addAttribute("category", orderManagementService.selectCategory(condition));
		return "order/management/ajax/category-list";
	}


	@GetMapping("/category/list/detail")
	private String getCategoryListDtl(HttpServletRequest request, OrderManagement condition, Model model, HttpSession session) {
		model.addAttribute("categoryDtl", orderManagementService.selectCategoryDtl(condition));

		return "order/management/ajax/category-list-dtl";
	}


	/**
	 * 카테고리 정보 변경
	 */
	@PutMapping("/category/list/detail")
	public @ResponseBody HashMap<String, Object> updateCategoryInfo(HttpServletRequest request, HttpSession session, OrderManagement condition) {
		HashMap<String, Object> responseData = new HashMap<String, Object>();

		String nullChk = isInputChk("updateCategoryInfo", condition);

		if(nullChk != null) {
			responseData.put("resultCode", 02);
			responseData.put("resultMsg", nullChk);

			return responseData;
		}

		int updateCategoryInfo = 0;
		updateCategoryInfo = orderManagementService.updateCategoryInfo(condition);
		if(updateCategoryInfo != 0) {
			responseData.put("resultCode", 01);
			responseData.put("resultMsg", "카테고리 정보 변경이 완료되었습니다.");
		} else {
			responseData.put("resultCode", 02);
			responseData.put("resultMsg", "카테고리 정보 변경 내역이 존재하지 않습니다.");
		}
		return responseData;
	}


	/**
	 * 카테고리 정보 등록
	 */
	@PostMapping("/category/list/detail")
	public @ResponseBody HashMap<String, Object> addCategoryInfo(HttpServletRequest request, HttpSession session, OrderManagement condition) {
		HashMap<String, Object> responseData = new HashMap<String, Object>();

		String nullChk = isInputChk("updateCategoryInfo", condition);

		if(nullChk != null) {
			responseData.put("resultCode", 02);
			responseData.put("resultMsg", nullChk);

			return responseData;
		}

		int updateCategoryInfo = 0;
		updateCategoryInfo = orderManagementService.insertCategoryInfo(condition);
		if(updateCategoryInfo != 0) {
			responseData.put("resultCode", 01);
			responseData.put("resultMsg", "카테고리 등록이 완료되었습니다.");
		} else {
			responseData.put("resultCode", 02);
			responseData.put("resultMsg", "카테고리 등록 내역이 존재하지 않습니다.");
		}
		return responseData;
	}


	/**
	 * 카테고리 정보 삭제
	 */
	@DeleteMapping("/category/list/detail")
	public @ResponseBody HashMap<String, Object> deleteCategoryInfo(OrderManagement condition) {
		OrderManagement menuYn = orderManagementService.selectMenuInCategory(condition);

		HashMap<String, Object> responseData = new HashMap<String, Object>();

		if(menuYn.getMenuHaveCnt() == 0) {
			int deleteCategoryInfo = orderManagementService.deleteCategoryInfo(condition);

			if(deleteCategoryInfo > 0) { // 정상 1 리턴
				responseData.put("resultCode", 1);
				responseData.put("resultMsg", "정상적으로 삭제되었습니다.");
			} else if(deleteCategoryInfo <= 0) { // 업데이트 없을 시 2 리턴
				responseData.put("resultCode", 2);
				responseData.put("resultMsg", "삭제된 자료가 없습니다.");
			} else { // 알 수 없는 에러 발생 시 99 리턴
				responseData.put("resultCode", 99);
				responseData.put("resultMsg", "알 수 없는 에러가 발생하였습니다.");
			}
		} else if(menuYn.getMenuHaveCnt() >= 0) {
			responseData.put("resultCode", 3);
			responseData.put("resultMsg", "삭제하려는 카테고리에" + " " + menuYn.getMenuHaveCnt() + "개의 메뉴가 존재합니다." + "\r\n" + "해당 메뉴를 삭제 후 진행하시기 바랍니다.");
		}

		return responseData;
	}


	/**
	 * @author 선희수
	 * @apiNote 관리자 웹에서 정보 CRUD시 입력 여부 유효성 검증
	 * @since 2024/05/19
	 */
	private String isInputChk(String function, OrderManagement orderManagement) { // 리턴 값이 이상함.
		if("changeMenuDetail".equals(function)) {
			if(orderManagement.getMenuName().trim().isEmpty()) {
				return "메뉴 이름을 입력해주세요.";
			} else if(orderManagement.getCategoryCode().trim().isEmpty()) {
				return "카테고리를 선택해주세요.";
			} else if(Integer.toString(orderManagement.getMenuPrice()).trim().isEmpty()) {
				return "메뉴 금액을 입력해주세요.";
			} else if(orderManagement.getShowYn().trim().isEmpty()) {
				return "노출 여부를 선택해주세요.";
			} else if(orderManagement.getBestYn().trim().isEmpty()) {
				return "깃발 여부를 선택해주세요.";
			} else if(orderManagement.getMenuImg() == null) { // 이미지를 선택하지 않았을 경우에
				if("post".equals(orderManagement.getType())) { // 신규 등록의 경우에만
					return "이미지를 선택해주세요.";
				}
			}
		}

		if("updateCategoryInfo".equals(function)) {
			if(orderManagement.getOpenTime().trim().isEmpty()) {
				return "노출 시작 시간을 입력해주세요.";
			} else if(orderManagement.getCloseTime().trim().isEmpty()) {
				return "노출 종료 시간을 입력해주세요.";
			} else if(orderManagement.getUseYn().trim().isEmpty()) {
				return "노출 여부를 선택해주세요.";
			} else if(orderManagement.getCategoryName().trim().isEmpty()) {
				return "카테고리 이름을 입력해주세요.";
			}
		}
		return null; // 얼리 리턴이 없을 때 무조건 성공
	}


	/**
	 * @author 선희수
	 * @apiNote 주문 건 제공완료 처리
	 */
	@PutMapping("/order/offer/done")
	public @ResponseBody HashMap<String, Object> changeOrderComplete(HttpServletRequest request, OrderManagement condition, HttpSession session) {
		HashMap<String, Object> responseData = new HashMap<String, Object>();

		condition.setCompanyCode(session.getAttribute("companyCode").toString());

		int updateResult = orderManagementService.updateOrderComplete(condition);

		if(updateResult != 0) {
			responseData.put("result", 01); // 성공
		} else {
			responseData.put("result", 02); // 실패
		}

		return responseData;
	}


	@GetMapping("/sale")
	public String storeClose(OrderManagement orderManagement, Model model) {
		//		condition.setCompanyCode(session.getAttribute("companyCode").toString());

		List<OrderManagement> selectCloseData = orderManagementService.selectCloseData(orderManagement);

		if(selectCloseData.size() != 0) {
			int totalPrice = 0;
			int supplyPrice = 0;
			int taxPrice = 0;

			for(int i = 0; i < selectCloseData.size(); i++) {
				totalPrice += selectCloseData.get(i).getMenuTotalPrice();
			}

			supplyPrice = (int) Math.round(totalPrice / 1.1);
			taxPrice = totalPrice - supplyPrice;

			model.addAttribute("totalPrice", totalPrice);
			model.addAttribute("supplyPrice", supplyPrice);
			model.addAttribute("taxPrice", taxPrice);
			model.addAttribute("orderCnt", selectCloseData.size());
		}
		return "order/management/sale";
	}


	/**
	 * 영업마감을 위해 자료를 조회한다.
	 * 현재는 미제공 주문이 있는지만 확인한다.
	 * 영업마감 완료 시 해당 회사코드의 오더세션코드의 상태 값을 2로 변경하고, 영업일자를 D+1로 설정한다.
	 */
	@GetMapping("/close-data")
	public @ResponseBody HashMap<String, Object> selectCloseData(OrderManagement orderManagement) {
		HashMap<String, Object> responseData = new HashMap<>();

		try {
			List<OrderManagement> selectNotOfferForSaleClose = orderManagementService.selectNotOfferForSaleClose(orderManagement); // 미제공 메뉴 조회

			if(selectNotOfferForSaleClose.size() != 0) {
				responseData.put("resultCode", 99);
				responseData.put("resultMsg", "미제공 주문 건이 존재합니다.\r\n영업 마감을 위해서 제공을 완료하십시오.");
			} else {
				int updateSaleDate = orderManagementService.updateSaleDate(orderManagement);

				if(updateSaleDate != 0) {
					int updateOrderCodeSessionStatus = orderManagementService.updateOrderCodeSessionStatus(orderManagement);

					if(updateOrderCodeSessionStatus != 0) {
						responseData.put("resultCode", 01);
						responseData.put("resultMsg", "마감이 완료되었습니다.");
					}
				}

				// List<OrderManagement> selectNotResetTableForSaleClose = orderManagementService.selectNotResetTableForSaleClose(orderManagement);

				// if (selectNotResetTableForSaleClose.size() != 0) {
				// 	return "초기화되지 않은 테이블이 존재합니다.\r\n영업마감은 모든 테이블이 초기화되어야 합니다.";
				// } else {
				// 	return "마감이 완료되었습니다.";
				// }
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return responseData;
	}


	@GetMapping("/coupon")
	public String coupon(OrderManagement orderManagement, Model model) {

		model.addAttribute("couponList", orderManagementService.selectCouponList(orderManagement));
		return "order/management/coupon";
	}


	@GetMapping("/coupon/info")
	public String couponInfo(OrderManagement orderManagement, Model model, HttpSession session) {
		if(orderManagement.getCouponCode() != null) {
			orderManagement.setCompanyCode(session.getAttribute("companyCode").toString());

			model.addAttribute("couponDetailInfo", orderManagementService.selectCouponDetail(orderManagement));
		}
		return "order/management/coupon-info";
	}


	@PostMapping("/coupon/info")
	@ResponseBody
	public HashMap<String, Object> createCouponInfo(OrderManagement orderManagement, HttpSession session) {
		HashMap<String, Object> responseData = new HashMap<String, Object>();  // 결과 데이터


		if(orderManagement.getCouponName().length() == 0) {
			responseData.put("returnMsg", "쿠폰 이름을 입력하십시오.");
			return responseData;
		}

		if(orderManagement.getUseYn() == null) {
			responseData.put("returnMsg", "쿠폰 사용여부를 지정하십시오.");
			return responseData;
		}


		try {
			orderManagement.setCompanyCode(session.getAttribute("companyCode").toString());

			int insertCoupon = orderManagementService.insertCoupon(orderManagement);

			if(insertCoupon != 0) {
				responseData.put("returnMsg", "쿠폰을 생성하였습니다.");
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return responseData;
	}
}
