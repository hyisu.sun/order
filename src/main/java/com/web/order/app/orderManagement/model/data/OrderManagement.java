package com.web.order.app.orderManagement.model.data;


import com.web.order.app.common.model.data.Pagination;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


@Setter
@Getter
public class OrderManagement extends Pagination {
	private String tableCode;


	private String companyCode;
	private String companyName;

	private String orderCode;

	private String tableNo;
	private String menuExplain;

	private String menuCode;
	private String menuName;
	private String menuQuantity;
	private String menuSeq;
	private int menuHaveCnt;
	private int menuPrice;
	private int menuTotalPrice;
	private MultipartFile menuImg;
	private String useYn;

	private String isrtDt;
	private String updtDt;
	private String offerDt;

	private String orderStatus;

	private String deviceId;

	private String id;
	private String pw;

	private String categoryCode;
	private String categoryName;

	private String imgCode;
	private String imgExtension;
	private String imgName;

	private String showYn;
	private String bestYn;
	private String startHour;
	private String endHour;
	private String startMinute;
	private String openTime;
	private String closeTime;
	private String endMinute;
	private String saleDate; // 영업일자

	private String calculateStatus;

	private String itemCode;
	private String itemQty;
	private String itemName;

	private String mappingYn; // 테이블 매칭 여부 확인

	private String type;

	private String viewMode; // 뷰모드에 따라 주문현황 UI 변경

	private String includeCalculate;
	private String searchCondition;
	private List<OrderManagement> menuListData;
	private int orderCnt;
	private int supplyPrice; // 공급가
	private int taxPrice; // 부가세


	private String couponCode;  // 쿠폰 코드
	private String couponName;  // 쿠폰 이름
	private String couponComment;  // 쿠폰 설명
	private String couponExpire; // 쿠폰 유효기간
}
