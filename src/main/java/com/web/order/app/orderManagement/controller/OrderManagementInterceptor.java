package com.web.order.app.orderManagement.controller;


import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.web.servlet.HandlerInterceptor;


public class OrderManagementInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		HttpSession session = request.getSession();

		String previousUrl = request.getRequestURI();
		session.setAttribute("previousUrl", previousUrl);

		if(request.getRequestURI().startsWith("/order/management")) {
			if(session.getAttribute("isLogin") == null) {
				response.sendRedirect("/order/management/login.do");
				return false;
			}
		}
		return true;
	}
}
