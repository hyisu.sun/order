package com.web.order.app.orderManagement.model.service;


import com.web.order.app.orderManagement.model.data.OrderManagement;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;


@Service
public class OrderManagementService {


	private static final String QUERY_ID_PREFIX = "orderManagement";


	Logger logger = Logger.getLogger("OrdermanagementService");


	@Autowired
	private SqlSession sqlSession;


	public List<OrderManagement> selectOrderList(OrderManagement condition) {
		return sqlSession.selectList(QUERY_ID_PREFIX + ".selectOrderList", condition);
	}


	public int updateOrderStatus(OrderManagement condition) {
		return sqlSession.update(QUERY_ID_PREFIX + ".updateOrderStatus", condition);
	}


	public List<OrderManagement> selectOrderListDtl(OrderManagement condition) {
		return sqlSession.selectList(QUERY_ID_PREFIX + ".selectOrderListDtl", condition);
	}


	public List<OrderManagement> selectCategory(OrderManagement condition) {
		return sqlSession.selectList(QUERY_ID_PREFIX + ".selectCategory", condition);
	}


	public OrderManagement selectAccount(OrderManagement condition) {

		if(condition == null) {
			logger.info("계정 상태 null 반환");
		}

		return sqlSession.selectOne(QUERY_ID_PREFIX + ".selectAccount", condition);
	}


	public List<OrderManagement> selectMenuList(OrderManagement condition, RowBounds rowBounds) {
		return sqlSession.selectList(QUERY_ID_PREFIX + ".selectMenuList", condition, rowBounds);
	}


	public OrderManagement selectMenuListCount(OrderManagement condition) {
		return sqlSession.selectOne(QUERY_ID_PREFIX + ".selectMenuListCount", condition);
	}


	public OrderManagement selectMenuDetail(OrderManagement condition) {
		return sqlSession.selectOne(QUERY_ID_PREFIX + ".selectMenuDetail", condition);
	}


	public int insertMenuInfo(OrderManagement condition) {
		return sqlSession.insert(QUERY_ID_PREFIX + ".insertMenuInfo", condition);
	}


	public int updateMenuInfo(OrderManagement condition) {
		return sqlSession.update(QUERY_ID_PREFIX + ".updateMenuInfo", condition);
	}


	public int insertMenuImg(OrderManagement condition) {
		return sqlSession.insert(QUERY_ID_PREFIX + ".insertMenuImg", condition);
	}


	public int updateMenuImg(OrderManagement condition) {
		return sqlSession.update(QUERY_ID_PREFIX + ".updateMenuImg", condition);
	}


	public int insertCategoryInfo(OrderManagement condition) {
		return sqlSession.insert(QUERY_ID_PREFIX + ".insertCategoryInfo", condition);
	}


	public int updateCategoryInfo(OrderManagement condition) {
		return sqlSession.update(QUERY_ID_PREFIX + ".updateCategoryInfo", condition);
	}


	public OrderManagement selectCategoryDtl(OrderManagement condition) {
		return sqlSession.selectOne(QUERY_ID_PREFIX + ".selectCategoryDtl", condition);
	}


	public OrderManagement selectMenuInCategory(OrderManagement condition) {
		return sqlSession.selectOne(QUERY_ID_PREFIX + ".selectMenuInCategory", condition);
	}


	public int deleteCategoryInfo(OrderManagement condition) {
		return sqlSession.delete(QUERY_ID_PREFIX + ".deleteCategoryInfo", condition);
	}


	public List<OrderManagement> selectmanagementTableList(OrderManagement condition) {

		List<OrderManagement> result = null;

		try {
			result = sqlSession.selectList(QUERY_ID_PREFIX + ".selectmanagementTableList", condition);
		} catch(Exception e) {
			if(condition.getCompanyCode() == null) {
				logger.warning("회사 코드가 누락되었습니다.");
			} else {
				e.printStackTrace();
			}
		}
		return result;
	}


	public OrderManagement selectTableInfoDtl(OrderManagement condition) {
		return sqlSession.selectOne(QUERY_ID_PREFIX + ".selectTableInfoDtl", condition);
	}


	public int updateResetTable(OrderManagement condition) {
		return sqlSession.update(QUERY_ID_PREFIX + ".updateResetTable", condition);
	}


	public List<OrderManagement> selectDeviceInfo(OrderManagement condition) {
		return sqlSession.selectList(QUERY_ID_PREFIX + ".selectDeviceInfo", condition);
	}


	public List<OrderManagement> selectCallStaffInfo(OrderManagement condition) {
		return sqlSession.selectList(QUERY_ID_PREFIX + ".selectCallStaffInfo", condition);
	}


	public int updateOrderComplete(OrderManagement condition) {
		return sqlSession.update(QUERY_ID_PREFIX + ".updateOrderComplete", condition);
	}


	public int deleteMenuInfo(OrderManagement condition) {
		return sqlSession.delete(QUERY_ID_PREFIX + ".deleteMenuInfo", condition);
	}


	public List<OrderManagement> selectCloseData(OrderManagement condition) {
		return sqlSession.selectList(QUERY_ID_PREFIX + ".selectCloseData", condition);
	}


	/**
	 * 영업마감을 위한 미제공 메뉴 조회
	 */
	public List<OrderManagement> selectNotOfferForSaleClose(OrderManagement condition) {
		return sqlSession.selectList(QUERY_ID_PREFIX + ".selectNotOfferForSaleClose", condition);
	}


	/**
	 * 영업마감을 위한 미초기화 테이블 조회
	 */
	public List<OrderManagement> selectNotResetTableForSaleClose(OrderManagement condition) {
		return sqlSession.selectList(QUERY_ID_PREFIX + ".selectNotResetTableForSaleClose", condition);
	}


	/**
	 * 영업마감을 위한 영업일자 재설정
	 */
	public int updateSaleDate(OrderManagement condition) {
		return sqlSession.update(QUERY_ID_PREFIX + ".updateSaleDate", condition);
	}


	/**
	 * 영업일자 조회
	 * @param condition
	 * @return
	 */
	public OrderManagement selectSaleDate(OrderManagement condition) {
		return sqlSession.selectOne(QUERY_ID_PREFIX + ".selectSaleDate", condition);
	}


	/**
	 * 영업마감을 위한 오더코드세션 업데이트
	 */
	public int updateOrderCodeSessionStatus(OrderManagement condition) {
		return sqlSession.update(QUERY_ID_PREFIX + ".updateOrderCodeSessionStatus", condition);
	}


	/**
	 * 테이블 번호 변경
	 */
	public int updateTableNo(OrderManagement condition) {
		return sqlSession.update(QUERY_ID_PREFIX + ".updateTableNo", condition);
	}


	public List<OrderManagement> selectCouponList(OrderManagement condition) {
		return sqlSession.selectList(QUERY_ID_PREFIX + ".selectCouponList", condition);
	}


	public int insertCoupon(OrderManagement condition) {
		return sqlSession.insert(QUERY_ID_PREFIX + ".insertCoupon", condition);
	}


	public OrderManagement selectCouponDetail(OrderManagement condition) {
		return sqlSession.selectOne(QUERY_ID_PREFIX + ".selectCouponDetail", condition);
	}
}
