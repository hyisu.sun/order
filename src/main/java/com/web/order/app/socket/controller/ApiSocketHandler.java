package com.web.order.app.socket.controller;


import jakarta.websocket.server.ServerEndpoint;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

@Component
@ServerEndpoint("/api/socket")
public class ApiSocketHandler extends AbstractWebSocketHandler { // or TextWebSocketHandler or AbstractWebSocketHandler
	private Logger logger = Logger.getLogger("ApiSocketHandler");


	private static final List<WebSocketSession> SESSION_LIST = new ArrayList<WebSocketSession>();


	@Override
	public void afterConnectionEstablished(WebSocketSession session) { // 연결 시
		SESSION_LIST.add(session);
	}


	@Override
	public void handleTextMessage(WebSocketSession session, TextMessage message) { // 클라이언트 ==>> 서버 수신 시
		String msg = message.getPayload(); // 클라이언트에서 전송한 메세지 값

		for (int i = 0; i < SESSION_LIST.size(); i++) {
			try {
				SESSION_LIST.get(i).sendMessage(new TextMessage(msg));
			} catch (IOException e) {
				logger.severe("연결된 소켓 세션에 데이터 전송중 에러 발생  ==>>  " + e.getMessage());
			}
		}
	}


	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) { // 전송중 에러 발생
		//		logger.severe("소켓 통신 세션 에러 발생  ==>>  " + session.getId()); // 현재까지는 에러 발생
	}


	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) { // 연결 종료 시
		//		logger.info("종료 상태 코드  ==>>  " + Integer.toString(status.getCode()));


		Iterator<WebSocketSession> list = SESSION_LIST.iterator();

		/*
		 *	1001: 정상적으로 종료된 경우
		 *  1006: 연결이 비정상적으로 종료된 경우
		 */
		if (status.getCode() == 1001 || status.getCode() == 1006) { // 연결을 종료하였을 경우
			while(list.hasNext()) {
				if(list.next().equals(session)) {
					list.remove();
				}
			}

			if (status.getCode() == 1001) {
				//				logger.info("소켓 통신 세션이 정상 종료  ==>>  " + session.getId());
			}

			if (status.getCode() == 1006) {
				//				logger.warning("소켓 통신 세션이 강제 종료  ==>>  " + session.getId());
			}
		}
	}
}
