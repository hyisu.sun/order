package com.web.order.util;


import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.HashMap;


/**
 * @author 선희수
 * @apiNote 각 컨트롤러에서 사용하는 공통 모듈
 */
public class CommonUtils {
	/**
	 * @return 디바이스 ID
	 *
	 * @author 선희수
	 * @apiNote 디바이스 ID 값 호출하는 펑션
	 */
	public String getDeviceId() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		HttpSession session = request.getSession();


		session.setAttribute("deviceId", request.getHeader("DEVICE_ID"));


		return (request.getHeader("DEVICE_ID") != null) ? request.getHeader("DEVICE_ID") : "TEST";
	}


	public HashMap<String, Object> initData(HttpServletRequest request) {
		HashMap<String, Object> initData = new HashMap<String, Object>();

		for (Cookie data : request.getCookies()) {
			if ("isLogin".equals(data.getName())) {
				initData.put("isLogin", data.getValue());
			}


			if ("companyCode".equals(data.getName())) {
				initData.put("companyCode", data.getValue());
			}


			if ("companyName".equals(data.getName())) {
				initData.put("companyName", data.getValue());
			}


			if ("viewMode".equals(data.getName())) {
				initData.put("viewMode", data.getValue());
			}


			if ("D_I".equals(data.getName())) {
				initData.put("deviceId", data.getValue());
			}
		}
		return initData;
	}
}
