package com.web.order.util;


import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.logging.Logger;


@Component
public class FtpUtil {


	Logger logger = Logger.getLogger("FileUtil");


	@Value("${ftp.host}")
	private String ftpHost;

	@Value("${ftp.port}")
	private int ftpPort;

	@Value("${ftp.username}")
	private String ftpUsername;

	@Value("${ftp.password}")
	private String ftpPassword;


	/**
	 * ftp 연결
	 * @return
	 */
	public FTPClient ftpConnect() {


		FTPClient ftpClient = new FTPClient();

		try {
			ftpClient.connect(ftpHost, ftpPort);

			if (ftpClient.isConnected()) {
				ftpClient.login(ftpUsername, ftpPassword);
			} else {
				logger.severe("ftp 연결 실패");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ftpClient;
	}


	/**
	 * ftp 종료
	 */
	public void ftpDisconnect() {
		try {
			ftpConnect().disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * 이미지 파일 base64로 변환
	 * @param inputStream
	 * @return
	 */
	public String convertBase64(InputStream inputStream) {


		// InputStream을 ByteArrayOutputStream으로 변환
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			IOUtils.copy(inputStream, byteArrayOutputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}


		// ByteArrayOutputStream을 byte 배열로 변환
		byte[] bytes = byteArrayOutputStream.toByteArray();


		// byte 배열을 Base64로 인코딩
		String base64Data = Base64.getEncoder().encodeToString(bytes);


		return base64Data;
	}


	/**
	 *
	 * @param imgCode
	 * @param companyCode
	 * @param imgExtension
	 * @return
	 */
	public String getImg(String imgCode, String companyCode, String imgExtension) {


		String menuImg = null;


		try {
			FTPFile[] ftpFiles = ftpConnect().listFiles(companyCode);
			for (FTPFile ftpFile : ftpFiles) {
				if (imgCode.equals(ftpFile.getName())) {
					InputStream inputStream = ftpConnect().retrieveFileStream(companyCode + "/" + ftpFile.getName());
					menuImg = "data:image/" + imgExtension + ";base64," + convertBase64(inputStream);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return menuImg;
	}
}
